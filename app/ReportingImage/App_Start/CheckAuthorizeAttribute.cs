﻿using ReportingImage.Controllers;
using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReportingImage.App_Start
{
    public class CheckAuthorizeAttribute : AuthorizeAttribute
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            //skip if AllowAnonymous
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)) return;

            try
            {
                if (GetSession.User_ID() == 0)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", returnUrl = filterContext.HttpContext.Request.FilePath }));
                }
                else
                {
                    if (this.Roles != "")
                    {
                        bool retval = false;

                        if (GetSession.IsAdministrator())
                        {
                            retval = true;
                        }
                        else
                        {
                            int Priv = GetSession.Privilege_ID();
                            var DT = from c in _db.zRole_Menu
                                     join b in _db.zMenus on c.menu_id equals b.id
                                     where c.previlage_id == Priv 
                                     && b.Code == Roles
                                     select c;
                            if (DT.ToList().Count != 0)
                            {
                                retval = Convert.ToBoolean(DT.Single().is_read);
                            }


                        }

                        if (!retval)
                        {

                            filterContext.Result = new RedirectToRouteResult(
                                                    new RouteValueDictionary
                                                    {
                                                        { "controller", "Home" },
                                                        { "action", "Error403" }
                                                    });
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectToRouteResult(
                                                   new RouteValueDictionary
                                                    {
                                                        { "controller", "Home" },
                                                        { "action", "Error403" }
                                                    });

            }


        }


        public class CheckButtonPriv
        {
            public static bool CheckButtonAuth(string Roles)
            {
                if (GetSession.User_ID() == 0)
                {

                }
                else
                {
                    ReportingImageEntities db = new ReportingImageEntities();
                    bool retval = false;
                    if (GetSession.IsAdministrator())
                    {
                        retval = true;
                    }
                    else
                    {
                        try
                        {
                            //Guid Priv = GetSession.Privilege_ID();
                            //var DT = from c in db.t_privilege_menu
                            //         join b in db.t_menu on c.menu_id equals b.MenuID
                            //         where c.privilege_id == Priv && b.Code == Roles
                            //         select c;
                            //if (DT.ToList().Count != 0) retval = DT.Single().permission;
                            retval = true;

                        }
                        catch (Exception ex)
                        {
                            retval = false;
                        }


                    }

                    return retval;
                }

                return false;
            }
        }
    }
}