﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class PosPolicyPoolOpenViewModels
    {
        public int CASE_ID { get; set; }

        public string CIF { get; set; }

        public string POLICY_NO { get; set; }

        public string SOURCE { get; set; }

        public string WORKTYPE_ID { get; set; }

        public string WORKTYPE { get; set; }

        public string CATEGORY { get; set; }

        public string RLS_TRANS { get; set; }

        public DateTime? RLS_CAPTURE_DATE { get; set; }

        public bool IS_COMPLETE { get; set; }

        public DateTime? DATE_COMPLETE { get; set; }
        
        public DateTime CREATE_DATE { get; set; }

        public string USER_ASSIGN { get; set; }

        public string REMARKS { get; set; }

        public DateTime? REMINDER { get; set; }

        public bool SENT_TO_COMPLIENCE { get; set; }

        public bool CHECKED { get; set; }

        public string METHOD { get; set; }

        public int COUNTS { get; set; }

        public string BUCKET { get; set; }

        public string ERRORMESSAGE { get; set; }

        public string HOLDER { get; set; }

        public int SLA { get; set; }

        public int AGINGSLA { get; set; }

        public bool IS_PAID { get; set; }

        public List<DetailImageViewModels> IMAGES { get; set; }

    }

    public class ListPosPolicyPoolViewModels
    {
        public List<PosPolicyPoolOpenViewModels> ListPosPolicyPoolOpenViewModels { get; set; }

        public HttpPostedFileBase FileUpload { get; set; }
    }
}