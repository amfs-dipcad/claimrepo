﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class ProductViewModels
    {
        public int id { get; set; }
        public string product_name { get; set; }
        public string product_code { get; set; }
        public string type_product { get; set; }
        public string payment_method { get; set; }
        public Nullable<bool> is_delete { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public string updated_by { get; set; }
        public int Counts { get; set; }
    }
}