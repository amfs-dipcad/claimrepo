﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class WeekViewModels
    {
        public string Row_Label  { get; set; }
        public int Week_1 { get; set; }
        public int Week_2 { get; set; }
        public int Week_3 { get; set; }
        public int Week_4 { get; set; }
        public int Week_5 { get; set; }

        public string Date { get; set; }
        public string Categories { get; set; }

        public string LineCategory { get; set; }

    }
}