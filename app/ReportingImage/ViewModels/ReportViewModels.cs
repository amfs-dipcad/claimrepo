﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class ReportViewModels
    {
        public string WDOS { get; set; }
        public string WPMODE { get; set; }
        public string WTTPRM { get; set; }
        public string NOMOR_POLIS { get; set; }
        public string PLAN_CODE { get; set; }
        public string Remark { get; set; }
        public string Status_Dispatcher { get; set; }
        public string source_data { get; set; }
        public string Type_of_UW { get; set; }
        public string User { get; set; }
        public string PADate { get; set; }
    }
}