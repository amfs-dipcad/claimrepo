﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class UserViewModels
    {
        public int id { get; set; }
        public string username { get; set; }
        public Nullable<int> parent_id { get; set; }
        public string parent_name { get; set; }
        public Nullable<int> previlage_id { get; set; }
        public string privilege_name { get; set; }
        public string dept_id { get; set; }
        public Nullable<decimal> min_ape { get; set; }
        public Nullable<decimal> max_ape { get; set; }
        public bool active_directory { get; set; }
        public string password { get; set; }
        public bool is_delete { get; set; }
        public bool? is_login { get; set; }
        public bool? bucket { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public string updated_by { get; set; }
        public int Counts { get; set; }
    }
}