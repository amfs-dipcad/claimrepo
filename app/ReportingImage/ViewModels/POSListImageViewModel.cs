﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class POSListImageViewModel
    {
        public List<DetailImageViewModels> DetailImageViewModels { get; set; }
        public string PolicyNumber { get; set; }
        public int Case_ID { get; set; }
        public string WorkType_id { get; set; }
        public string x { get; set; }
        public int y { get; set; }
    }
}