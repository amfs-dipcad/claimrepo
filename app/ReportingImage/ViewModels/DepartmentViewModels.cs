﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class DepartmentViewModels
    {
        public string dept_id { get; set; }
        public string dept_name { get; set; }
    }
}