﻿using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class MenuViewModels
    {
        public bool Checked { get; set; }
        public zMenu Parent { get; set; }
        public List<MenuViewModels> Child { get; set; }
    }

    public class MenuModels
    {
        public List<MenuViewModels> Menu { get; set; }
    }
}