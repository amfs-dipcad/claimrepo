﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class TargetActualViewModels
    {
        public string Team { get; set; }
        public int Target { get; set; }
        public int Actual { get; set; }
        public int GAP { get; set; }
        public int Total { get; set; }
    }
}