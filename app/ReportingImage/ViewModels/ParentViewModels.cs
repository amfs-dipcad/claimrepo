﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class ParentViewModels
    {
        public string Parent_Id { get; set; }
        public string Parent_Name { get; set; }
    }
}