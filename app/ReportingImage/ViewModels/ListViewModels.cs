﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class ListViewModels
    {
        public List<DetailImageViewModels> DetailImageViewModels { get; set; }
        public string PolicyNumber { get; set; }
        public string TypeOfUwId { get; set; }
        public string TypeOfUwText { get; set; }
        public string BucketId { get; set; }
        public string BucketName { get; set; }
        public string isCleanId { get; set; }
        public string isCleanName { get; set; }
        public string x { get; set; }
        public int y { get; set; }
    }
}