﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class DetailImageViewModels
    {
        public string PolicyNumber { get; set; }
        public string FILENAME { get; set; }
        public string FILENAMEORIGIN { get; set; }
        public string DocumentName { get; set; }
        public string Source { get; set; }
        public bool Checked { get; set; }
        public byte[] FILEIMAGE { get; set; }
        public string Extension { get; set; }
        public string PreviewContentId { get; set; }
        public byte[] FILEIMAGE_temp { get; set; }
        public string FilePath { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}