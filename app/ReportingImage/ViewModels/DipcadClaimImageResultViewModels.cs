﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class DipcadClaimImageResultViewModels
    {
        public string POLICYNO { get; set; }
        public string FILENAMEORIGIN { get; set; }
        public string FILENAME { get; set; }
        public string DOCUMENTNAME { get; set; }
        public string SOURCE { get; set; }
        public byte[] FILEIMAGE { get; set; }
        public Nullable<System.DateTime> CREATEDATE { get; set; }
    }
}