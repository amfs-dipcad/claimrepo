﻿using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class PrivilegeViewModels
    {
        public zPrevilage Parent { get; set; }
        public List<MenuViewModels> Menu { get; set; }
    }
}