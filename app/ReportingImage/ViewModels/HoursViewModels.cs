﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class HoursViewModels
    {
        public string Row_Label { get; set; }
        public int Eight { get; set; }
        public int Nine { get; set; }
        public int Ten { get; set; }
        public int Eleven { get; set; }
        public int Twelve { get; set; }
        public int Thirteen { get; set; }
        public int FourTeen { get; set; }

        public int FifTeen { get; set; }
        public int SixTeen { get; set; }

        public int SevenTeen { get; set; }
        public int Eighteen { get; set; }
        public int Total { get; set; }
    }
}