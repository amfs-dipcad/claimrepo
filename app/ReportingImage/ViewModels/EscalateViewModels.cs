﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class EscalateViewModels
    {
        public string Policy_Number { get; set; }
        public string Users { get; set; }
        [Required]
        public string Remarks { get; set; }
        public string ErrorUsers { get; set; }
        public string ErrorRemarks { get; set; }
        public string BucketType { get; set; }
    }
}