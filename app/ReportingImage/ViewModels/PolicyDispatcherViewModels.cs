﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class PolicyDispatcherViewModels
    {
        public string policy_no { get; set; }
        public string insured_name { get; set; }
        public Nullable<decimal> dob_insured { get; set; }
        public string sex_insured { get; set; }
        public string owner_name { get; set; }
        public Nullable<decimal> dob_owner { get; set; }
        public string sex_owner { get; set; }
        public string currency { get; set; }
        public string payment_type { get; set; }
        public string product_code { get; set; }
        public Nullable<decimal> ape { get; set; }
        public Nullable<decimal> sum_assured { get; set; }
        public string status_policy { get; set; }
        public Nullable<int> batch_id { get; set; }
        public string assigned_to { get; set; }
        public Nullable<System.DateTime> create_date { get; set; }
        public string create_by { get; set; }
        public string is_escalate { get; set; }
        public string remark_escalate { get; set; }
        public int id { get; set; }
        public string product_name { get; set; }
        public string master_product_code { get; set; }
        public string type_product { get; set; }
        public string payment_method { get; set; }
        public Nullable<bool> is_delete { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public string updated_by { get; set; }
        public int is_clean { get; set; }
        public string is_clean_name { get; set; }
        public string policy_no_closed { get; set; }
        public string policy_no_escalate { get; set; }
        public string policy_no_isclean { get; set; }
        public string title_btn_escalate { get; set; }
        public DateTime date_submission { get; set; }
        public string status_spaj { get; set; }
        public DateTime? pa_date { get; set; }
        public int Counts { get; set; }
    }
}