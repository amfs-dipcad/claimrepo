﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.ViewModels
{
    public class DipcadClaimOpenViewModels
    {
        public int CASE_ID { get; set; }

        public string POLICY_NO { get; set; }

        //public string CIF { get; set; }

        public string SOURCE { get; set; }

        public string WORKTYPE_ID { get; set; }

        public string WORKTYPE { get; set; }

        public string CATEGORY { get; set; }

        public string RLS_TRANS { get; set; }

        public DateTime? RLS_CAPTURE_DATE { get; set; }

        public bool IS_COMPLETE { get; set; }

        public DateTime? REQPENDATE { get; set; }
        public DateTime? FINISHPENDATE { get; set; }
        public DateTime? REMINDERPENDATE { get; set; }
        public bool IS_PENDING { get; set; }

        public DateTime? DATE_COMPLETE { get; set; }

        public DateTime CREATE_DATE { get; set; }

        public string USER_ASSIGN { get; set; }

        public string REMARKS { get; set; }

        public DateTime? REMINDER { get; set; }

        public bool SENT_TO_COMPLIENCE { get; set; }

        public bool CHECKED { get; set; }

        public string METHOD { get; set; }

        public int COUNTS { get; set; }

        public string BUCKET { get; set; }

        public string ERRORMESSAGE { get; set; }

        public string HOLDER { get; set; }

        public int SLA { get; set; }

        public int AGINGSLA { get; set; }

        public bool IS_PAID { get; set; }
        public int SEQ { get; set; }
        public string ColorSLA { get; set; }
        public bool IS_COMPLIANCE { get; set; }

        public string PRIORITY { get; set; }

        public string RECLAIMNO { get; set; }

        public string CLAIMNO { get; set; }
        public DateTime? RECEIVED_DATE { get; set; }
        public string OWNER_NAME { get; set; }
        public string MEMBER_NAME { get; set; }
        public DateTime? INCURED_DATE { get; set; }
        public string CASE_TYPE { get; set; }
        public string VIP { get; set; }
        public string PENDING_REASON { get; set; }
        public bool  IS_REJECT { get; set; }
        public int? CLAIM_AMOUNT { get; set; }
        public DateTime? OWNER_DOB { get; set; }
        public string MEMBER_NO { get; set; }
        public string OWNER_ID { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PRIMARY_DOC { get; set; }
        public string MEMBER_ID { get; set; }
        public string DOC_SOURCE_TYPE { get; set; }
        public string TSA { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string PRODUCT_TYPE { get; set; }
        public string DOCUMENT_TITLE { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string CHANNEL { get; set; }
        public string MESSAGE { get; set; }

        public string SLAClaim { get; set; }
        public string SLAPending { get; set; }
        public string UserFrom { get; set; }
        public DateTime RequestPendingDate { get; set; }
        public string PENDING_LETTER { get; set; }
        public string REJECT_LETTER { get; set; }

        public List<DetailImageViewModels> IMAGES { get; set; }
        public List<AuditJobHistory> AuditJobHistories { get; set; }
        
    }

    public class ListDipcadClaimPoolViewModels
    {
        public List<DipcadClaimOpenViewModels> ListDipcadClaimPoolOpenViewModels { get; set; }

        public HttpPostedFileBase FileUpload { get; set; }
    }

    public class SLASetup
    {
        public int ParamTo { get; set; }
        public int ParamFrom { get; set; }
        public string Priority { get; set; }
        public string ClaimType { get; set; }
        public string Color { get; set; }

    }

    public class DetailImageTempView
    {
        public string PolicyNo { get; set; }
        public string FileName { get; set; }
        public string DocumentName { get; set; }
        public string Source { get; set; }
        public byte[] FileImage { get; set; }
    }

    public class AuditJobHistory
    {
        public string Action { get; set; }
        public string User { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }
        public DateTime? Date { get; set; }
        public string Value { get; set; }
    }

    public class DipcadClaimAssignViewModel
    {
        public long? CASE_ID { get; set; }
        public string POLICY_NO { get; set; }
    }

    public class PolicyMapping
    {
        //public string CaseId { get; set; }
        public long CaseId { get; set; }
        public string PolicyNo { get; set; }
        public string WorkType { get; set; }
        public string ClaimType { get; set; }
    }

    public class EmployeeLeaveStatus
    {
        public string No { get; set; }
        public string Id { get; set; }
        public string EmployeeName { get; set; }
        public string DeptId { get; set; }
        public DateTime? LeaveDateFrom { get; set; }
        public DateTime? LeaveDateTo { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public bool IsFullDay { get; set; }
        public string Remarks { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public string UserSubmitted { get; set; } 
    }

    public class AssignDataList
    {
        public List<AssignData> AssignDataLists { get; set; }
    }

    public class AssignData
    {
        public Nullable<long> SEQ { get; set; }
        public Nullable<long> CaseId { get; set; }
        public string PolicyNo { get; set; }
        public string Source { get; set; }
        public string WorkType { get; set; }
        public string Category { get; set; }
        public string RLSTRANS { get; set; }
        public Nullable<System.DateTime> RLSCaptureDate { get; set; }
        public Nullable<bool> IsComplete { get; set; }
        public Nullable<System.DateTime> CompleteDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public string UserAssign { get; set; }
        public Nullable<bool> IsPending { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> DateSet { get; set; }
        public int SLA { get; set; }
        public string ColorSLA { get; set; }
        public string VIP { get; set; }
        public string Bucket { get; set; }
        public string CheckBox { get; set; }
    }

    public class RoleMenuMapping
    {
        public string Role { get; set; }
        public string Menu { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsFullAccess { get; set; }
    }

    public  class PolicyPool_Inq
    {
        public Nullable<long> SEQ { get; set; }
        public Nullable<long> CaseId { get; set; }
        public string PolicyNo { get; set; }
        public string Source { get; set; }
        public string WorkType { get; set; }
        public string Category { get; set; }
        public string RLSTRANS { get; set; }
        public Nullable<System.DateTime> RLSCaptureDate { get; set; }
        public Nullable<bool> IsComplete { get; set; }
        public Nullable<System.DateTime> CompleteDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public string UserAssign { get; set; }
        public Nullable<bool> IsPending { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> DateSet { get; set; }
        public int SLA { get; set; }
        public string ColorSLA { get; set; }
        public string VIP { get; set; }
    }

    public class HolidayList
    {
        public DateTime HolidayDate { get; set; }
         
    }
}