﻿function startTimer(duration) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        //console.log(timer);
        if (--timer === 0) {
            var countA = timer;
            $.ajax({
                type: 'POST',
                url: 'Home/UpdateUserLogin',
                dataType: 'json',
                data: { count: countA },
                success: function (a) {
                    //alert(a);
                }
            });
            return false;
            //timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 20;
    startTimer(fiveMinutes);
};