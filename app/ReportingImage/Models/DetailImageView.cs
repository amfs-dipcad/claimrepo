//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingImage.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetailImageView
    {
        public string POLICYNO { get; set; }
        public string FILENAME { get; set; }
        public string DocumentName { get; set; }
        public string Source { get; set; }
        public byte[] FILEIMAGE { get; set; }
    }
}
