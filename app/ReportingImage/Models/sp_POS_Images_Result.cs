//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingImage.Models
{
    using System;
    
    public partial class sp_POS_Images_Result
    {
        public string POLICYNO { get; set; }
        public string FILENAMEORIGIN { get; set; }
        public string FILENAME { get; set; }
        public string DOCUMENTNAME { get; set; }
        public string SOURCE { get; set; }
        public byte[] FILEIMAGE { get; set; }
        public Nullable<System.DateTime> CREATEDATE { get; set; }
    }
}
