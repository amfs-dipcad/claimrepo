//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingImage.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HolidaySetup
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> HolidayDate { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> DateSubmitted { get; set; }
        public string UserSubmitted { get; set; }
    }
}
