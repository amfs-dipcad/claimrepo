﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingImage.Models
{
    public class RequestResponse
    {

        public class MessageResponse
        {
            public long Id { get; set; }
            public object Content { get; set; }
            public string Message { get; set; }
            public string Code { get; set; }
        }

        public class ResultException
        {
            public exception exception;
        }

        public class exception
        {
            public string message { get; set; }
            public string reasonCode { get; set; }
            public string reason { get; set; }
        }
    }
}