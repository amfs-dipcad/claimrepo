﻿using ReportingImage.App_Start;
using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class AppPramController : Controller
    {
        // GET: AppPram
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "App Param")]
        public ActionResult Index()
        {
            var userName = GetSession.UserName();
            var appPram = _db.zMaster_AppParam.Where(a => a.is_delete == false && a.parent_id == null);
            _db.zsp_AuditTrailInsert("App Param", null, userName);
            return View(appPram.ToList());
        }
        [CheckAuthorize(Roles = "Create App Param")]
        public ActionResult Create()
        {
            TempData["paramName"] = "";
            zMaster_AppParam model = new zMaster_AppParam();
            if (TempData["idParamName"] != null)
            {
                var id = Convert.ToInt32(TempData["idParamName"]);
                model.param_name = _db.zMaster_AppParam.Where(a => a.id == id).Single().param_name;
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Create App Param", null, userName);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Create App Param")]
        public ActionResult Create(zMaster_AppParam zAppParam)
        {
            try
            {
                if (zAppParam.param_name == null) { ModelState.AddModelError("param_name", "This field is required."); }
                var idParamName = Convert.ToInt32(TempData["idParamName"]);
                var checkAppParam = _db.zMaster_AppParam.Where(x => x.parent_id == idParamName).ToList();
                if (checkAppParam.Count() != 0)
                {
                    if (ModelState.IsValid)
                    {
                        _db.zsp_AuditTrailInsert("Create App Param", null, GetSession.UserName());
                        TempData["idParamName"] = null;
                        TempData["Success"] = "Success for Saving " + zAppParam.param_name;
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["Error"] = "Add New Paremeter before submit";
                }

                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Create App Param", null, userName);

                return View(zAppParam);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [CheckAuthorize(Roles = "Update App Param")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zMaster_AppParam zAppParam = _db.zMaster_AppParam.Find(id);
            if (zAppParam == null)
            {
                return HttpNotFound();
            }
            TempData["idParamName"] = id;
            if (TempData["idParamName"] != null)
            {
                zAppParam.param_name = _db.zMaster_AppParam.Where(a => a.id == id).Single().param_name;
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Update App Param", null, userName);

            return View(zAppParam);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Update App Param")]
        public ActionResult Edit(zMaster_AppParam zAppParam)
        {
            try
            {
                if (zAppParam.param_name == null) { ModelState.AddModelError("param_name", "This field is required."); }
                var idParamName = Convert.ToInt32(TempData["idParamName"]);
                var checkAppParam = _db.zMaster_AppParam.Where(x => x.parent_id == idParamName).ToList();
                if (checkAppParam.Count() != 0)
                {
                    if (ModelState.IsValid)
                    {
                        _db.zsp_AuditTrailInsert("Update App Param", null, GetSession.UserName());
                        TempData["idParamName"] = null;
                        TempData["Success"] = "Success for Saving " + zAppParam.param_name;
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["Error"] = "Add New Paremeter before submit";
                }

                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Update App Param", null, userName);

                return View(zAppParam);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [CheckAuthorize(Roles = "Create Parent App Param")]
        public ActionResult CreateParent()
        {
            try
            {
                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Create Parent App Param", null, userName);

                return PartialView("CreateParent");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Create Parent App Param")]
        public ActionResult CreateParent(string isBack, zMaster_AppParam zAppParam)
        {
            string parameterName = "";
            if (TempData["paramName"] != null)
            {
               parameterName  = TempData["paramName"].ToString();
            }
            
            if (zAppParam.param_name == null) { ModelState.AddModelError("param_name", "This field is required."); }

            if (zAppParam.param_value == null) { ModelState.AddModelError("param_value", "This field is required."); }

            var parentId = _db.zMaster_AppParam.Where(a => a.param_name == parameterName).Single().id;
            var checkAppParam = _db.zMaster_AppParam.Where(x => x.param_name == zAppParam.param_name).ToList();
            if (checkAppParam.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_MasterAppParamInsert(GetSession.UserName(), zAppParam.param_name, Convert.ToInt32(parentId), zAppParam.param_value,
                        zAppParam.description, isBack);
                    _db.zsp_AuditTrailInsert("Create Parent App Param", null, GetSession.UserName());
                    TempData["idParamName"] = parentId;
                    return Json(new { success = true });
                }
            }
            else
            {
                TempData["Error"] = "Parameter Name :" + zAppParam.param_name + " already exist!";
            }

            return Json(new { success = true });
        }

        [CheckAuthorize(Roles = "Parent App Param")]
        public ActionResult Parent(int? id)
        {
            try
            {
                var parentList = _db.zMaster_AppParam.Where(a => a.parent_id == id && a.is_delete == false);
                TempData["idParamName"] = id;

                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Paremt App Param", null, userName);

                return PartialView("Parent", parentList.ToList());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [CheckAuthorize(Roles = "Update Parent App Param")]
        public ActionResult EditParent(int? parentId)
        {
            try
            {
                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Update Parent App Param", null, userName);

                zMaster_AppParam parentList = _db.zMaster_AppParam.Find(parentId);
                return PartialView("EditParent",parentList);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Update Parent App Param")]
        public ActionResult EditParent(zMaster_AppParam zAppParam)
        {
            //string parameterName = TempData["paramName"].ToString();
            if (zAppParam.param_name == null) { ModelState.AddModelError("param_name", "This field is required."); }

            if (zAppParam.param_value == null) { ModelState.AddModelError("param_value", "This field is required."); }

            //var parentId = _db.zMaster_AppParam.Where(a => a.param_name == parameterName).Single().id;
            var checkAppParam = _db.zMaster_AppParam.Where(x => x.param_name == zAppParam.param_name && x.id != zAppParam.id).ToList();
            if (checkAppParam.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_MasterAppParamUpdate(Convert.ToInt32(zAppParam.id), GetSession.UserName(), zAppParam.param_name, zAppParam.parent_id, zAppParam.param_value, zAppParam.description, false);
                    _db.zsp_AuditTrailInsert("Update Parent App Param", null, GetSession.UserName());
                    return Json(new { success = true });
                }
            }
            else
            {
                TempData["Error"] = "Parameter Name :" + zAppParam.param_name + " already exist!";
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Update Parent App Param", null, userName);

            return Json(new { success = true });
        }
        public JsonResult zAppParamInsert(string paramName)
        {
            bool error = false;
            try
            {
                TempData["paramName"] = paramName;
                var existingParamName = _db.zMaster_AppParam.Where(a => a.param_name == paramName).Count();
                if(existingParamName == 0)
                {
                    _db.zsp_MasterAppParamInsert(GetSession.UserName(), paramName, null, null, null,null);
                }
                
                return Json(error);
            }
            catch (Exception ex)
            {
                error = true;
                return Json(error);
            }
        }
        
        public ActionResult Delete(int? id, string isBack)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            zMaster_AppParam zAppParam = _db.zMaster_AppParam.Find(id);
            if (zAppParam == null)
            {
                TempData["Error"] = "Parameter Name not Found";
            }

            TempData["idParamName"] = null;
            _db.zsp_MasterAppParamDelete(zAppParam.id, isBack);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Delete App Param", null, userName);

            return RedirectToAction("Index");
        }
        [CheckAuthorize(Roles = "Delete App Param")]
        public ActionResult DeleteUpdate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zMaster_AppParam zAppParam = _db.zMaster_AppParam.Find(id);
            if (zAppParam == null)
            {
                TempData["Error"] = "Parameter Name not Found";
            }

            _db.zsp_MasterAppParamUpdate(Convert.ToInt32(zAppParam.id),GetSession.UserName(),zAppParam.param_name,zAppParam.parent_id,zAppParam.param_value,zAppParam.description,true);
            _db.zsp_AuditTrailInsert("Delete App Param", null, GetSession.UserName());

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Delete App Param", null, userName);

            return RedirectToAction("Index");
        }
        
        public ActionResult DeleteUpdateParent(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zMaster_AppParam zAppParam = _db.zMaster_AppParam.Find(id);
            if (zAppParam == null)
            {
                TempData["Error"] = "Parameter Name not Found";
            }

            _db.zsp_MasterAppParamUpdate(Convert.ToInt32(zAppParam.id), GetSession.UserName(), zAppParam.param_name, zAppParam.parent_id, zAppParam.param_value, zAppParam.description, true);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Delete App Param", null, userName);

            return RedirectToAction("Edit/"+zAppParam.parent_id);
        }
    }
}