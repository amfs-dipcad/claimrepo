﻿using Ionic.Zip;
using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ReportingImage.Controllers
{
    public class Common
    {
        //ReportingImageEntities _db = new ReportingImageEntities();
        public static SqlConnection GetConnection()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportingImageConnection"].ConnectionString);
            return con;
        }
        public static DataTable ExecuteQuery(string Query)
        {
            try
            {
                DataTable Tbl = new DataTable();
                SqlConnection con = GetConnection();
                SqlCommand cmd = new SqlCommand(Query, con);
                cmd.CommandTimeout = 60;
                con.Open();
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                Tbl.Load(reader);
                con.Close();
                return Tbl;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static void ExecuteNonQuery(string Query)
        {
            try
            {
                SqlConnection con = GetConnection();
                SqlCommand cmd = new SqlCommand(Query, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string EncodePasswordMd5(string pass) //Encrypt using MD5    
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            string result = string.Empty;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)    
            md5 = new MD5CryptoServiceProvider();
            if (!string.IsNullOrEmpty(pass))
            {
                originalBytes = ASCIIEncoding.Default.GetBytes(pass);
                encodedBytes = md5.ComputeHash(originalBytes);
                //Convert encoded bytes back to a 'readable' string  
                result = BitConverter.ToString(encodedBytes);
            }

            return result;
        }
        
        public static string GetStringRight(string value, int lenghtSubstring)
        {
            return value.Substring(value.Length - lenghtSubstring);
        }
        public static string SplitValueFileName(string value)
        {
            string[] valueTmp = value.Split('.');
            return valueTmp[0].ToString();
        }
        public static void LoginOff(ReportingImageEntities _db, int? userId)
        {
            _db.zsp_LoginUpdate(userId, false);
            HttpContext.Current.Session["User_ID"] = null;
            HttpContext.Current.Session["User_Name"] = null;
            HttpContext.Current.Session["Privilege_ID"] = null;
        }

        public static void RemoveSession(int id)
        {
            try
            {
                ReportingImageEntities db = new ReportingImageEntities();
                zUser _zUser = db.zUsers.Find(id);
                _zUser.session_id = null;
                _zUser.is_login = false;
                db.Entry(_zUser).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // ------------------------------------------------
        // Author   : Andhi Sumarjo | POS attributes
        public static void Delete_MST_Image(string filename)
        {
            try
            {
                SqlConnection con = GetConnection();
                SqlCommand cmd = new SqlCommand("DELETE MST_IMAGE WHERE FILENAME = @filename", con);

                cmd.Parameters.AddWithValue("@filename", filename);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Insert_MST_Image(string policyno, string filename, byte[] fileimage)
        {
            try
            {
                SqlConnection con = GetConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO MST_IMAGE (POLICYNO, FILENAME, FILEIMAGE, CREATEDATE, ISGENERATED, GENERATEDATE, ISADDITIONAL, XML_ISDOWNLOAD, TEPOL_ISDOWNLOAD, ISPRINTING, ORIGIN) VALUES " +
                            "(@policyno," +
                            "@filename," +
                            "@fileimage," +
                            "@date," +
                            "'1'," +
                            "@date," +
                            "'0'," +
                            "'FALSE'," +
                            "'FALSE'," +
                            "'False'," +
                            "'AdHoc')", con);

                cmd.Parameters.AddWithValue("@policyno", policyno);
                cmd.Parameters.AddWithValue("@filename", filename);
                cmd.Parameters.AddWithValue("@fileimage", fileimage);
                cmd.Parameters.AddWithValue("@date", DateTime.Now);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // ------------------------------------------------
        // Author   : Agus Handoko - Lanbar Rizky | CLAIM attributes
        public static void InsertMSTImageAdditional(string policyno, string filename, byte[] fileimage, int caseid, string deptId, int docId, string docTitle, string docType, string workType, string description, string source)
        {
            try
            {
                SqlConnection con = GetConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO MST_IMAGE_ADDITIONAL (" +
                                                   "[case_id]" +
                                                   ",[PolicyNo]" +
                                                   ",[Filename]" +
                                                   ",[Fileimage]" +
                                                   ",[Dept_Id]" +
                                                   ",[Document_Id]" +
                                                   ",[DocumentTitle]" +
                                                   ",[DocumentType]" +
                                                   ",[WorkType]" +
                                                   ",[Description]" +
                                                   ",[CreateDate]" +
                                                   ",[CreateBy]" +
                                                   ",[Source]) " +
                                                "VALUES " +
                                                   "(@case_id" +
                                                   ",@PolicyNo" +
                                                   ",@Filename" +
                                                   ",@Fileimage" +
                                                   ",@Dept_Id" +
                                                   ",@Document_Id" +
                                                   ",@DocumentTitle" +
                                                   ",@DocumentType" +
                                                   ",@WorkType" +
                                                   ",@Description" +
                                                   ",getdate()" +
                                                   ",'" + GetSession.UserName() + "'" +
                                                   ",@source)"
                                                , con);

                cmd.Parameters.Add("@case_id", SqlDbType.Int).Value = caseid;
                cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 20).Value = policyno;
                cmd.Parameters.Add("@Filename", SqlDbType.VarChar, 150).Value = filename;
                cmd.Parameters.Add("@Fileimage", SqlDbType.VarBinary, fileimage.Length).Value = fileimage;
                cmd.Parameters.Add("@Dept_Id", SqlDbType.VarChar, 10).Value = deptId;
                cmd.Parameters.Add("@Document_Id", SqlDbType.Int).Value = docId;
                cmd.Parameters.Add("@DocumentTitle", SqlDbType.VarChar, 150).Value = docTitle;
                cmd.Parameters.Add("@DocumentType", SqlDbType.VarChar, 50).Value = docType;
                cmd.Parameters.Add("@WorkType", SqlDbType.VarChar, 30).Value = workType;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = description;
                cmd.Parameters.Add("@source", SqlDbType.VarChar).Value = source;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}