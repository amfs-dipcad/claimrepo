﻿using Ionic.Zip;
using iTextSharp.text;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class DipcadClaimOpenController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        string thisMenu = "Open";

        // GET: DipcadClaimOpen
        public ActionResult Index(string policyNumber, string ddlCategory, int pageNumber = 1, string receiveddatefrom = null, string receiveddateto = null)
        {

            var User = GetSession.UserName();
            TempData["ddlCategory"] = ddlCategory;

            var tempResult = _db.SPCLM_PolicyPool_Inq().Where(x => User == x.UserAssign?.Trim()
                                                                && (x.IsComplete != true)
                                                                && (x.IsPending != true)
                                                                ).ToList();

            //var tempResulttemp = _db.SPCLM_PolicyPool_Inq_dev(10, pageNumber).

            //var tempResult = new List<PolicyPool_Inq>();
            //DataTable dTab = Common.ExecuteQuery($"dbo.SPCLM_PolicyPool_Inq");
            //for (int i = 0; i < dTab.Rows.Count; i++)
            //{
            //    if (dTab.Rows[i]["IsPending"].ToString() != "" && dTab.Rows[i]["UserAssign"].ToString().Trim() == User)
            //    {
            //        var data = new PolicyPool_Inq();
            //        data.CaseId = Convert.ToInt64(dTab.Rows[i]["CaseId"].ToString());
            //        data.VIP = dTab.Rows[i]["VIP"].ToString();
            //        data.PolicyNo = dTab.Rows[i]["PolicyNo"].ToString();
            //        data.Source = dTab.Rows[i]["Source"].ToString();
            //        data.WorkType = dTab.Rows[i]["WorkType"].ToString();
            //        data.Category = dTab.Rows[i]["Category"].ToString();
            //        data.RLSTRANS = dTab.Rows[i]["RLSTRANS"].ToString();
            //        data.RLSCaptureDate = String.IsNullOrWhiteSpace(dTab.Rows[0]["RLSCaptureDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["RLSCaptureDate"].ToString());
            //        data.IsComplete = dTab.Rows[0]["IsComplete"].ToString() == "1" ? true : false;
            //        data.IsPending = dTab.Rows[0]["IsPending"].ToString() == "1" ? true : false;
            //        data.CompleteDate = String.IsNullOrWhiteSpace(dTab.Rows[0]["CompleteDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["CompleteDate"].ToString());
            //        data.CreatedDate = String.IsNullOrWhiteSpace(dTab.Rows[0]["CreatedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["CreatedDate"].ToString());
            //        data.ReceivedDate = String.IsNullOrWhiteSpace(dTab.Rows[0]["ReceivedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["ReceivedDate"].ToString());
            //        data.UserAssign = dTab.Rows[i]["UserAssign"].ToString();
            //        data.Message = dTab.Rows[i]["Message"].ToString();
            //        tempResult.Add(data);
            //    }
            //}
            //var tempResult = _db.SPCLM_PolicyPool_Inq().ToList();

            //if(User != null && User != "")
            //{
            //    tempResult = tempResult.Where(a => a.UserAssign.Trim() == User).ToList();
            //}
            if (receiveddatefrom != null && receiveddatefrom != "")
            {
                DateTime dateTimereceiveddatefrom = Convert.ToDateTime(receiveddatefrom);
                tempResult = tempResult.Where(a => a.CreatedDate.Date >= dateTimereceiveddatefrom).ToList();
            }
            if (receiveddateto != null && receiveddateto != "")
            {
                DateTime dateTimereceiveddateto = Convert.ToDateTime(receiveddateto);
                tempResult = tempResult.Where(a => a.CreatedDate.Date <= dateTimereceiveddateto).ToList();
            }
            if (policyNumber != null && policyNumber != "")
            {
                tempResult = tempResult.Where(a => a.PolicyNo == policyNumber).ToList();
            }

            foreach (var item in tempResult)
            {
                if (item.VIP == "YES") item.ColorSLA = "#f78181";
            }

            //get SLA setup
            var SLASetups = new List<SLASetup>();
            DataTable SLATab = Common.ExecuteQuery($"select * from [dbo].[CLM_SLASetup]");
            for (int i = 0; i < SLATab.Rows.Count; i++)
            {
                SLASetups.Add(new SLASetup()
                {
                    ClaimType = SLATab.Rows[i]["ClaimType"].ToString(),
                    ParamTo = Convert.ToInt32(SLATab.Rows[i]["ParamTo"].ToString()),
                    ParamFrom = Convert.ToInt32(SLATab.Rows[i]["ParamFrom"].ToString()),
                    Priority = SLATab.Rows[i]["Priority"].ToString(),
                    Color = SLATab.Rows[i]["Color"].ToString()
                });
            }

            //tempResult = tempResult.Where(a => a.CaseId == 5894).ToList();

            foreach (var item in tempResult)
            {
                //var newdate = item.CreatedDate ?? DateTime.Now;
                var newdate = item.CreatedDate;
                var SLASet = DateTime.Now.Subtract(newdate == null ? DateTime.Now : newdate).TotalHours;
                item.SLA = Convert.ToInt32(Math.Floor(SLASet)); //int.Parse(SLASet.ToString());

                //var query = @"select b.CASEID,b.POLICYNO,b.WORKTYPE,c.CODE2 as CLAIMTYPE
                //              from clm_policy_pool b
                //                join clm_mapping c on b.worktype = c.code1
                //              where caseid = '" + item.CaseId + "'";

                var cek = (from b in _db.CLM_Mapping
                           join c in _db.CLM_Policy_Pool on b.Code1 equals c.WorkType
                           where c.CaseId == item.CaseId
                           select new PolicyMapping()
                           {
                               CaseId = c.CaseId,
                               PolicyNo = c.PolicyNo,
                               WorkType = c.WorkType,
                               ClaimType = b.Code2
                           }).FirstOrDefault();

                //DataTable cekClaimTab = Common.ExecuteQuery(query);
                //var policyMap = new PolicyMapping();
                if (cek != null)
                {
                    //policyMap = new PolicyMapping()
                    //{
                    //    CaseId = cekClaimTab.Rows[0]["CASEID"].ToString(),
                    //    PolicyNo = cekClaimTab.Rows[0]["POLICYNO"].ToString(),
                    //    WorkType = cekClaimTab.Rows[0]["WORKTYPE"].ToString(),
                    //    ClaimType = cekClaimTab.Rows[0]["CLAIMTYPE"].ToString()
                    //};

                    //check SLA and set colour from table
                    for (int j = 0; j < SLASetups.Count; j++)
                    {
                        if (SLASetups[j].ClaimType.Trim() == cek.ClaimType.Trim())
                        {
                            if ((item.SLA >= SLASetups[j].ParamTo) && (item.SLA <= SLASetups[j].ParamFrom))
                            {
                                item.ColorSLA = SLASetups[j].Color;
                                item.Category = SLASetups[j].Priority; //temporary 
                                break;
                            }
                        }
                    }
                    //only if SLA = 0
                    //if (item.ColorSLA == "")
                    //    item.ColorSLA = "#90EE90";

                    if (item.VIP == "YES") item.ColorSLA = "#f78181";
                }
            }
            //end SLA setup

            ViewBag.ListPolicyPool = tempResult.Where(x=>x.ClaimNo != null && x.ClaimNo != "" && x.Category == "Normal");

            List<SelectListItem> dataCategory = new List<SelectListItem>();
            dataCategory.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
            dataCategory.Add(new SelectListItem() { Text = "New Case", Value = "New Case" });
            dataCategory.Add(new SelectListItem() { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            ViewBag.ddlCategory = new SelectList(dataCategory, "Value", "Text", ddlCategory);

            _db.zsp_AuditTrailInsert("Claim Open Policy List", policyNumber, GetSession.UserName());

            return View();
        }

        // GET: DipcadClaimOpen/Details/5
        public ActionResult DetailClient(int? caseid)
        {
            if (caseid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string[] wType;

            var userName = GetSession.UserName();

            DipcadClaimOpenViewModels singleCLMOpen = GetListCLMbyID(caseid);

            if (singleCLMOpen == null)
            { return HttpNotFound(); }

            #region 'List'
            string dllCategoryVal = singleCLMOpen.CATEGORY;
            string dllWorktypeVal = singleCLMOpen.WORKTYPE;
            string dllPriorityVal = singleCLMOpen.PRIORITY;
            if (singleCLMOpen.VIP == "YES")
                dllPriorityVal = "3"; 
            //string dllCaseTypeVal = singleCLMOpen.CASE_TYPE;
            string dllPeReasonVal = singleCLMOpen.PENDING_REASON;
            //string dllAsignTo = getUserAssign(caseid);
            //string dllAsignTo = getUserAssign(singleCLMOpen.WORKTYPE);

            TempData["ddlWorktype"] = dllWorktypeVal;
            TempData["ddlCategory"] = dllCategoryVal;
            //TempData["AssignTo"] = dllAsignTo;

            List<SelectListItem> dataWorkType = new List<SelectListItem>();
            var workTypeList = GetListWorkType();            
            dataWorkType.Add(new SelectListItem() { Text = "", Value = "" });
            if (workTypeList != null)
            {
                foreach (string item in workTypeList)
                {
                    wType = item.Split('|');
                    dataWorkType.Add(new SelectListItem() { Text = wType[1], Value = wType[0] });
                }
            }
            string[] reasonType;
            List<SelectListItem> dataReasonPending = new List<SelectListItem>();
            var pendingReasonList = GetReferenceData("ReasonPending");
            dataReasonPending.Add(new SelectListItem() { Text = "", Value = "" });
            if (pendingReasonList != null)
            {
                foreach (string item in pendingReasonList)
                {
                    reasonType = item.Split('|');
                    dataReasonPending.Add(new SelectListItem() { Text = reasonType[1], Value = reasonType[0] });
                }
            }
            string[] assignOtherDiv;
            List<SelectListItem> dataOtherDiv = new List<SelectListItem>();
            var otherDivList = GetReferenceData("AssignToOtherDiv");
            dataOtherDiv.Add(new SelectListItem() { Text = "", Value = "" });
            if (otherDivList != null)
            {
                foreach (string item in otherDivList)
                {
                    assignOtherDiv = item.Split('|');
                    dataOtherDiv.Add(new SelectListItem() { Text = assignOtherDiv[1], Value = assignOtherDiv[0] });
                }
            }
            string[] priority;
            List<SelectListItem> dataPriority = new List<SelectListItem>();
            var priorityList = GetReferenceData("Priority");
            dataPriority.Add(new SelectListItem() { Text = "", Value = "" });
            if (priorityList != null)
            {
                foreach (string item in priorityList)
                {
                    priority = item.Split('|');
                    dataPriority.Add(new SelectListItem() { Text = priority[1], Value = priority[0] });
                }
            }
            //string[] caseType;
            //List<SelectListItem> dataCaseType = new List<SelectListItem>();
            //var caseTypeList = GetReferenceData("CaseType");
            //dataCaseType.Add(new SelectListItem() { Text = "", Value = "" });
            //if (caseTypeList != null)
            //{
            //    foreach (string item in caseTypeList)
            //    {
            //        caseType = item.Split('|');
            //        dataCaseType.Add(new SelectListItem() { Text = caseType[1], Value = caseType[0] });
            //    }
            //}

            ViewBag.dllWorktype = new SelectList(dataWorkType, "Value", "Text", dllWorktypeVal);
            ViewBag.dllPenReasonList = new SelectList(dataReasonPending, "Value", "Text", dllPeReasonVal);
            ViewBag.dllOtherDiv = new SelectList(dataOtherDiv, "Value", "Text");
            ViewBag.dllPriority = new SelectList(dataPriority, "Value", "Text", dllPriorityVal);
            //ViewBag.dllCaseType = new SelectList(dataCaseType, "Value", "Text", dllCaseTypeVal);

            List<SelectListItem> dataCategory = new List<SelectListItem>();
            dataCategory.Add(new SelectListItem() { Text = "New Case", Value = "New Case" });
            dataCategory.Add(new SelectListItem() { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            ViewBag.dllCategory = new SelectList(dataCategory, "Value", "Text", dllCategoryVal);

            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            //dataAssignTo.Add(new SelectListItem() { Text = "System RR", Value = "SystemRR" });
            var users = GetUserClaim();
            if (users != null)
            {
                foreach (var item in users)
                {
                    if (item.username == GetSession.UserName()) continue;
                    dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username });
                }
            }
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text");
            //ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", "Administrator");

            //set SLA
            var newdate = singleCLMOpen.CREATE_DATE;
            
            //var SLASet = DateTime.Now.Subtract(newdate == null ? DateTime.Now : newdate).  TotalHours;
            var SLASetWithoutWeekend = SubstractDateTimeWithoutWeekend(DateTime.Now, newdate);

            //if (singleCLMOpen.FINISHPENDATE != null)
            //{
            //    SLASet = (singleCLMOpen.FINISHPENDATE ?? DateTime.Now).Subtract(newdate == null ? DateTime.Now : newdate).TotalHours;
            //}
            singleCLMOpen.SLA = Convert.ToInt32(Math.Floor(SLASetWithoutWeekend)); //int.Parse(SLASet.ToString());
            
            int remainder; // for hours
            double quotient = Math.DivRem(singleCLMOpen.SLA, 24, out remainder); 
            TimeSpan result = TimeSpan.FromHours(singleCLMOpen.SLA); //day type double
            int x = (int)result.TotalDays; //day type int

            ViewBag.SLA = x + " Days " + remainder + " Hours";

            var cekVIP = _db.CLM_ReferenceData.Where(n => n.Type == "VIP").ToList();
            string VIPVal = "";
            foreach (var item in cekVIP)
            {
                if (singleCLMOpen.VIP == item.Code)
                {
                    VIPVal = item.Description;
                }
            }
            if (VIPVal == "") VIPVal = "NO";
            ViewBag.VIP = VIPVal;

            singleCLMOpen.PRODUCT_NAME = GetProductName(singleCLMOpen.PRODUCT_CODE);
            singleCLMOpen.DOCUMENT_TYPE = GetDocumentDescription(singleCLMOpen.DOCUMENT_TYPE);
            #endregion

            //cek role
            ViewBag.IsInRole = GetRoleMetrix(userName);

            _db.zsp_AuditTrailInsert("Claim Detail Client", singleCLMOpen.POLICY_NO, GetSession.UserName());

            return View(singleCLMOpen);
        }

        [HttpPost]
        public ActionResult DetailClient(DipcadClaimOpenViewModels ClaimDetail, string AssignTo, string AssignToOtherDiv, string ddlWorktype, string ddlCategory, string txRemarks, string dllPenReason)
        {

            string iscomplete = Convert.ToString(ClaimDetail.IS_COMPLETE).ToLower() == "false" ? "0" : "1";
            string isPending = Convert.ToString(ClaimDetail.IS_PENDING).ToLower() == "false" ? "0" : "1";
            string isPaid = Convert.ToString(ClaimDetail.IS_PAID).ToLower() == "false" ? "0" : "1";
            string isCompliance = Convert.ToString(ClaimDetail.IS_COMPLIANCE).ToLower() == "false" ? "0" : "1";
            string isReject = Convert.ToString(ClaimDetail.IS_REJECT).ToLower() == "false" ? "0" : "1";

            if (ClaimDetail.IS_PENDING)
            {
                if (dllPenReason == "" || dllPenReason == null)
                {
                    TempData["Error"] = $"Pending Reason Is Required";
                    return RedirectToAction("DetailClient", new { caseid = ClaimDetail.CASE_ID });
                }
            }

            //DataTable result = Common.ExecuteQuery($"dbo.sp_POS_Submit {ClaimDetail.CASE_ID},'{ClaimDetail.CIF}','{ClaimDetail.POLICY_NO}','{ddlWorktype}','{ddlCategory}','{GetSession.UserName()}','{AssignTo}','{txRemarks}','{iscomplete}','{ClaimDetail.REMINDER}','{(ClaimDetail.IS_PAID == true ? "1" : "0")}'");
            DataTable result = Common.ExecuteQuery($"dbo.SPUpd_CLM_Submit '{ClaimDetail.CASE_ID}','{ClaimDetail.POLICY_NO}','{ddlWorktype}','{iscomplete}','{ClaimDetail.REQPENDATE}','{ClaimDetail.FINISHPENDATE}','{isPending}','{dllPenReason}','{AssignTo}','{AssignToOtherDiv}','{txRemarks}','{isPaid}','{isCompliance}','{GetSession.UserName()}','{ClaimDetail.PRIORITY}','{ClaimDetail.CLAIMNO}','{ClaimDetail.RECLAIMNO}','{ClaimDetail.INCURED_DATE}','{isReject}','{ClaimDetail.CASE_TYPE}','{ClaimDetail.CLAIM_AMOUNT}','{ClaimDetail.REMINDERPENDATE}','{ClaimDetail.PENDING_LETTER}','{ClaimDetail.REJECT_LETTER}'");

            string cek = result.Rows[0][0].ToString();

            if (cek == "SUCCESS")
            {
                if (ClaimDetail.IS_COMPLETE)
                { _db.zsp_AuditTrailInsert("Claim Save as Complete", ClaimDetail.POLICY_NO, GetSession.UserName()); }
                else
                {
                    if (string.IsNullOrWhiteSpace(AssignTo))
                    { _db.zsp_AuditTrailInsert("Claim Assign User", ClaimDetail.POLICY_NO, GetSession.UserName()); }
                    else
                    {
                        _db.zsp_AuditTrailInsert("Claim Save Data", ClaimDetail.POLICY_NO, GetSession.UserName());
                    }
                }

                TempData["Success"] = $"{cek} change for Policy Number {ClaimDetail.POLICY_NO}";
            }
            else
            {
                TempData["Error"] = $"Error For : {cek}";
                return View(ClaimDetail);
            }

            return RedirectToAction("Index");
        }

        public ActionResult ImagesClient(int case_id)
        {
            string sourcePathTmp = string.Empty;
            string sourcePath = string.Empty;
            string filePath = string.Empty;
            var extension = string.Empty;
            Byte[] fileByte;

            DipcadClaimListImageViewModel model = new DipcadClaimListImageViewModel();
            List<DetailImageViewModels> images = new List<DetailImageViewModels>();
            //var imagesList = _db.sp_POS_Images(case_id, GetSession.UserName());
            List<ImageResult> imagesList = new List<ImageResult>();

            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Images '{case_id}',''");
            int counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                imagesList.Add(new ImageResult()
                {
                    POLICYNO = dTab.Rows[i]["POLICYNO"].ToString(),
                    FILENAMEORIGIN = dTab.Rows[i]["FILENAMEORIGIN"].ToString(),
                    FILENAME = dTab.Rows[i]["FILENAME"].ToString(),//.Replace(" ", "_"),
                    DOCUMENTNAME = dTab.Rows[i]["DOCUMENTNAME"].ToString(),
                    SOURCE = dTab.Rows[i]["SOURCE"].ToString(),
                    FILEIMAGE = (Byte[])dTab.Rows[i]["FILEIMAGE"],
                    CREATEDATE = Convert.ToDateTime(dTab.Rows[i]["CREATEDATE"].ToString()),
                    DESCRIPTION = dTab.Rows[i]["DESCRIPTION"].ToString(),
                });
            }



            foreach (var item in imagesList)
            {
                int j = 0;
                sourcePathTmp = Server.MapPath("~/Files/" + item.POLICYNO);
                sourcePath = Server.MapPath("~/Files/" + item.POLICYNO + "/");
                if (model.PolicyNumber == null || model.PolicyNumber == string.Empty)
                    model.PolicyNumber = item.POLICYNO;
                var ext = Path.GetExtension(item.FILENAME);
                if (!Directory.Exists(sourcePath))
                {
                    Directory.CreateDirectory(sourcePath);
                }
                else
                {
                    filePath = sourcePath + item.FILENAME;
                    System.IO.File.Delete(filePath);
                }

                if (ext == ".tif" || ext == ".tiff")
                {
                    using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        fileByte = inStream.ToArray();
                        string bytes = BitConverter.ToString(fileByte).Replace("-", "");
                    }
                }
                else
                {
                    fileByte = item.FILEIMAGE;
                }

                byte[] data = (byte[])fileByte;

                string[] fileName = item.FILENAME.Split('.');
                filePath = sourcePath + fileName[0].ToString();

                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                //if (ext == ".jpg" || ext == ".jpeg")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);
                }
                //else if (ext == ".tif" || ext == ".tiff")
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".tiff", data);
                }
                else
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".pdf", data);
                }

                //if (ext == ".jpg" || ext == ".jpeg")
                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                {
                    extension = "image/jpg";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";
                }
                //else if (ext == ".tif" || ext == ".tiff")
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    #region ' Convert tiff to PDF '
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                    using (var stream = new MemoryStream())
                    {
                        using (var docs = new Document())
                        {
                            try
                            {
                                var writer =
                                iTextSharp.text.pdf.PdfWriter.GetInstance(docs, new FileStream(sourcePath + fileName[0] + ".pdf", FileMode.Create));

                                //count files images in tiff
                                Bitmap bMap = new Bitmap(sourcePath + fileName[0] + ".tiff");
                                //Bitmap bMap = new Bitmap(sourcePath + item.FILENAME);
                                int total = bMap.GetFrameCount(FrameDimension.Page);

                                docs.Open();

                                iTextSharp.text.pdf.PdfContentByte cbyte = writer.DirectContent;
                                for (int k = 0; k < total; ++k)
                                {
                                    bMap.SelectActiveFrame(FrameDimension.Page, k);
                                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bMap, ImageFormat.Bmp);

                                    //calculate dimension
                                    img.ScalePercent(50f / img.DpiX * 100);
                                    img.SetAbsolutePosition(0, 0);
                                    cbyte.AddImage(img);
                                    docs.NewPage();
                                }

                                docs.Close();
                                bMap.Dispose();

                                FileInfo dirInfo = new FileInfo(sourcePath + fileName[0] + ".tiff");
                                if (item.SOURCE != "ADDITIONAL")
                                    dirInfo.Delete();
                            }
                            catch
                            {
                                TempData["Error"] = "Image Corrupt CaseId : "+ case_id;
                                return RedirectToAction("Index");
                            }                            
                        }
                    }
                    #endregion
                }
                //else if (ext == ".pdf")
                else if (Common.GetStringRight(item.FILENAME, 3) == "pdf")
                {
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                }

                images.Add(new DetailImageViewModels()
                {
                    PolicyNumber = item.POLICYNO,
                    Checked = false,
                    DocumentName = item.DOCUMENTNAME,
                    FILENAME = item.FILENAME,
                    FILENAMEORIGIN = item.FILENAMEORIGIN,
                    Source = item.SOURCE,
                    FILEIMAGE = fileByte,
                    Extension = extension,
                    PreviewContentId = Common.SplitValueFileName(item.FILENAME),
                    FILEIMAGE_temp = item.FILEIMAGE,
                    FilePath = filePath,
                    CreatedDate = Convert.ToDateTime(item.CREATEDATE),
                    Description = item.DESCRIPTION
                });
                j++;
            }

            model.Case_ID = case_id;
            model.DetailImageViewModels = images;

            string[] docsSplit;
            List<SelectListItem> dataDocumentType = new List<SelectListItem>();
            var documentTypeList = GetListDocumentType();
            dataDocumentType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentTypeList != null)
            {
                foreach (string item in documentTypeList)
                {
                    docsSplit = item.Split('|');
                    dataDocumentType.Add(new SelectListItem() { Text = docsSplit[3], Value = item });
                }
            }
            ViewBag.dllDocType = new SelectList(dataDocumentType, "Value", "Text", null);

            return View(model);
        }

        [HttpPost]
        public ActionResult ImagesClient(DipcadClaimListImageViewModel images, string Submit, string dllDocType, HttpPostedFileBase mdlImageFile, string workType)
        {
            try
            {
                string policyNo = string.Empty;
                string sourcePathTmp = string.Empty;
                string sourcePath = string.Empty;
                string filePath = string.Empty;
                string zipName = string.Empty;
                int count = 0;

                string[] arrFileName;
                string fileName = string.Empty;

                if (Submit == "Add Image")
                {
                    if(mdlImageFile != null)
                    {

                        string[] ddlDocType = dllDocType.Split('|');
                        int docIdValue = int.Parse(ddlDocType[0]);
                        string docTitleValue = ddlDocType[2];
                        string docTypeValue = ddlDocType[1];

                        byte[] data;
                        using (Stream inputStream = mdlImageFile.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            data = memoryStream.ToArray();
                        }

                        if (workType == null)
                        {
                            workType = GetWorkType(images.Case_ID);
                        }

                        try
                        {
                            //Common.ExecuteNonQuery( status );
                            Common.InsertMSTImageAdditional(images.PolicyNumber
                                                            , mdlImageFile.FileName.Replace(" ","_")
                                                            , data
                                                            , images.Case_ID
                                                            , "CLAIM"
                                                            , docIdValue
                                                            , docTitleValue
                                                            , docTypeValue
                                                            , workType
                                                            , images.Description
                                                            , "ADDITIONAL");

                        }
                        catch (Exception err)
                        {
                            TempData["Error"] = err.Message;
                            goto End;
                        }
                        TempData["Success"] = "Add image for " + images.PolicyNumber + " successed";
                    }
                }
                else
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        foreach (var item in images.DetailImageViewModels)
                        {
                            if (item.Checked)
                            {
                                count++;
                                if (policyNo == string.Empty)
                                { policyNo = item.PolicyNumber; }

                                sourcePathTmp = Server.MapPath("~/Files/" + item.PolicyNumber);
                                sourcePath = Server.MapPath("~/Files/" + item.PolicyNumber + "/");
                                if (!Directory.Exists(sourcePath))
                                {
                                    Directory.CreateDirectory(sourcePath);
                                }

                                DataTable dTab = Common.ExecuteQuery($"[dbo].[SPInq_CLM_DetailImageViewTmp] '{item.PolicyNumber}','{item.FILENAMEORIGIN}'");
                                DetailImageTempView dataImage = new DetailImageTempView();

                                if (dTab.Rows.Count > 0)
                                {
                                    dataImage.FileImage = (Byte[])dTab.Rows[0]["FILEIMAGE"];
                                }

                                //var fileImageTmp = _db.SPInq_CLM_DetailImageViewTmp(item.PolicyNumber, item.FILENAMEORIGIN).ToList().Single().FILEIMAGE;
                                var fileImageTmp = dataImage.FileImage;
                                //byte[] data = (byte[])item.FILEIMAGE_temp;
                                byte[] data = (byte[])fileImageTmp;

                                System.IO.File.WriteAllBytes(sourcePath + item.FILENAME, data);

                                filePath = sourcePath + item.FILENAME;
                                zip.AddFile(filePath, "");
                            }
                        }

                        if (count == 0)
                        {
                            TempData["Error"] = "Please check before submit";
                            goto End;
                        }

                        _db.zsp_AuditTrailInsert("Download Image Open Claim", policyNo, GetSession.UserName());

                        Response.Clear();
                        Response.BufferOutput = false;
                        zipName = $"{policyNo}_{DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")}.zip";
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        Response.End();
                    }

                    foreach (var item in images.DetailImageViewModels)
                    {
                        if (item.Checked)
                        {
                            filePath = sourcePath + item.FILENAME;
                            System.IO.File.Delete(filePath);
                            if (item.Extension == "image/jpg")
                            {
                                arrFileName = item.FILENAME.Split('.');
                                fileName = arrFileName[0] + ".jpg";
                                filePath = sourcePath + fileName;
                                System.IO.File.Delete(filePath);
                            }
                        }
                    }
                    Directory.Delete(sourcePath);
                }                
            }
            catch (Exception ex)
            {
                throw;
            }
        End:
            string[] docsSplit;
            List<SelectListItem> dataDocumentType = new List<SelectListItem>();
            var documentTypeList = GetListDocumentType();
            dataDocumentType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentTypeList != null)
            {
                foreach (string item in documentTypeList)
                {
                    docsSplit = item.Split('|');
                    dataDocumentType.Add(new SelectListItem() { Text = docsSplit[3], Value = item });
                }
            }
            ViewBag.dllDocType = new SelectList(dataDocumentType, "Value", "Text", null);
            //return View(images);
            return RedirectToAction("ImagesClient", new { case_id = images.Case_ID });
        }

        //public ActionResult AddImage(DipcadClaimListImageViewModel models, string Submit, string ddlDocumentType, HttpPostedFileBase mdlImageFile, string workType)
        //{
        //    if (Submit == "Submit" || Submit == "Update")
        //    {

        //        //Adding save
        //        if (models.DetailImageViewModels == null)
        //        {
        //            TempData["Error"] = "Please add images/pdf file.";
        //            //goto Here;
        //        }
        //        ReportingImageEntities _db = new ReportingImageEntities();
        //        string status = string.Empty;
        //        //string[] worktypes = models.WORKTYPE.Split('.');
        //        //models.WORKTYPE = worktypes[1];
        //        //models.WorkType_id = worktypes[0];

        //        string[] ddlDocType = ddlDocumentType.Split('|');
        //        int docIdValue = int.Parse(ddlDocType[0]);
        //        string docTitleValue = ddlDocType[1];
        //        string docTypeValue = ddlDocType[2];

        //        #region ' save data '
        //        //image
        //        foreach (DetailImageViewModels images in models.DetailImageViewModels)
        //        {
        //            if (!System.IO.File.Exists($"{images.FilePath}"))
        //            { 
        //                TempData["Error"] = $"File {images.FILENAME} not found"; 
        //                //goto Here; 
        //            }

        //            else if (images.Status == "ADD")
        //            {
        //                //images.FILENAME = $"{Common.ExecuteQuery($"dbo.sp_POS_Image_setFileName '{models.PolicyNumber}','{images.DocumentName}','{images.Extension}'").Rows[0][0].ToString()};{models.WorkType_id}";
        //                images.FILENAME = ("doc_"+DateTime.Now).Replace(" ","_");

        //                try
        //                {
        //                    //Common.ExecuteNonQuery( status );
        //                    Common.InsertMSTImageAdditional(models.PolicyNumber
        //                                                    , images.FILENAME
        //                                                    , System.IO.File.ReadAllBytes(images.FilePath)
        //                                                    , models.Case_ID
        //                                                    , "CLAIM"
        //                                                    , docIdValue
        //                                                    , docTitleValue
        //                                                    , docTypeValue
        //                                                    , workType
        //                                                    , models.Description);

        //                }
        //                catch (Exception err)
        //                {
        //                    TempData["Error"] = err.Message;
        //                    goto Here;
        //                }
        //                TempData["Success"] = "Add image for " + models.PolicyNumber + " successed";
        //            }

        //        }
        //        //data
        //        //status = Common.ExecuteQuery($"dbo.sp_POS_AddHoc_Save '{models.CASE_ID}','{models.CIF}','{models.POLICY_NO}','{models.WORKTYPE_ID}','{models.WorkType_id}','{models.CATEGORY}','{models.RLS_TRANS}','{models.RLS_CAPTURE_DATE}','{GetSession.UserName()}','{models.USER_ASSIGN}','{models.HOLDER}','Y'").Rows[0][0].ToString();
        //        //if (status != "Success")
        //        //{
        //        //    TempData["Error"] = status;
        //        //    //goto Here;
        //        //}
        //        //else
        //        //{
        //        //    TempData["Success"] = $"Change for Policy Number {models.POLICY_NO}";
        //        //}


        //        #endregion
        //    }

        //    Here:
        //    return RedirectToAction("ImagesClient", new { case_id = models.Case_ID });
        //}

        //------------------------------------------------------------------ attribut non action ---------------------------------

        public static List<DipcadClaimOpenViewModels> GetPolicyPoolList(string policyNumber, string user, string ddlCategory, string statusComplete, int pageNumber)
        {
            List<DipcadClaimOpenViewModels> models = new List<DipcadClaimOpenViewModels>();
            int counter = 0;

            if (ddlCategory == "ALL") ddlCategory = "";
            //ex exec dbo.SPInq_CLM_PolicyPool '1','10','and UserAssign = ''Budi Sudarsono''','','1'
            var userassign = "and UserAssign = ''" + GetSession.UserName() + "'' and IsComplete is null";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_PolicyPool '{pageNumber}','10','{userassign}','','1'");
            counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                models.Add(new DipcadClaimOpenViewModels()
                {
                    SEQ = Convert.ToInt32(dTab.Rows[i]["SEQ"].ToString()),
                    CASE_ID = Convert.ToInt32(dTab.Rows[i]["CaseId"].ToString()),
                    //CIF = dTab.Rows[i]["CIF"] == null ? string.Empty : dTab.Rows[i]["CIF"].ToString(),
                    POLICY_NO = dTab.Rows[i]["PolicyNo"].ToString(),
                    SOURCE = dTab.Rows[i]["Source"].ToString(),
                    //WORKTYPE_ID = dTab.Rows[i]["WORKTYPE_ID"].ToString(),
                    WORKTYPE = dTab.Rows[i]["WorkType"].ToString(),
                    CATEGORY = dTab.Rows[i]["Category"].ToString(),
                    RLS_TRANS = dTab.Rows[i]["RLSTRANS"].ToString(),
                    RLS_CAPTURE_DATE = String.IsNullOrWhiteSpace(dTab.Rows[i]["RLSCaptureDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["RLSCaptureDate"].ToString()),
                    USER_ASSIGN = dTab.Rows[i]["UserAssign"].ToString(),
                    CREATE_DATE = Convert.ToDateTime(dTab.Rows[i]["CreatedDate"].ToString()),
                    //REMARKS = dTab.Rows[i]["REMARKS"].ToString(),
                    //COUNTS = counter
                });
            }

            return models;
        }

        public static DipcadClaimOpenViewModels GetListCLMbyID(int? case_id)
        {
            DipcadClaimOpenViewModels model = null;

            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail '{case_id}','{GetSession.UserName()}'"); 
            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail '29',''");
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Detail '{case_id}','{GetSession.UserName()}'");

            int count = dTab.Rows.Count;
            if (count > 0)
            {
                //if (!String.IsNullOrWhiteSpace(dTab.Rows[0]["DATE_COMPLETE"].ToString()))
                //{ date_comp = Convert.ToDateTime(dTab.Rows[0]["DATE_COMPLETE"].ToString()); }

                DataRow drow = dTab.Rows[0];

                model = new DipcadClaimOpenViewModels()
                {
                    CASE_ID = Convert.ToInt32(dTab.Rows[0]["CaseId"].ToString()),
                    //CIF = dTab.Rows[0]["CIF"].ToString(),
                    POLICY_NO = dTab.Rows[0]["PolicyNo"].ToString(),
                    SOURCE = dTab.Rows[0]["Source"].ToString(),
                    WORKTYPE = dTab.Rows[0]["WorkType"].ToString(),
                    CATEGORY = dTab.Rows[0]["Category"].ToString(),
                    RLS_TRANS = dTab.Rows[0]["RLSTrans"].ToString(),
                    RLS_CAPTURE_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["RLSCaptureDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["RLSCaptureDate"].ToString()),
                    USER_ASSIGN = dTab.Rows[0]["UserAssign"].ToString(),
                    CREATE_DATE = Convert.ToDateTime(dTab.Rows[0]["CreatedDate"].ToString()),
                    DATE_COMPLETE = String.IsNullOrWhiteSpace(dTab.Rows[0]["CompleteDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["CompleteDate"].ToString()),
                    SENT_TO_COMPLIENCE = false,
                    //SLA = Convert.ToInt32(dTab.Rows[0]["SLA"].ToString()),
                    //AGINGSLA = Convert.ToInt32(dTab.Rows[0]["AGINGSLA"].ToString()),
                    HOLDER = dTab.Rows[0]["Holder"].ToString(),
                    IS_COMPLETE = dTab.Rows[0]["IsComplete"].ToString().ToLower() == "true" ? true : false,
                    IS_PAID = dTab.Rows[0]["IsPaid"].ToString().ToLower() == "true" ? true : false,
                    IS_PENDING = dTab.Rows[0]["IsPending"].ToString().ToLower() == "true" ? true : false,
                    IS_COMPLIANCE = dTab.Rows[0]["IsCompliance"].ToString().ToLower() == "true" ? true : false,
                    PRIORITY = dTab.Rows[0]["Priority"].ToString(),
                    VIP = dTab.Rows[0]["VIP"].ToString(),
                    CASE_TYPE = dTab.Rows[0]["CaseType"].ToString(),
                    INCURED_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["IncuredDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["IncuredDate"].ToString()),
                    IS_REJECT = dTab.Rows[0]["IsReject"].ToString().ToLower() == "true" ? true : false,
                    RECLAIMNO = dTab.Rows[0]["ReClaimNo"].ToString(),
                    CLAIMNO = dTab.Rows[0]["ClaimNo"].ToString(),
                    OWNER_NAME = dTab.Rows[0]["OwnerName"].ToString(),
                    OWNER_DOB = String.IsNullOrWhiteSpace(dTab.Rows[0]["OwnerDOB"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["OwnerDOB"].ToString()),
                    MEMBER_NO = dTab.Rows[0]["MemberNo"].ToString(),
                    MEMBER_NAME = dTab.Rows[0]["MemberName"].ToString(),
                    PRIMARY_DOC = dTab.Rows[0]["PrimaryDoc"].ToString(),
                    MEMBER_ID = dTab.Rows[0]["MemberId"].ToString(),
                    PRODUCT_CODE = dTab.Rows[0]["ProductCode"].ToString(),
                    DOC_SOURCE_TYPE = dTab.Rows[0]["DocumentSourceType"].ToString(),
                    //TSA use SumInsured Coloumn
                    TSA = dTab.Rows[0]["SumInsured"].ToString(),
                    PRODUCT_NAME = dTab.Rows[0]["ProductName"].ToString(),
                    PRODUCT_TYPE = dTab.Rows[0]["ProductType"].ToString(),
                    DOCUMENT_TITLE = dTab.Rows[0]["DocumentTitle"].ToString(),
                    DOCUMENT_TYPE = dTab.Rows[0]["DocumentType"].ToString(),
                    CHANNEL = dTab.Rows[0]["Channel"].ToString(),
                    PENDING_REASON = dTab.Rows[0]["ReasonPending"].ToString(),
                    FINISHPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["FinishPendingDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["FinishPendingDate"].ToString()),
                    REQPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["RequestPendingDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["RequestPendingDate"].ToString()),
                    REMINDERPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["ReminderPendDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["ReminderPendDate"].ToString()),
                    RECEIVED_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["ReceivedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["ReceivedDate"].ToString()),
                    OWNER_ID = dTab.Rows[0]["OwnerId"].ToString(),
                    PENDING_LETTER = dTab.Rows[0]["PendingLetter"].ToString(),
                    REJECT_LETTER = dTab.Rows[0]["RejectLetter"].ToString(),

                    COUNTS = 1
                };
                if (dTab.Rows[0]["ClaimAmount"].ToString() != "")
                    model.CLAIM_AMOUNT = Convert.ToInt32(dTab.Rows[0]["ClaimAmount"].ToString());


                DataTable dTabAuditHistories = Common.ExecuteQuery($"dbo.SPInq_CLM_AuditJobHistory '{case_id}',''");
                
                var auditHistories = new List<AuditJobHistory>();
                if (dTabAuditHistories.Rows.Count > 0)
                {
                    for (int i = 0; i < dTabAuditHistories.Rows.Count; i++)
                    {
                        var auditHist = new AuditJobHistory();
                        auditHist.Action = dTabAuditHistories.Rows[i]["JOB"].ToString();
                        auditHist.Date = String.IsNullOrWhiteSpace(dTabAuditHistories.Rows[i]["DATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTabAuditHistories.Rows[i]["DATE"].ToString());
                        auditHist.User = dTabAuditHistories.Rows[i]["USER"].ToString();
                        auditHist.Comment = dTabAuditHistories.Rows[i]["COMMENT"].ToString();
                        auditHist.Value = dTabAuditHistories.Rows[i]["VALUE"].ToString();
                        auditHistories.Add(auditHist);
                    }
                }
                else
                {
                    var auditHist = new AuditJobHistory();
                    auditHist.Action = " ";
                    auditHist.User = " ";
                    auditHist.Comment = " ";
                    auditHistories.Add(auditHist);
                }
                model.AuditJobHistories = auditHistories;
            }

            return model;
        }

        private string getUserAssign(string worktype)
        {
            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail_AssignFrom {caseid}");
            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail_AssignFrom 29"); ///
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Detail_AssignFrom '{worktype}'");
            string userfrom = string.Empty;

            foreach (DataRow item in dTab.Rows)
            {
                userfrom = item[0].ToString();
            }

            return userfrom;
        }

        public List<string> GetListWorkType()
        {
            List<string> workType = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.SPInq_CLM_Worktype");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    workType.Add($"{rows[2].ToString()}|{rows[1].ToString()}");
                }
            }

            return workType;
        }

        public List<string> GetReferenceData(string referenceDataType)
        {
            List<string> penReasonType = new List<string>();
            var type = "type = ''"+ referenceDataType + "'' and status = 1";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_ReferenceData 1, 10, '{type}', '', '1'");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    penReasonType.Add($"{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return penReasonType;
        }

        public List<UserView> GetUserClaim()
        {
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_getUserActive");
            List<UserView> usr = new List<UserView>();

            foreach (DataRow item in dTab.Rows)
            {
                if (item["USERNAME"].ToString() == GetSession.UserName()) continue;
                if (item["PREVILAGE_NAME"].ToString() == "Associate") continue;
                usr.Add(new UserView() { username = item["USERNAME"].ToString() });
            }

            return usr;
        }

        public string GetProductName(string productCode)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from zmaster_product where product_code = '{productCode}'");
            var productName = ""; 
            if(dTab.Rows.Count > 0)
                productName = dTab.Rows[0]["product_name"].ToString();
            return productName;
        }

        public string GetDocumentDescription(string docCode)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from clm_referencedata where code = '{docCode}'");
            var documentName = "";
            if (dTab.Rows.Count > 0)
                documentName = dTab.Rows[0]["Description"].ToString();
            return documentName;
        }

        public string GetRoleMetrix(string username)
        {
            //cek mapping role 
            DataTable dTabMap = Common.ExecuteQuery($"[SPInq_CLM_RolesMapping] '{GetSession.UserName()}','{thisMenu}'");

            var rolemenu = new RoleMenuMapping();
            if (dTabMap.Rows.Count > 0)
            {
                for (int i = 0; i < dTabMap.Rows.Count; i++)
                {
                    
                    rolemenu.Role = dTabMap.Rows[0]["Role"].ToString();
                    rolemenu.Menu = dTabMap.Rows[0]["Menu"].ToString();
                    rolemenu.IsReadOnly = dTabMap.Rows[0]["IsReadOnly"].ToString().ToLower() == "true" ? true : false;
                    rolemenu.IsFullAccess = dTabMap.Rows[0]["IsFullAccess"].ToString().ToLower() == "true" ? true : false;
                }
            }

            //1 fullaccess, 0 readonly
            if (rolemenu != null)
            {
                if (rolemenu.IsFullAccess == true)
                    return "1";
                else
                    return "0";
            }
            else
                return "0";
        }

        public double SubstractDateTimeWithoutWeekend(DateTime endDate, DateTime startDate)
        {
            //hitung stardate hari per hari
            //cek dia hari apa, klo misal hari sabtu atau minggu, jangan masukan di hitungan jam
            double hours = 0.0;
            //double cekcekHours = 0.0;
            DateTime newdatePlusOne = default(DateTime);
            DateTime cektggl = startDate;

            //kurangin tanggal tersebut dari holiday setup
            var holidaySetup = GetHolidaySetup(startDate, endDate);

            while (endDate.AddDays(1) > startDate)
            {
                if (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    //cekcekHours = endDate.Subtract(newdatePlusOne).TotalHours;
                    startDate = startDate.AddDays(1);
                }
                else
                {
                    if (cektggl == startDate)
                    {
                        startDate = startDate.AddDays(1);
                    }
                    else
                    {
                        //for (int i = 0; i < holidaySetup.Count; i++)
                        //{
                        //    if (startDate.Date == holidaySetup[i].HolidayDate.Date)
                        //    {
                        //        startDate = startDate.AddDays(1);
                        //    }
                        //    else
                        //    {
                                newdatePlusOne = startDate.AddDays(1);
                                //cekcekHours = endDate.Subtract(newdatePlusOne).TotalHours;
                                hours = hours + newdatePlusOne.Subtract(startDate).TotalHours;
                                startDate = startDate.AddDays(1);
                        //    }
                        //}
                    }
                }
            }
            int minusHoliday = 0;
            if (holidaySetup.Count > 1)
            {
                foreach (var item in holidaySetup)
                {
                    if (!(item.HolidayDate.DayOfWeek == DayOfWeek.Saturday && item.HolidayDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        if (!(item.HolidayDate.Date == cektggl.Date))
                        {
                            minusHoliday = minusHoliday  + (-24);
                        }
                    }
                }
            }
            else
            {
                if (!(holidaySetup[0].HolidayDate.DayOfWeek == DayOfWeek.Saturday && holidaySetup[0].HolidayDate.DayOfWeek == DayOfWeek.Sunday))
                {
                    if(holidaySetup[0].HolidayDate.Date != cektggl.Date)
                        minusHoliday = -24;
                }
            }

            //int cekHours = Convert.ToInt32(Math.Floor(hours + (cekcekHours*-1)));
            newdatePlusOne = newdatePlusOne.AddDays(-1);
            var hoursDiff = endDate.Subtract(newdatePlusOne).TotalHours;
            if (endDate.DayOfWeek == DayOfWeek.Saturday || endDate.DayOfWeek == DayOfWeek.Sunday)
                hoursDiff = 0.0;
            double cekHours = 0;
            if (hoursDiff == 0)
                cekHours = Convert.ToInt32(Math.Floor(hours));
            else
                cekHours = Convert.ToInt32(Math.Floor(hours + hoursDiff));
            cekHours = cekHours + minusHoliday;
            return cekHours;
        }

        public List<string> GetListDocumentType()
        {
            List<string> DocumentTipe = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.[SPInq_CLM_Document_Type]");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    DocumentTipe.Add($"{rows[0].ToString()}|{rows[1].ToString()}|{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return DocumentTipe;
        }

        public string GetWorkType(int caseid)
        {
            DataTable dTab = Common.ExecuteQuery($"select top 1 WorkType from clm_policy_pool where caseid = '{caseid}'");
            var worktype = "";
            if (dTab.Rows.Count > 0)
                worktype = dTab.Rows[0]["WorkType"].ToString();
            return worktype;
        }

        public List<HolidayList> GetHolidaySetup(DateTime stardate, DateTime enddate)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from HolidaySetup order by HolidayDate");
           
            var holidayList = new List<HolidayList>();
            if (dTab.Rows.Count > 0)
            {
                for (int i = 0; i < dTab.Rows.Count; i++)
                {
                    var dateholiday = new HolidayList();
                    dateholiday.HolidayDate = Convert.ToDateTime(dTab.Rows[i]["HolidayDate"]);
                    holidayList.Add(dateholiday);
                }
            }

            holidayList = holidayList.Where(x => x.HolidayDate >= stardate).ToList();
            holidayList = holidayList.Where(x => x.HolidayDate <= enddate).ToList();
            return holidayList;
        }


    }
}
