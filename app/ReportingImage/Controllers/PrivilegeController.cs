﻿using ReportingImage.App_Start;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class PrivilegeController : Controller
    {
        // GET: Privilege
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "Privilege")]
        public ActionResult Index()
        {
            string nameLogin = GetSession.UserName();
            List<PrivilegeView> Data = new List<PrivilegeView>();
            Data = _db.PrivilegeViews.ToList();
            _db.zsp_AuditTrailInsert("Privilege", null, nameLogin);
            
                      
            return View(Data);
        }

        [CheckAuthorize(Roles = "Create Privilege")]
        public ActionResult Create()
        {
            PrivilegeViewModels DP = new ViewModels.PrivilegeViewModels();
            DP.Parent = null;
            DP.Menu = DTMenu(0, GetSession.Privilege_ID());

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Create Privilege", null, userName);

            return View(DP);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Create Privilege")]
        public ActionResult Create(PrivilegeViewModels Privilege)
        {
            if (Privilege.Parent.previlage_name == null) { ModelState.AddModelError("privilege_name", "This field is required."); }
            //check data in database if already exist skip
            var CheckData = _db.zPrevilages.Where(x => x.previlage_name == Privilege.Parent.previlage_name).ToList();
            if (CheckData.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_PrivilegeInsert(GetSession.UserName(), Privilege.Parent.previlage_name);

                    var privilegeId = _db.zPrevilages.Where(a => a.previlage_name == Privilege.Parent.previlage_name).Single().id;

                    SaveMenu(Privilege.Menu, privilegeId);
                    _db.zsp_AuditTrailInsert("Create Privilege", null, GetSession.UserName());

                    var userName = GetSession.UserName();
                    _db.zsp_AuditTrailInsert("Create Privilege", null, userName);

                    TempData["Success"] = "Success saving Data for " + Privilege.Parent.previlage_name;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Name :" + Privilege.Parent.previlage_name + " already exist!";
            }

            return View(Privilege);
        }

        [CheckAuthorize(Roles = "Update Privilege")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrivilegeViewModels DP = new ViewModels.PrivilegeViewModels();
            DP.Parent = _db.zPrevilages.Find(id);

            DP.Menu = DTMenuEdit(0, id, GetSession.Privilege_ID());

            if (DP.Parent == null)
            {
                return HttpNotFound();
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Update Privilege", null, userName);

            return View(DP);
        }

        // POST: Privilege/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Update Privilege")]
        public ActionResult Edit(PrivilegeViewModels Privilege)
        {
            if (Privilege.Parent.previlage_name == null) { ModelState.AddModelError("privilege_name", "This field is required."); }
            //check data in database if already exist skip
            var CheckData = _db.zPrevilages.Where(x => x.previlage_name == Privilege.Parent.previlage_name && x.id != Privilege.Parent.id).ToList();
            if (CheckData.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_PrivilegeUpdate(Privilege.Parent.id, GetSession.UserName(), Privilege.Parent.previlage_name, false);
                    var dt = (from c in _db.zRole_Menu
                              where c.previlage_id == Privilege.Parent.id
                              select c).ToList();
                    foreach (var item in dt)
                    {
                        _db.zsp_RoleMenuUpdate(item.id, GetSession.UserName(), item.previlage_id, item.menu_id, item.is_read);
                    }
                    SaveMenu(Privilege.Menu, Privilege.Parent.id);
                    _db.zsp_AuditTrailInsert("Update Privilege", null, GetSession.UserName());
                    TempData["Success"] = "Success Editing Data for " + Privilege.Parent.previlage_name;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Name :" + Privilege.Parent.previlage_name + " already exist!";
            }
         
            return View(Privilege);
        }
        public List<MenuViewModels> DTMenuEdit(int? ParentIDValue, int? Privilege_ID, int Privilege_ID_ME)
        {
            List<MenuViewModels> DATA = new List<MenuViewModels>();
            List<zMenu> DT = new List<zMenu>();
            DT = GetPrivilegeTree(Privilege_ID_ME, Privilege_ID, ParentIDValue, GetSession.IsAdministrator());
            foreach (zMenu itemMenu in DT)
            {
                PrivilegeController MeMC = new PrivilegeController();
                MenuViewModels Menu = new MenuViewModels();
                Menu.Parent = itemMenu;
                Menu.Checked = Convert.ToBoolean(itemMenu.is_read);
                Menu.Child = MeMC.DTMenuEdit(Convert.ToInt32(itemMenu.id), Privilege_ID, Privilege_ID_ME);
                DATA.Add(Menu);
            }

            return DATA;
        }

        public List<zMenu> GetPrivilegeTree(int privilege_id_me, int? privilege_id, int? parent_id, Boolean is_administrator)
        {
            List<zMenu> Result;
            using (var ctx = new ReportingImageEntities())
            {
                Result = ctx.Database.SqlQuery<zMenu>("exec zsp_GetPrivilegeTree @privilege_id_me,@privilege_id,@parent_id,@is_administrator ",
                    new SqlParameter("@privilege_id_me", privilege_id_me),
                     new SqlParameter("@privilege_id", privilege_id),
                     new SqlParameter("@parent_id", parent_id),
                      new SqlParameter("@is_administrator", is_administrator)).ToList();
            }
            return Result;
        }
        public List<MenuViewModels> DTMenu(int? ParentIDValue, int? Privilege_ID)
        {

            List<MenuViewModels> DATA = new List<MenuViewModels>();
            List<zMenu> DT = new List<zMenu>();
            if (GetSession.IsAdministrator())
            {
                DT = (from c in _db.zMenus
                      where c.ParentID == ParentIDValue
                      select c).OrderBy(x => x.Sort).ToList();

            }
            else
            {
                var DTH = (from c in _db.zMenus
                           join b in _db.zRole_Menu on c.id equals b.menu_id
                           where c.ParentID == ParentIDValue && b.previlage_id == Privilege_ID
                           select new { c, b }).OrderBy(x => x.c.Sort).ToList();

                foreach (var itm in DTH)
                {
                    var H = new zMenu
                    {
                        id = itm.c.id,
                        Action = itm.c.Action,
                        Code = itm.c.Code,
                        Control = itm.c.Control,
                        Description = itm.c.Description,
                        Icon = itm.c.Icon,
                        Menu = itm.c.Menu,
                        ParentID = itm.c.ParentID,
                        ShowMenu = itm.c.ShowMenu,
                        Sort = itm.c.Sort
                    };
                    DT.Add(H);
                }

            }

            foreach (zMenu itemMenu in DT)
            {
                PrivilegeController MeMC = new PrivilegeController();
                MenuViewModels menuViewModels = new MenuViewModels();
                menuViewModels.Checked = false;
                menuViewModels.Parent = itemMenu;
                menuViewModels.Child = MeMC.DTMenu(Convert.ToInt32(itemMenu.id), Privilege_ID);
                DATA.Add(menuViewModels);
            }

            return DATA;
        }

        public List<MenuViewModels> SaveMenu(List<MenuViewModels> DataMenu, int Privilege_id)
        {
            if (DataMenu != null)
            {
                foreach (var item in DataMenu)
                {
                    ReportingImageEntities _dbNew = new ReportingImageEntities();
                    _dbNew.zsp_RoleMenuInsert(GetSession.UserName(), Privilege_id, item.Parent.id, item.Checked);
                    SaveMenu(item.Child, Privilege_id);
                }
            }

            return DataMenu;
        }
    }
}