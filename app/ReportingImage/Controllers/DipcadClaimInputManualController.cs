﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using RestSharp;

namespace ReportingImage.Controllers
{
    public class DipcadClaimInputManualController : Controller
    {
        GetSession MeGetSession = new GetSession();
        ReportingImageEntities _db = new ReportingImageEntities();
        public readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects };

        // GET: DipcadClaimInputManual
        public ActionResult Index(ClaimManual models, int caseid = 0)
        {
            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = "RR", Value = "NULL" });
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", "NULL");

            GetList(models);

            return View(models);
        }

        [HttpPost]
        public ActionResult Index(ClaimManual models, int caseid = 0, string ddlPriority = "", string ddlPrimaryDoc = "", string ddlDocumentSourceType = "", string ddlVIP = "", string ddlWorkType = "", string ddlCaseType = "", string ddlDocumentType = "", string SourceId = "", string ddlSource = "")
        {
            if (models.StepName == "Submit")
            {
                var tempResult = SubmitPolicyPool(models, ddlPriority, ddlPrimaryDoc, ddlDocumentSourceType, ddlVIP, ddlWorkType, ddlCaseType, ddlDocumentType, ddlSource);
                if (tempResult != null)
                {
                    models.CaseId = tempResult.Id;
                }
            }
            if (models.StepName == "Update")
            {
                var tempResult = UpdatePolicyPool(models, ddlPriority, ddlPrimaryDoc, ddlDocumentSourceType, ddlVIP, ddlWorkType, ddlCaseType, ddlDocumentType, ddlSource);
                models = getCLMPolicyPool(caseid);
                models.mSTImageAddtionals = getMstImgAddtional((int)models.CaseId);
            }
            if (models.StepName == "VerificationPolicy")
            {
                var tempResult = getVerificationPolicy(models.PolicyNo);
                if (tempResult != null)
                {
                    models = tempResult;
                }
                else
                {
                    TempData["Warning"] = "Data not found";
                    models.RLSTrans = "Failed";
                    models.RLSCaptureDate = DateTime.Now;
                }
            }
            if (models.StepName == "Image")
            {
                var tempResult = File(models, ddlPriority, ddlPrimaryDoc, ddlDocumentSourceType, ddlVIP, ddlWorkType, ddlCaseType, ddlDocumentType, ddlSource);
                models.mSTImageAddtionals = getMstImgAddtional((int)models.CaseId);
            }
            if (models.StepName == "DeleteSource")
            {
                var tempResult = DeleteMstImageAdditional(int.Parse(SourceId));
                models.mSTImageAddtionals = getMstImgAddtional((int)models.CaseId);
            }
            if (models.CaseId != 0)
            {
                models = getCLMPolicyPool(models.CaseId);
                models.mSTImageAddtionals = getMstImgAddtional((int)models.CaseId);
            }
            else
            {
                models = new ClaimManual();
            }

            GetList(models);

            return View(models);
        }
        public void GetList(ClaimManual models)
        {
            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = "RR", Value = "" });
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", "");

            string[] wType;
            List<SelectListItem> dataWorkType = new List<SelectListItem>();
            var workTypeList = GetReferenceData("WorkType");
            dataWorkType.Add(new SelectListItem() { Text = "", Value = "" });
            if (workTypeList != null)
            {
                foreach (string item in workTypeList)
                {
                    wType = item.Split('|');
                    dataWorkType.Add(new SelectListItem() { Text = wType[1], Value = wType[0] });
                }
            }
            ViewBag.dllWorktype = new SelectList(dataWorkType, "Value", "Text", models.WorkType);

            List<SelectListItem> dataCaseType = new List<SelectListItem>();
            dataCaseType.Add(new SelectListItem() { Text = "CLAIM", Value = "" });
            ViewBag.dllCaseType = new SelectList(dataCaseType, "Value", "Text", "");

            string[] vip;
            List<SelectListItem> dataVIP = new List<SelectListItem>();
            var vipList = GetReferenceData("VIP");
            dataVIP.Add(new SelectListItem() { Text = "", Value = "" });
            if (vipList != null)
            {
                foreach (string item in vipList)
                {
                    vip = item.Split('|');
                    dataVIP.Add(new SelectListItem() { Text = vip[1], Value = vip[0] });
                }
            }
            ViewBag.dllVIP = new SelectList(dataVIP, "Value", "Text", models.VIP);

            string[] primaryDoc;
            List<SelectListItem> dataPrimaryDoc = new List<SelectListItem>();
            var PrimaryDocList = GetReferenceData("PrimaryDoc");
            dataPrimaryDoc.Add(new SelectListItem() { Text = "", Value = "" });
            if (PrimaryDocList != null)
            {
                foreach (string item in PrimaryDocList)
                {
                    primaryDoc = item.Split('|');
                    dataPrimaryDoc.Add(new SelectListItem() { Text = primaryDoc[1], Value = primaryDoc[0] });
                }
            }
            ViewBag.dllPrimaryDoc = new SelectList(dataPrimaryDoc, "Value", "Text", models.PrimaryDoc);

            string[] docsSplit;
            List<SelectListItem> dataDocumentType = new List<SelectListItem>();
            var documentTypeList = GetListDocumentType();
            dataDocumentType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentTypeList != null)
            {
                foreach (string item in documentTypeList)
                {
                    docsSplit = item.Split('|');
                    dataDocumentType.Add(new SelectListItem() { Text = docsSplit[3], Value = item });
                }
            }
            ViewBag.dllDocumentType = new SelectList(dataDocumentType, "Value", "Text", models.DocumentType);

            string[] docSourceType;
            List<SelectListItem> dataDocumentSourceType = new List<SelectListItem>();
            var documentSourceTypeList = GetReferenceData("DocumentSourceType");
            dataDocumentSourceType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentSourceTypeList != null)
            {
                foreach (string item in documentSourceTypeList)
                {
                    docSourceType = item.Split('|');
                    dataDocumentSourceType.Add(new SelectListItem() { Text = docSourceType[1], Value = docSourceType[0] });
                }
            }
            ViewBag.dllDocumentSourceType = new SelectList(dataDocumentSourceType, "Value", "Text", models.DocumentSourceType);

            string[] priority;
            List<SelectListItem> dataPriority = new List<SelectListItem>();
            var dataPriorityList = GetReferenceData("Priority");
            dataPriority.Add(new SelectListItem() { Text = "", Value = "" });
            if (dataPriorityList != null)
            {
                foreach (string item in dataPriorityList)
                {
                    priority = item.Split('|');
                    dataPriority.Add(new SelectListItem() { Text = priority[1], Value = priority[0] });
                }
            }
            ViewBag.dllPriority = new SelectList(dataPriority, "Value", "Text", models.Priority);

            string[] source;
            List<SelectListItem> dataSource = new List<SelectListItem>();
            var dataSourceList = GetReferenceData("Source");
            dataSource.Add(new SelectListItem() { Text = "", Value = "" });
            if (dataSourceList != null)
            {
                foreach (string item in dataSourceList)
                {
                    source = item.Split('|');
                    dataSource.Add(new SelectListItem() { Text = source[1], Value = source[0] });
                }
            }
            ViewBag.dllSource = new SelectList(dataSource, "Value", "Text", models.Source);
        }

        public ClaimManual getVerificationPolicy(string policynumber)
        {
            try
            {
                var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + $"api/verification/v1/getprod/{policynumber}");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);

                if (response.IsSuccessful)
                {
                    object listarray1 = JsonConvert.DeserializeObject<object>(response.Content);

                    var SourceChannelCD = "";
                    var ProductCode = "";
                    var MemberNo = "";
                    var MemberId = "";
                    var SumInsured = "";
                    var MemberName = "";
                    var Holder = "";
                    var OwnerName = "";
                    var OwnerId = "";
                    var OwnerDob = "";

                    dynamic stuffhasPolicyAccount = JObject.Parse(listarray1.ToString());
                    var hasPolicyAccount = stuffhasPolicyAccount.Content.Body.Customer.hasPolicyAccount;

                    List<object> LAhasPolicyAccount = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(hasPolicyAccount));
                    foreach (var a in LAhasPolicyAccount)
                    {
                        dynamic temphasPolicyAccount = JObject.Parse(a.ToString());

                        SourceChannelCD = temphasPolicyAccount.sourceChannelCD;

                        object isAssociatedWithLifeInsuranceProduct = JsonConvert.DeserializeObject<object>(Convert.ToString(temphasPolicyAccount.isAssociatedWithLifeInsuranceProduct));
                        object hasDetailsOfRisksIn = JsonConvert.DeserializeObject<object>(Convert.ToString(temphasPolicyAccount.hasDetailsOfRisksIn));

                        List<object> LAisAssociatedWithLifeInsuranceProduct = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(isAssociatedWithLifeInsuranceProduct));
                        foreach (var b in LAisAssociatedWithLifeInsuranceProduct)
                        {
                            dynamic tempisAssociatedWithLifeInsuranceProduct = JObject.Parse(b.ToString());

                            ProductCode = tempisAssociatedWithLifeInsuranceProduct.planCd;

                            object hasPolicySegmentDetailsIn = JsonConvert.DeserializeObject<object>(Convert.ToString(tempisAssociatedWithLifeInsuranceProduct.hasPolicySegmentDetailsIn));

                            List<object> LAhasPolicySegmentDetailsIn = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(hasPolicySegmentDetailsIn));
                            foreach (var c in LAhasPolicySegmentDetailsIn)
                            {
                                dynamic tempLAhasPolicySegmentDetailsIn = JObject.Parse(c.ToString());

                                SumInsured = tempLAhasPolicySegmentDetailsIn.sumInsured;
                            }
                        }

                        List<object> LAhasDetailsOfRisksIn = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(hasDetailsOfRisksIn));
                        foreach (var d in LAhasDetailsOfRisksIn)
                        {
                            dynamic temphasDetailsOfRisksIn = JObject.Parse(d.ToString());

                            MemberNo = temphasDetailsOfRisksIn.insuredID;
                            MemberId = temphasDetailsOfRisksIn.insuredID;

                            object hasPersonalDetailsIn = JsonConvert.DeserializeObject<object>(Convert.ToString(temphasDetailsOfRisksIn.hasPersonalDetailsIn));

                            List<object> LAhasPersonalDetailsIn = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(hasPersonalDetailsIn));
                            foreach (var e in LAhasPersonalDetailsIn)
                            {
                                dynamic temphasPersonalDetailsIn = JObject.Parse(e.ToString());

                                var firstNM = temphasPersonalDetailsIn.firstNM;
                                var middleNm = temphasPersonalDetailsIn.middleNm;
                                var lastNM = temphasPersonalDetailsIn.lastNM;
                                MemberName = firstNM + " " + middleNm + " " + lastNM;
                            }
                        }
                    }

                    var canBeIndividual = stuffhasPolicyAccount.Content.Body.Customer.canBeIndividual;
                    Holder = canBeIndividual.firstNM;
                    OwnerName = Holder;
                    OwnerId = canBeIndividual.socialSecurityNO;
                    OwnerDob = canBeIndividual.birthDT;
                    var tempYear = OwnerDob.Substring(0, 4);
                    var tempMonth = OwnerDob.Substring(4, 2);
                    var tempDay = OwnerDob.Substring(6, 2);
                    OwnerDob = tempMonth + "-" + tempDay + "-" + tempYear;

                    ClaimManual claimManual = new ClaimManual();
                    claimManual.ProductCode = ProductCode.ToString();
                    claimManual.PolicyNo = policynumber;
                    claimManual.MemberNo = policynumber;
                    claimManual.MemberId = MemberId.ToString();
                    claimManual.Holder = Holder.ToString();
                    claimManual.MemberName = MemberName.ToString();
                    claimManual.OwnerName = OwnerName.ToString();
                    claimManual.OwnerId = OwnerId.ToString();
                    claimManual.OwnerDob = DateTime.Parse(OwnerDob.ToString());
                    claimManual.SumInsured = int.Parse(SumInsured.ToString());
                    claimManual.VIP = SourceChannelCD.ToString() == "" ? "NO" : "YES";
                    var tempPriority = claimManual.VIP == "YES" ? "3" : "1";
                    claimManual.Priority = tempPriority.ToString() == "3" ? "3" : "1";
                    claimManual.RLSCaptureDate = DateTime.Now;
                    claimManual.RLSTrans = "Success";

                    return claimManual;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ClaimManual getCLMPolicyPool(long caseid)
        {
            try
            {
                var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + $"api/clmpolicypools/{caseid}");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse res = client.Execute(request);
                Console.WriteLine(res.Content);

                if (res.IsSuccessful)
                {
                    ClaimManual claimManual = new ClaimManual();
                    ClaimManual tempResult = JsonConvert.DeserializeObject<ClaimManual>(res.Content);
                    return tempResult;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<MSTImageAddtional> getMstImgAddtional(int caseid)
        {
            try
            {
                var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + $"api/mstimageadditionals/{caseid}");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse res = client.Execute(request);
                Console.WriteLine(res.Content);

                if (res.IsSuccessful)
                {
                    List<MSTImageAddtional> listmSTImageAddtional = new List<MSTImageAddtional>();
                    var tempResult = JsonConvert.DeserializeObject<object>(res.Content);
                    List<object> listArray = JsonConvert.DeserializeObject<List<object>>(Convert.ToString(tempResult));
                    foreach (var a in listArray)
                    {
                        dynamic result = JObject.Parse(a.ToString());

                        MSTImageAddtional mSTImageAddtional = new MSTImageAddtional();
                        mSTImageAddtional.Id = result.Id;
                        mSTImageAddtional.CaseId = result.CaseId;
                        mSTImageAddtional.Filename = result.Filename;
                        mSTImageAddtional.Fileimage = result.Fileimage;
                        mSTImageAddtional.WorkType = result.WorkType;
                        mSTImageAddtional.CreateDate = result.CreateDate;
                        mSTImageAddtional.Source = result.Source;
                        mSTImageAddtional.DocumentType = result.DocumentType;
                        listmSTImageAddtional.Add(mSTImageAddtional);
                    }

                    return listmSTImageAddtional;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RequestResponse.MessageResponse SubmitPolicyPool(ClaimManual claimManual, string ddlPriority, string ddlPrimaryDoc, string ddlDocumentSourceType, string ddlVIP, string ddlWorkType, string ddlCaseType, string ddlDocumentType, string ddlSource)
        {

            var messageResponse = new RequestResponse.MessageResponse();
            claimManual.MemberId = claimManual.MemberId;
            claimManual.CreatedBy = GetSession.UserName();
            claimManual.CreatedDate = DateTime.Now;
            claimManual.Priority = ddlPriority;
            claimManual.PrimaryDoc = ddlPrimaryDoc;
            claimManual.DocumentSourceType = ddlDocumentSourceType;
            claimManual.VIP = ddlVIP;
            claimManual.WorkType = ddlWorkType;
            claimManual.CaseType = "CLAIM";
            claimManual.Source = ddlSource;
            if (claimManual.ReceivedDate == null)
            {
                claimManual.ReceivedDate = DateTime.Now;
            }

            var temp = JsonConvert.SerializeObject(claimManual);
            var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + "api/clmpolicypools/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", temp, ParameterType.RequestBody);
            IRestResponse res = client.Execute(request);

            if (res.IsSuccessful)
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Content = JsonConvert.DeserializeObject<object>(res.Content);
                dynamic resultId = JObject.Parse(messageResponse.Content.ToString());
                messageResponse.Id = resultId.CaseId;
                TempData["Success"] = "Success submit data";
                return messageResponse;
            }
            else
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = "Failed";
                TempData["Warning"] = "Failed submit data";
                return messageResponse;
            }
        }

        public RequestResponse.MessageResponse UpdatePolicyPool(ClaimManual claimManual, string ddlPriority, string ddlPrimaryDoc, string ddlDocumentSourceType, string ddlVIP, string ddlWorkType, string ddlCaseType, string ddlDocumentType, string ddlSource)
        {
            var messageResponse = new RequestResponse.MessageResponse();
            claimManual.MemberId = claimManual.MemberId.Trim();
            claimManual.CreatedBy = GetSession.UserName();
            claimManual.CreatedDate = DateTime.Now;
            claimManual.Priority = ddlPriority;
            claimManual.PrimaryDoc = ddlPrimaryDoc;
            claimManual.DocumentSourceType = ddlDocumentSourceType;
            claimManual.VIP = ddlVIP;
            claimManual.WorkType = ddlWorkType;
            claimManual.CaseType = "CLAIM";
            claimManual.Source = ddlSource;
            if (claimManual.ReceivedDate == null)
            {
                claimManual.ReceivedDate = DateTime.Now;
            }
            var caseid = claimManual.CaseId;
            var temp = JsonConvert.SerializeObject(claimManual);
            var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + $"api/clmpolicypools/{caseid}");
            client.Timeout = -1;
            var request = new RestRequest(Method.PUT);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", temp, ParameterType.RequestBody);
            IRestResponse res = client.Execute(request);

            if (res.IsSuccessful)
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = "Success";
                TempData["Success"] = "Success update data";
                return messageResponse;
            }
            else
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = "Failed";
                TempData["Warning"] = "Failed update data";
                return messageResponse;
            }
        }

        public RequestResponse.MessageResponse File(ClaimManual models, string ddlPriority, string ddlPrimaryDoc, string ddlDocumentSourceType, string ddlVIP, string ddlWorkType, string ddlCaseType, string ddlDocumentType, string ddlSource)
        {
            string[] docsSplit;
            docsSplit = ddlDocumentType.Split('|');

            Stream fs = models.SourceDoc.InputStream;
            BinaryReader br = new System.IO.BinaryReader(fs);
            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            var imageBytes = Convert.FromBase64String(base64String);

            MSTImageAddtional mSTImageAddtional = new MSTImageAddtional();
            mSTImageAddtional.CaseId = models.CaseId;
            mSTImageAddtional.PolicyNo = models.PolicyNo;
            mSTImageAddtional.Filename = models.SourceDoc.FileName;
            mSTImageAddtional.Fileimage = imageBytes;
            mSTImageAddtional.DeptId = "CLAIM";
            mSTImageAddtional.DocumentId = int.Parse(docsSplit[0]);
            mSTImageAddtional.DocumentTitle = models.DocumentTitle;
            mSTImageAddtional.DocumentType = docsSplit[1];
            mSTImageAddtional.WorkType = ddlWorkType;
            mSTImageAddtional.CreateBy = GetSession.UserName();
            mSTImageAddtional.CreateDate = DateTime.Now;
            mSTImageAddtional.Source = ddlSource;

            var result = SubmitMstIMageAdditional(mSTImageAddtional);

            return result;
        }


        public RequestResponse.MessageResponse SubmitMstIMageAdditional(MSTImageAddtional mSTImageAddtional)
        {
            var messageResponse = new RequestResponse.MessageResponse();
            mSTImageAddtional.CreateBy = GetSession.UserName();
            mSTImageAddtional.CreateDate = DateTime.Now;
            var temp = JsonConvert.SerializeObject(mSTImageAddtional);

            var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + "api/mstimageadditionals/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", temp, ParameterType.RequestBody);
            IRestResponse res = client.Execute(request);

            if (res.IsSuccessful)
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = res.Content;
                TempData["Success"] = "Success add file";
                return messageResponse;
            }
            else
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = "Failed";
                TempData["Warning"] = "Dailed add file";
                return messageResponse;
            }
        }

        public RequestResponse.MessageResponse DeleteMstImageAdditional(int id)
        {
            var messageResponse = new RequestResponse.MessageResponse();
            var client = new RestClient(ConfigurationManager.AppSettings["DipcadAPI"].ToString() + $"api/mstimageadditionals/{id}");
            client.Timeout = -1;
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse res = client.Execute(request);
            Console.WriteLine(res.Content);

            if (res.IsSuccessful)
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = res.Content;
                TempData["Success"] = "Success delete data";
                return messageResponse;
            }
            else
            {
                messageResponse.Code = res.StatusCode.ToString();
                messageResponse.Message = "Failed";
                TempData["Warning"] = "Failed delete data";
                return messageResponse;
            }
        }

        private string getUserAssign(string worktype)
        {
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Detail_AssignFrom '{worktype}'");
            string userfrom = string.Empty;

            foreach (DataRow item in dTab.Rows)
            {
                userfrom = item[0].ToString();
            }

            return userfrom;
        }

        public List<string> GetListWorkType()
        {
            List<string> workType = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.SPInq_CLM_Worktype");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    workType.Add($"{rows[0].ToString()}|{rows[1].ToString()}");
                }
            }

            return workType;
        }

        public List<string> GetReferenceData(string referenceDataType)
        {
            List<string> penReasonType = new List<string>();
            var type = "type = ''" + referenceDataType + "'' and status = 1";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_ReferenceData 1, 20, '{type}', '', '1'");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    penReasonType.Add($"{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return penReasonType;
        }

        public List<string> GetListDocumentType()
        {
            List<string> DocumentTipe = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.[SPInq_CLM_Document_Type]");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    DocumentTipe.Add($"{rows[0].ToString()}|{rows[1].ToString()}|{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return DocumentTipe;
        }
    }
}
