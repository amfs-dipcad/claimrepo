﻿using ReportingImage.App_Start;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "Product")]
        public ActionResult Index(int pageNumber = 1)
        {
            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Product", null, userName);

            var product = _db.zMaster_Product.Where(a => a.is_delete == false);
            return View(product.ToList());
        }

        [CheckAuthorize(Roles = "Create Product")]
        public ActionResult Create()
        {
            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Create Product", null, userName);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Create Product")]
        public ActionResult Create(zMaster_Product zProduct)
        {
            if (zProduct.product_code == null) { ModelState.AddModelError("product_code", "This field is required."); }

            if (zProduct.product_name == null) { ModelState.AddModelError("product_name", "This field is required."); }

            if (zProduct.payment_method == null) { ModelState.AddModelError("payment_method", "This field is required."); }

            if (zProduct.type_product == null) { ModelState.AddModelError("type_product", "This field is required."); }

            var checkProductCode = _db.zMaster_Product.Where(x => x.product_code == zProduct.product_code).ToList();
            if (checkProductCode.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_MasterProductInsert(GetSession.UserName(), zProduct.product_name, zProduct.product_code,
                        zProduct.type_product, zProduct.payment_method);
                    _db.zsp_AuditTrailInsert("Create Product", null, GetSession.UserName());
                    TempData["Success"] = "Success for Saving " + zProduct.product_code;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Product Code :" + zProduct.product_code + " already exist!";
            }

            return View(zProduct);
        }

        [CheckAuthorize(Roles = "Update Product")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Update Product", null, userName);

            zMaster_Product zProduct = _db.zMaster_Product.Find(id);
            if (zProduct == null)
            {
                return HttpNotFound();
            }

            return View(zProduct);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Update Product")]
        public ActionResult Edit(zMaster_Product zProduct)
        {
            if (zProduct.product_code == null) { ModelState.AddModelError("product_code", "This field is required."); }
            if (zProduct.product_name == null) { ModelState.AddModelError("product_name", "This field is required."); }

            if (zProduct.payment_method == null) { ModelState.AddModelError("payment_method", "This field is required."); }

            if (zProduct.type_product == null) { ModelState.AddModelError("type_product", "This field is required."); }

            if (zProduct.id == null) { ModelState.AddModelError("id", "This field is required."); }

            //check data in database if already exist skip
            var checkProductCode = _db.zMaster_Product.Where(x => x.product_code == zProduct.product_code && x.id != zProduct.id).ToList();
            if (checkProductCode.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_MasterProductUpdate(zProduct.id, GetSession.UserName(), zProduct.product_name, zProduct.product_code,
                        zProduct.type_product, zProduct.payment_method, false);
                    _db.zsp_AuditTrailInsert("Update Product", null, GetSession.UserName());
                    TempData["Success"] = "Success Edit for " + zProduct.product_code;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Product Code :" + zProduct.product_code + " already exist!";
            }
            return View(zProduct);  
        }

        [CheckAuthorize(Roles = "Delete Product")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zMaster_Product zProduct = _db.zMaster_Product.Find(id);
            if (zProduct == null)
            {
                TempData["Error"] = "Product Code not Found";
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Delete Product", null, userName);

            _db.zsp_MasterProductUpdate(zProduct.id, GetSession.UserName(), zProduct.product_name, zProduct.product_code,
                      zProduct.type_product, zProduct.payment_method, true);
            TempData["Success"] = "Delete Success for " + zProduct.product_code;
            return RedirectToAction("Index");
        }

        public static List<ProductViewModels> GetProductList(string userName, int pageNumber)
        {
            var titleBtnEscalate = string.Empty;
            List<ProductViewModels> model = new List<ProductViewModels>();

            DataTable tbl = Common.ExecuteQuery("dbo.zsp_MasterProduct '" + userName + "','" + pageNumber + "'");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                DateTime updatedDate = DateTime.Now;
                if (tbl.Rows[i]["updated_date"].ToString() == "")
                {
                    updatedDate = Convert.ToDateTime(tbl.Rows[i]["date_submission"].ToString());
                }
                else
                {
                    updatedDate = Convert.ToDateTime(tbl.Rows[i]["updated_date"].ToString());
                }

                model.Add(new ProductViewModels()
                {
                    id = Convert.ToInt32(tbl.Rows[i]["id"].ToString())
                    ,product_code = tbl.Rows[i]["product_code"].ToString()
                    ,product_name = tbl.Rows[i]["product_name"].ToString()
                    ,type_product = tbl.Rows[i]["type_product"].ToString()
                    ,payment_method = tbl.Rows[i]["payment_method"].ToString()
                    ,is_delete = Convert.ToBoolean(tbl.Rows[i]["is_delete"].ToString())
                    ,created_date= Convert.ToDateTime(tbl.Rows[i]["created_date"].ToString())
                    ,created_by= tbl.Rows[i]["created_by"].ToString()
                    ,updated_by = tbl.Rows[i]["updated_by"].ToString()
                    ,updated_date = updatedDate,
                    Counts = Convert.ToInt32(tbl.Rows[i]["Counts"].ToString())
                });
            }
            return model;
        }
    }
}