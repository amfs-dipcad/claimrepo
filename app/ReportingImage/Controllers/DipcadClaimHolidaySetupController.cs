﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportingImage.Models;
using ReportingImage.ViewModels;

namespace ReportingImage.Controllers
{
    public class DipcadClaimHolidaySetupController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();

        // GET: DipcadClaimHolidaySetup
        public ActionResult Index()
        {
            var tempResult = _db.HolidaySetups.ToList().OrderBy(x=>x.Id);
            ViewBag.ListHolidaySetup = tempResult;
            return View();
        }

        [HttpPost]
        public ActionResult Index(int pageNumber = 1, string receiveddatefrom = null, string receiveddateto = null, string txRemarks = null)
        {
            var dateFrom = new DateTime();
            var dateTo = new DateTime();


            if (receiveddatefrom != null && receiveddatefrom != "" && receiveddateto != null && receiveddateto != "")
            {
                try
                {
                    dateFrom = Convert.ToDateTime(receiveddatefrom);
                    dateTo = Convert.ToDateTime(receiveddateto);

                    int totalDaysInsert = (int)dateTo.Subtract(dateFrom).TotalDays;
                    //var inserteddate = dateFrom.Date.ToString();

                    for (int i = 0; i <= totalDaysInsert; i++)
                    {
                        var query = "dbo.SP_InsertHolidaySetup '',";
                        query = query + "'" + dateFrom.AddDays(i) + "',";//holidaydate
                        query = query + "'" + txRemarks + "',";//remarks
                        query = query + "'" + GetSession.UserName() + "',";//holidaydate
                        query = query + "'insert'";//action
                        DataTable dTab = Common.ExecuteQuery(query);
                    }

                    TempData["Success"] = $"Success Insert Holiday Date from {receiveddatefrom} to {dateTo}";
                }
                catch (Exception ex)
                {
                    TempData["Error"] = $"Error For : {ex.Message}";
                }
                
            }
            else
            {
                TempData["Error"] = $"Error. Date From and Date To Is Required";
            }

            var tempResult = _db.HolidaySetups.ToList();
            ViewBag.ListHolidaySetup = tempResult;
            return View();
        }


        // GET: DipcadClaimHolidaySetup/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var query = "dbo.SP_InsertHolidaySetup '"+id.ToString()+"',";
                query = query + "'',";//holidaydate
                query = query + "'',";//remarks
                query = query + "'',";//holidaydate
                query = query + "'delete'";//action
                DataTable dTab = Common.ExecuteQuery(query);

                TempData["Success"] = $"Success Delete";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = $"Error For : {ex.Message}";
                return RedirectToAction("Index");
            }
            
        }

        // POST: DipcadClaimHolidaySetup/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
