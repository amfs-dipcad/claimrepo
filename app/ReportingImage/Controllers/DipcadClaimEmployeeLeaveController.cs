﻿using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class DipcadClaimEmployeeLeaveController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();

        // GET: DipcadClaimEmployeeLeave
        public ActionResult Index()
        {
            //var tempResult = _db.EmployeeLeaveStatus.ToList();
            //ViewBag.ListEmployeeLeaveStatus = tempResult;
            var listEmpLeaveStatus = ListData();

            string[] wTL;
            List<SelectListItem> dataTimeList = new List<SelectListItem>();
            var timeList = GetReferenceData("EmployeeLeaveTime");
            dataTimeList.Add(new SelectListItem() { Text = "", Value = "" });
            if (timeList != null)
            {
                foreach (string item in timeList)
                {
                    wTL = item.Split('|');
                    dataTimeList.Add(new SelectListItem() { Text = wTL[1], Value = wTL[0] });
                }
            }
            ViewBag.dllTimeFrom = new SelectList(dataTimeList, "Value", "Text");
            ViewBag.dllTimeTo = new SelectList(dataTimeList, "Value", "Text");

            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            //dataAssignTo.Add(new SelectListItem() { Text = "System RR", Value = "SystemRR" });
            var users = GetUserClaim();
            if (users != null)
            {
                foreach (var item in users)
                {
                    dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username });
                }
            }
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text");

            ViewBag.ListEmployeeLeaveStatus = listEmpLeaveStatus;
            return View();
        }

        [HttpPost]
        public ActionResult Index(int pageNumber = 1, string AssignTo = null, string receiveddatefrom = null, string receiveddateto = null, string txRemarks = null, bool fullDay = false, string dllTimeFrom = null, string dllTimeTo = null)
        {
            var dateFrom = new DateTime();
            var dateTo = new DateTime();


            if (receiveddatefrom != null && receiveddatefrom != "" && receiveddateto != null && receiveddateto != "")
            {
                try
                {
                    dateFrom = Convert.ToDateTime(receiveddatefrom);
                    dateTo = Convert.ToDateTime(receiveddateto);

                    string isfulday = Convert.ToString(fullDay).ToLower() == "false" ? "0" : "1";

                    if (fullDay)
                    {
                        dllTimeFrom = "00:00";
                        dllTimeTo = "23:59";
                    }

                    var query = "dbo.SP_InsertEmployeLeaveStatus ";
                    query = query + "'',";//id
                    query = query + "'" + AssignTo + "',";//employee name
                    query = query + "'" + receiveddatefrom + "',";//leave date from
                    query = query + "'" + receiveddateto + "',";//leave date to
                    query = query + "'" + dllTimeFrom + "',";//time from
                    query = query + "'" + dllTimeTo + "',";//time to
                    query = query + "'" + txRemarks + "',";//remarks
                    query = query + "'" + GetSession.UserName() + "',";//user submitted
                    query = query + "'insert',";//action
                    query = query + "'" + isfulday + "'";//full day
                    DataTable dTab = Common.ExecuteQuery(query);

                    TempData["Success"] = $"Success Insert Holiday Date from {receiveddatefrom} to {dateTo}";
                }
                catch (Exception ex)
                {
                    TempData["Error"] = $"Error For : {ex.Message}";
                }

            }
            else
            {
                TempData["Error"] = $"Error. Date From and Date To Is Required";
            }

            //var tempResult = _db.HolidaySetups.ToList();
            //ViewBag.ListHolidaySetup = tempResult;
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            string idstr = id.ToString();

            var listEmpLeaveStatus = ListData().Where(x=>x.Id == idstr).FirstOrDefault();

            string[] wTL;
            List<SelectListItem> dataTimeList = new List<SelectListItem>();
            var timeList = GetReferenceData("EmployeeLeaveTime");
            dataTimeList.Add(new SelectListItem() { Text = "", Value = "" });
            if (timeList != null)
            {
                foreach (string item in timeList)
                {
                    wTL = item.Split('|');
                    dataTimeList.Add(new SelectListItem() { Text = wTL[1], Value = wTL[0] });
                }
            }
            ViewBag.dllTimeFrom = new SelectList(dataTimeList, "Value", "Text", listEmpLeaveStatus.TimeFrom);
            ViewBag.dllTimeTo = new SelectList(dataTimeList, "Value", "Text", listEmpLeaveStatus.TimeTo);

            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            //dataAssignTo.Add(new SelectListItem() { Text = "System RR", Value = "SystemRR" });
            var users = GetUserClaim();
            if (users != null)
            {
                foreach (var item in users)
                {
                    dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username });
                }
            }
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", listEmpLeaveStatus.EmployeeName);


            ViewBag.EmployeeLeaveStatus = listEmpLeaveStatus;


            return View(listEmpLeaveStatus);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeLeaveStatus dataStat)
        {
            try
            {
                string isFullDay = Convert.ToString(dataStat.IsFullDay).ToLower() == "false" ? "0" : "1";
                if (dataStat.IsFullDay)
                {
                    dataStat.TimeTo = "00:00";
                    dataStat.TimeFrom = "23:59";
                }
                DataTable dTab = Common.ExecuteQuery($"exec[SP_InsertEmployeLeaveStatus] '{dataStat.Id}', '{dataStat.EmployeeName}', '{dataStat.LeaveDateFrom}', '{dataStat.LeaveDateFrom}', '{dataStat.TimeFrom}', '{dataStat.TimeTo}', '{dataStat.Remarks}', '{GetSession.UserName()}', 'update', '{isFullDay}'");

                TempData["Success"] = $"Success Edit Id:{dataStat.Id}";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = $"Error For : {ex.Message}";
                return RedirectToAction("Index");
            }
        }

        // GET: DipcadClaimEmployeeLeave/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                DataTable dTab = Common.ExecuteQuery($"exec[SP_InsertEmployeLeaveStatus] '{id}', '', '', '', '', '', '', '', 'delete', ''");

                TempData["Success"] = $"Success Delete";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = $"Error For : {ex.Message}";
                return RedirectToAction("Index");
            }
        }

        // POST: DipcadClaimEmployeeLeave/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        public List<EmployeeLeaveStatus> ListData()
        {
            DataTable dTab = Common.ExecuteQuery($"exec[SP_InsertEmployeLeaveStatus] '', '', '', '', '', '', '', '', 'view', ''");

            var listEmpLeaveStatus = new List<EmployeeLeaveStatus>();
            if (dTab.Rows.Count > 0)
            {
                for (int i = 0; i < dTab.Rows.Count; i++)
                {
                    var empLeaveStatus = new EmployeeLeaveStatus();
                    empLeaveStatus.No = dTab.Rows[i]["No"].ToString();
                    empLeaveStatus.Id = dTab.Rows[i]["Id"].ToString();
                    empLeaveStatus.EmployeeName = dTab.Rows[i]["EmployeeName"].ToString();
                    empLeaveStatus.DeptId = dTab.Rows[i]["DeptId"].ToString();
                    empLeaveStatus.LeaveDateFrom = String.IsNullOrWhiteSpace(dTab.Rows[i]["LeaveDateFrom"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["LeaveDateFrom"].ToString());
                    empLeaveStatus.LeaveDateTo = String.IsNullOrWhiteSpace(dTab.Rows[i]["LeaveDateTo"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["LeaveDateTo"].ToString());
                    empLeaveStatus.TimeFrom = dTab.Rows[i]["TimeFrom"].ToString();
                    empLeaveStatus.TimeTo = dTab.Rows[i]["Timeto"].ToString();

                    var cekBool = dTab.Rows[i]["IsFullDay"].ToString();
                    //if (cekBool == 1)
                    empLeaveStatus.IsFullDay = Convert.ToBoolean( dTab.Rows[i]["IsFullDay"].ToString() == "True" ? "true" : "false");

                    empLeaveStatus.Remarks = dTab.Rows[i]["Remarks"].ToString();
                    empLeaveStatus.DateSubmitted = String.IsNullOrWhiteSpace(dTab.Rows[i]["DateSubmitted"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["DateSubmitted"].ToString());
                    empLeaveStatus.UserSubmitted = dTab.Rows[i]["UserSubmitted"].ToString();
                    listEmpLeaveStatus.Add(empLeaveStatus);
                }
            }
            return listEmpLeaveStatus;
        }


        public List<string> GetReferenceData(string referenceDataType)
        {
            List<string> penReasonType = new List<string>();
            var type = "type = ''" + referenceDataType + "'' and status = 1";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_ReferenceData 1, 100, '{type}', '', '1'");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    penReasonType.Add($"{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return penReasonType;
        }

        public List<UserView> GetUserClaim()
        {
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_getUserActive");
            List<UserView> usr = new List<UserView>();

            foreach (DataRow item in dTab.Rows)
            {
                usr.Add(new UserView() { username = item["USERNAME"].ToString() });
            }

            return usr;
        }
    }
}
