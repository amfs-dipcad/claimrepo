﻿using ReportingImage.Models;
using System;
using System.Linq;
using System.Web;

namespace ReportingImage.Controllers
{
    public class GetSession
    {
        public static int User_ID()
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(HttpContext.Current.Session["User_ID"].ToString());
            }
            catch { }

            return value;
        }

        public static bool IsAdministrator()
        {
            try
            {
                var value = HttpContext.Current.Session["User_ID"].ToString();
                if (value == "d0d89473-1d55-4606-891e-b63f6933a7e5")
                {
                    return true;
                }
            }
            catch { }

            return false;
        }

        public static int Privilege_ID()
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(HttpContext.Current.Session["Privilege_ID"].ToString());
            }
            catch { }
            return value;
        }

        public static string UserName()
        {
            var value = "";
            try
            {
                value = HttpContext.Current.Session["User_Name"].ToString();
            }
            catch { }

            return value;
        }

        public DateTime GetServerDate()
        {
            DateTime Result;
            using (var ctx = new ReportingImageEntities())
            {
                //Get student name of string type
                Result = ctx.Database.SqlQuery<DateTime>("select GETDATE() GETDATETIME").FirstOrDefault<DateTime>();
            }
            return Result;
            //return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("select GETDATE() GETDATETIME").Single().ToString();
        }

        public static string Password()
        {
            var value = "";
            try
            {
                value = HttpContext.Current.Session["Password"].ToString();
            }
            catch { }

            return value;
        }
    }
}