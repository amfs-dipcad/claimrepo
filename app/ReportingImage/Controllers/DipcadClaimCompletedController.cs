﻿using Ionic.Zip;
using iTextSharp.text;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class DipcadClaimCompletedController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();

        // GET: DipcadClaimCompleted
        public ActionResult Index(string policyNumber, string ddlCategory, int pageNumber = 1, string receiveddatefrom = null, string receiveddateto = null)
        {

            var User = GetSession.UserName();
            TempData["ddlCategory"] = ddlCategory;

            var tempResult = _db.SPCLM_PolicyPool_Inq().Where(x => User == x.UserAssign?.Trim() 
                                                                && (x.IsComplete == true)
                                                                ).ToList();

            //if(User != null && User != "")
            //{
            //    tempResult = tempResult.Where(a => a.UserAssign.Trim() == User).ToList();
            //}
            if (receiveddatefrom != null && receiveddatefrom != "")
            {
                DateTime dateTimereceiveddatefrom = Convert.ToDateTime(receiveddatefrom);
                tempResult = tempResult.Where(a => a.CreatedDate.Date >= dateTimereceiveddatefrom).ToList();
            }
            if (receiveddateto != null && receiveddateto != "")
            {
                DateTime dateTimereceiveddateto = Convert.ToDateTime(receiveddateto);
                tempResult = tempResult.Where(a => a.CreatedDate.Date <= dateTimereceiveddateto).ToList();
            }
            if (policyNumber != null && policyNumber != "")
            {
                tempResult = tempResult.Where(a => a.PolicyNo == policyNumber).ToList();
            }

            ViewBag.ListPolicyPool = tempResult;

            List<SelectListItem> dataCategory = new List<SelectListItem>();
            dataCategory.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
            dataCategory.Add(new SelectListItem() { Text = "New Case", Value = "New Case" });
            dataCategory.Add(new SelectListItem() { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            ViewBag.ddlCategory = new SelectList(dataCategory, "Value", "Text", ddlCategory);

            _db.zsp_AuditTrailInsert("Claim Open Policy List", policyNumber, GetSession.UserName());

            return View();
        }

        public ActionResult DetailClient(int? caseid)
        {
            if (caseid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string[] wType;

            var userName = GetSession.UserName();

            DipcadClaimOpenViewModels singleCLMOpen = GetListCLMbyID(caseid);

            if (singleCLMOpen == null)
            { return HttpNotFound(); }

            #region 'List'
            string dllCategoryVal = singleCLMOpen.CATEGORY;
            string dllWorktypeVal = singleCLMOpen.WORKTYPE;
            string dllPriorityVal = singleCLMOpen.PRIORITY;
            if (singleCLMOpen.VIP == "YES")
                dllPriorityVal = "3";
            //string dllCaseTypeVal = singleCLMOpen.CASE_TYPE;
            string dllPeReasonVal = singleCLMOpen.PENDING_REASON;
            //string dllAsignTo = getUserAssign(caseid);
            //string dllAsignTo = getUserAssign(singleCLMOpen.WORKTYPE);

            TempData["ddlWorktype"] = dllWorktypeVal;
            TempData["ddlCategory"] = dllCategoryVal;
            //TempData["AssignTo"] = dllAsignTo;

            List<SelectListItem> dataWorkType = new List<SelectListItem>();
            var workTypeList = GetListWorkType();
            dataWorkType.Add(new SelectListItem() { Text = "", Value = "" });
            if (workTypeList != null)
            {
                foreach (string item in workTypeList)
                {
                    wType = item.Split('|');
                    dataWorkType.Add(new SelectListItem() { Text = wType[1], Value = wType[0] });
                }
            }
            string[] reasonType;
            List<SelectListItem> dataReasonPending = new List<SelectListItem>();
            var pendingReasonList = GetReferenceData("ReasonPending");
            dataReasonPending.Add(new SelectListItem() { Text = "", Value = "" });
            if (pendingReasonList != null)
            {
                foreach (string item in pendingReasonList)
                {
                    reasonType = item.Split('|');
                    dataReasonPending.Add(new SelectListItem() { Text = reasonType[1], Value = reasonType[0] });
                }
            }
            string[] assignOtherDiv;
            List<SelectListItem> dataOtherDiv = new List<SelectListItem>();
            var otherDivList = GetReferenceData("AssignToOtherDiv");
            dataOtherDiv.Add(new SelectListItem() { Text = "", Value = "" });
            if (otherDivList != null)
            {
                foreach (string item in otherDivList)
                {
                    assignOtherDiv = item.Split('|');
                    dataOtherDiv.Add(new SelectListItem() { Text = assignOtherDiv[1], Value = assignOtherDiv[0] });
                }
            }
            string[] priority;
            List<SelectListItem> dataPriority = new List<SelectListItem>();
            var priorityList = GetReferenceData("Priority");
            dataPriority.Add(new SelectListItem() { Text = "", Value = "" });
            if (priorityList != null)
            {
                foreach (string item in priorityList)
                {
                    priority = item.Split('|');
                    dataPriority.Add(new SelectListItem() { Text = priority[1], Value = priority[0] });
                }
            }
            //string[] caseType;
            //List<SelectListItem> dataCaseType = new List<SelectListItem>();
            //var caseTypeList = GetReferenceData("CaseType");
            //dataCaseType.Add(new SelectListItem() { Text = "", Value = "" });
            //if (caseTypeList != null)
            //{
            //    foreach (string item in caseTypeList)
            //    {
            //        caseType = item.Split('|');
            //        dataCaseType.Add(new SelectListItem() { Text = caseType[1], Value = caseType[0] });
            //    }
            //}

            ViewBag.dllWorktype = new SelectList(dataWorkType, "Value", "Text", dllWorktypeVal);
            ViewBag.dllPenReasonList = new SelectList(dataReasonPending, "Value", "Text", dllPeReasonVal);
            ViewBag.dllOtherDiv = new SelectList(dataOtherDiv, "Value", "Text");
            ViewBag.dllPriority = new SelectList(dataPriority, "Value", "Text", dllPriorityVal);
            //ViewBag.dllCaseType = new SelectList(dataCaseType, "Value", "Text", dllCaseTypeVal);

            List<SelectListItem> dataCategory = new List<SelectListItem>();
            dataCategory.Add(new SelectListItem() { Text = "New Case", Value = "New Case" });
            dataCategory.Add(new SelectListItem() { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            ViewBag.dllCategory = new SelectList(dataCategory, "Value", "Text", dllCategoryVal);

            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            var users = GetUserClaim();
            if (users != null)
            {
                foreach (var item in users)
                {
                    if (item.username == GetSession.UserName()) continue;
                    dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username });
                }
            }
            //ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", dllAsignTo);
            ViewBag.dllAssignTo = new SelectList(dataAssignTo, "Value", "Text", "Administrator");

            //set SLA
            var newdate = singleCLMOpen.CREATE_DATE;
            //var SLASet = DateTime.Now.Subtract(newdate == null ? DateTime.Now : newdate).  TotalHours;
            var SLASetWithoutWeekend = SubstractDateTimeWithoutWeekend(DateTime.Now, newdate);
            if (singleCLMOpen.FINISHPENDATE != null)
            {
                //SLASetWithoutWeekend = (singleCLMOpen.DATE_COMPLETE ?? DateTime.Now).Subtract(newdate == null ? DateTime.Now : newdate).TotalHours;
                SLASetWithoutWeekend = SubstractDateTimeWithoutWeekend(singleCLMOpen.DATE_COMPLETE ?? DateTime.Now, newdate);
            }
            singleCLMOpen.SLA = Convert.ToInt32(Math.Floor(SLASetWithoutWeekend)); //int.Parse(SLASet.ToString());

            int remainder; // for hours
            double quotient = Math.DivRem(singleCLMOpen.SLA, 24, out remainder);
            TimeSpan result = TimeSpan.FromHours(singleCLMOpen.SLA); //day type double
            int x = (int)result.TotalDays; //day type int

            ViewBag.SLA = x + " Days " + remainder + " Hours";

            var cekVIP = _db.CLM_ReferenceData.Where(n => n.Type == "VIP").ToList();
            string VIPVal = "";
            foreach (var item in cekVIP)
            {
                if (singleCLMOpen.VIP == item.Code)
                {
                    VIPVal = item.Description;
                }
            }
            if (VIPVal == "") VIPVal = "NO";
            ViewBag.VIP = VIPVal;

            singleCLMOpen.PRODUCT_NAME = GetProductName(singleCLMOpen.PRODUCT_CODE);
            singleCLMOpen.DOCUMENT_TYPE = GetDocumentDescription(singleCLMOpen.DOCUMENT_TYPE);

            #endregion

            _db.zsp_AuditTrailInsert("Claim Detail Client", singleCLMOpen.POLICY_NO, GetSession.UserName());

            return View(singleCLMOpen);
        }

        [HttpPost]
        public ActionResult DetailClient(DipcadClaimOpenViewModels ClaimDetail, bool cbSendBack)
        {
            string isback = cbSendBack == true ? "2" : "3";

            //DataTable result = Common.ExecuteQuery($"dbo.sp_POS_Submit {PosDetail.CASE_ID},'{PosDetail.CIF}','{PosDetail.POLICY_NO}','','','{GetSession.UserName()}','','','{isback}','{DateTime.Now.ToString("yyyy-MM-dd")}','{(PosDetail.IS_PAID == true ? "1" : "0")}'");
            DataTable result = Common.ExecuteQuery($"dbo.SPUpd_CLM_Submit {ClaimDetail.CASE_ID},''"); //check what submit do in complete?

            string cek = result.Rows[0][0].ToString();

            if (cek == "SUCCESS")
            {
                _db.zsp_AuditTrailInsert("Claim Back Complete", ClaimDetail.POLICY_NO, GetSession.UserName());

                TempData["Success"] = $"{cek} change for Policy Number {ClaimDetail.POLICY_NO}";
            }
            else
            {
                TempData["Error"] = $"Error For : {cek}";
                return View(ClaimDetail);
            }

            return RedirectToAction("Index");
        }

        public ActionResult ImagesClient(int case_id)
        {
            string sourcePathTmp = string.Empty;
            string sourcePath = string.Empty;
            string filePath = string.Empty;
            var extension = string.Empty;
            byte[] fileByte;

            DipcadClaimListImageViewModel model = new DipcadClaimListImageViewModel();
            List<DetailImageViewModels> images = new List<DetailImageViewModels>();
            //var imagesList = _db.sp_POS_Images(case_id, GetSession.UserName());
            List<ImageResult> imagesList = new List<ImageResult>();

            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Images '{case_id}',''");
            int counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                imagesList.Add(new ImageResult()
                {
                    POLICYNO = dTab.Rows[i]["POLICYNO"].ToString(),
                    FILENAMEORIGIN = dTab.Rows[i]["FILENAMEORIGIN"].ToString(),
                    FILENAME = dTab.Rows[i]["FILENAME"].ToString(),
                    DOCUMENTNAME = dTab.Rows[i]["DOCUMENTNAME"].ToString(),
                    SOURCE = dTab.Rows[i]["SOURCE"].ToString(),
                    FILEIMAGE = (Byte[])dTab.Rows[i]["FILEIMAGE"],
                    CREATEDATE = Convert.ToDateTime(dTab.Rows[i]["CREATEDATE"].ToString()),
                    DESCRIPTION = dTab.Rows[i]["DESCRIPTION"].ToString(),
                });
            }

            foreach (var item in imagesList)
            {
                sourcePathTmp = Server.MapPath("~/Files/" + item.POLICYNO);
                sourcePath = Server.MapPath("~/Files/" + item.POLICYNO + "/");
                if (!Directory.Exists(sourcePath))
                {
                    Directory.CreateDirectory(sourcePath);
                }
                else
                {
                    filePath = sourcePath + item.FILENAME;
                    System.IO.File.Delete(filePath);
                }

                if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        fileByte = inStream.ToArray();
                        string bytes = BitConverter.ToString(fileByte).Replace("-", "");
                    }
                }
                else
                {
                    fileByte = item.FILEIMAGE;
                }

                byte[] data = (byte[])fileByte;

                string[] fileName = item.FILENAME.Split('.');
                filePath = sourcePath + fileName[0].ToString();

                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".tiff", data);
                }
                else
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".pdf", data);
                }

                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                {
                    extension = "image/jpg";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    #region ' Convert tiff to PDF '
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                    using (var stream = new MemoryStream())
                    {
                        using (var docs = new Document())
                        {
                            try
                            {
                                var writer =
                                iTextSharp.text.pdf.PdfWriter.GetInstance(docs, new FileStream(sourcePath + fileName[0] + ".pdf", FileMode.Create));

                                //count files images in tiff
                                Bitmap bMap = new Bitmap(sourcePath + fileName[0] + ".tiff");
                                //Bitmap bMap = new Bitmap(sourcePath + item.FILENAME);
                                int total = bMap.GetFrameCount(FrameDimension.Page);

                                docs.Open();

                                iTextSharp.text.pdf.PdfContentByte cbyte = writer.DirectContent;
                                for (int k = 0; k < total; ++k)
                                {
                                    bMap.SelectActiveFrame(FrameDimension.Page, k);
                                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bMap, ImageFormat.Bmp);

                                    //calculate dimension
                                    img.ScalePercent(50f / img.DpiX * 100);
                                    img.SetAbsolutePosition(0, 0);
                                    cbyte.AddImage(img);
                                    docs.NewPage();
                                }

                                docs.Close();
                                bMap.Dispose();

                                FileInfo dirInfo = new FileInfo(sourcePath + fileName[0] + ".tiff");
                                dirInfo.Delete();
                            }
                            catch
                            {
                                TempData["Error"] = "Image Corrupt CaseId : " + case_id;
                                return RedirectToAction("Index");
                            }
                        }
                    }
                    #endregion
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "pdf")
                {
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                }

                images.Add(new DetailImageViewModels()
                {
                    PolicyNumber = item.POLICYNO,
                    Checked = false,
                    DocumentName = item.DOCUMENTNAME,
                    FILENAME = item.FILENAME,
                    FILENAMEORIGIN = item.FILENAMEORIGIN,
                    Source = item.SOURCE,
                    FILEIMAGE = fileByte,
                    Extension = extension,
                    PreviewContentId = Common.SplitValueFileName(item.FILENAME),
                    FILEIMAGE_temp = item.FILEIMAGE,
                    FilePath = filePath,
                    CreatedDate = Convert.ToDateTime(item.CREATEDATE),
                    Description = item.DESCRIPTION
                });
            }

            model.Case_ID = case_id;
            model.DetailImageViewModels = images;

            string[] docsSplit;
            List<SelectListItem> dataDocumentType = new List<SelectListItem>();
            var documentTypeList = GetListDocumentType();
            dataDocumentType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentTypeList != null)
            {
                foreach (string item in documentTypeList)
                {
                    docsSplit = item.Split('|');
                    dataDocumentType.Add(new SelectListItem() { Text = docsSplit[3], Value = item });
                }
            }
            ViewBag.dllDocType = new SelectList(dataDocumentType, "Value", "Text", null);

            return View(model);
        }

        [HttpPost]
        public ActionResult ImagesClient(DipcadClaimListImageViewModel images, string Submit, string dllDocType, HttpPostedFileBase mdlImageFile, string workType)
        {
            try
            {
                string policyNo = string.Empty;
                string sourcePathTmp = string.Empty;
                string sourcePath = string.Empty;
                string filePath = string.Empty;
                string zipName = string.Empty;
                int count = 0;

                string[] arrFileName;
                string fileName = string.Empty;

                if (Submit == "Add Image")
                {
                    if (mdlImageFile != null)
                    {

                        string[] ddlDocType = dllDocType.Split('|');
                        int docIdValue = int.Parse(ddlDocType[0]);
                        string docTitleValue = ddlDocType[2];
                        string docTypeValue = ddlDocType[1];

                        byte[] data;
                        using (Stream inputStream = mdlImageFile.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            data = memoryStream.ToArray();
                        }

                        if (workType == null)
                        {
                            workType = GetWorkType(images.Case_ID);
                        }

                        try
                        {
                            //Common.ExecuteNonQuery( status );
                            Common.InsertMSTImageAdditional(images.PolicyNumber
                                                            , mdlImageFile.FileName.Replace(" ", "_").Replace("(", "_").Replace(")", "_")
                                                            , data
                                                            , images.Case_ID
                                                            , "CLAIM"
                                                            , docIdValue
                                                            , docTitleValue
                                                            , docTypeValue
                                                            , workType
                                                            , images.Description
                                                            , "ADDITIONAL");

                        }
                        catch (Exception err)
                        {
                            TempData["Error"] = err.Message;
                            goto End;
                        }
                        TempData["Success"] = "Add image for " + images.PolicyNumber + " successed";
                    }
                }
                else
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        foreach (var item in images.DetailImageViewModels)
                        {
                            if (item.Checked)
                            {
                                count++;
                                if (policyNo == string.Empty)
                                { policyNo = item.PolicyNumber; }

                                sourcePathTmp = Server.MapPath("~/Files/" + item.PolicyNumber);
                                sourcePath = Server.MapPath("~/Files/" + item.PolicyNumber + "/");
                                if (!Directory.Exists(sourcePath))
                                {
                                    Directory.CreateDirectory(sourcePath);
                                }

                                DataTable dTab = Common.ExecuteQuery($"[dbo].[SPInq_CLM_DetailImageViewTmp] '{item.PolicyNumber}','{item.FILENAMEORIGIN}'");
                                DetailImageTempView dataImage = new DetailImageTempView();

                                if (dTab.Rows.Count > 0)
                                {
                                    dataImage.FileImage = (Byte[])dTab.Rows[0]["FILEIMAGE"];
                                }

                                //var fileImageTmp = _db.SPInq_CLM_DetailImageViewTmp(item.PolicyNumber, item.FILENAMEORIGIN).ToList().Single().FILEIMAGE;
                                var fileImageTmp = dataImage.FileImage;
                                //byte[] data = (byte[])item.FILEIMAGE_temp;
                                byte[] data = (byte[])fileImageTmp;

                                System.IO.File.WriteAllBytes(sourcePath + item.FILENAME, data);

                                filePath = sourcePath + item.FILENAME;
                                zip.AddFile(filePath, "");
                            }
                        }

                        if (count == 0)
                        {
                            TempData["Error"] = "Please check before submit";
                            goto End;
                        }

                        _db.zsp_AuditTrailInsert("Download Image Open Claim", policyNo, GetSession.UserName());

                        Response.Clear();
                        Response.BufferOutput = false;
                        zipName = $"{policyNo}_{DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")}.zip";
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        Response.End();
                    }

                    foreach (var item in images.DetailImageViewModels)
                    {
                        if (item.Checked)
                        {
                            filePath = sourcePath + item.FILENAME;
                            System.IO.File.Delete(filePath);
                            if (item.Extension == "image/jpg")
                            {
                                arrFileName = item.FILENAME.Split('.');
                                fileName = arrFileName[0] + ".jpg";
                                filePath = sourcePath + fileName;
                                System.IO.File.Delete(filePath);
                            }
                        }
                    }
                    Directory.Delete(sourcePath);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        End:
            string[] docsSplit;
            List<SelectListItem> dataDocumentType = new List<SelectListItem>();
            var documentTypeList = GetListDocumentType();
            dataDocumentType.Add(new SelectListItem() { Text = "", Value = "" });
            if (documentTypeList != null)
            {
                foreach (string item in documentTypeList)
                {
                    docsSplit = item.Split('|');
                    dataDocumentType.Add(new SelectListItem() { Text = docsSplit[3], Value = item });
                }
            }
            ViewBag.dllDocType = new SelectList(dataDocumentType, "Value", "Text", null);
            //return View(images);
            return RedirectToAction("ImagesClient", new { case_id = images.Case_ID });
        }

        //------------------------------------------------------------------ attribut non action ---------------------------------

        public static List<DipcadClaimOpenViewModels> GetPolicyPoolList(string policyNumber, string user, string ddlCategory, string statusComplete, int pageNumber)
        {
            List<DipcadClaimOpenViewModels> models = new List<DipcadClaimOpenViewModels>();
            int counter = 0;

            if (ddlCategory == "ALL") ddlCategory = "";
            //ex exec dbo.SPInq_CLM_PolicyPool '1','10','and UserAssign = ''Budi Sudarsono''','','1'
            var userassign = "and UserAssign = ''" + GetSession.UserName() + "''";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_PolicyPool '{pageNumber}','10','{userassign}','','1'");
            counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                models.Add(new DipcadClaimOpenViewModels()
                {
                    //CASE_ID = Convert.ToInt32(dTab.Rows[i]["CASE_ID"].ToString()),
                    //CIF = dTab.Rows[i]["CIF"] == null ? string.Empty : dTab.Rows[i]["CIF"].ToString(),
                    POLICY_NO = dTab.Rows[i]["PolicyNo"].ToString(),
                    SOURCE = dTab.Rows[i]["Source"].ToString(),
                    //WORKTYPE_ID = dTab.Rows[i]["WORKTYPE_ID"].ToString(),
                    WORKTYPE = dTab.Rows[i]["WorkType"].ToString(),
                    CATEGORY = dTab.Rows[i]["Category"].ToString(),
                    RLS_TRANS = dTab.Rows[i]["RLSTRANS"].ToString(),
                    RLS_CAPTURE_DATE = String.IsNullOrWhiteSpace(dTab.Rows[i]["RLSCaptureDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["RLSCaptureDate"].ToString()),
                    USER_ASSIGN = dTab.Rows[i]["UserAssign"].ToString(),
                    CREATE_DATE = Convert.ToDateTime(dTab.Rows[i]["CreatedDate"].ToString()),
                    //REMARKS = dTab.Rows[i]["REMARKS"].ToString(),
                    //COUNTS = counter
                });
            }

            return models;
        }

        public static DipcadClaimOpenViewModels GetListCLMbyID(int? case_id)
        {
            DipcadClaimOpenViewModels model = null;

            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail '{case_id}','{GetSession.UserName()}'"); 
            //DataTable dTab = Common.ExecuteQuery($"dbo.sp_POS_Detail '29',''");
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Detail '{case_id}','{GetSession.UserName()}'");
            int count = dTab.Rows.Count;
            if (count > 0)
            {
                //if (!String.IsNullOrWhiteSpace(dTab.Rows[0]["DATE_COMPLETE"].ToString()))
                //{ date_comp = Convert.ToDateTime(dTab.Rows[0]["DATE_COMPLETE"].ToString()); }

                DataRow drow = dTab.Rows[0];

                model = new DipcadClaimOpenViewModels()
                {
                    CASE_ID = Convert.ToInt32(dTab.Rows[0]["CaseId"].ToString()),
                    //CIF = dTab.Rows[0]["CIF"].ToString(),
                    POLICY_NO = dTab.Rows[0]["PolicyNo"].ToString(),
                    SOURCE = dTab.Rows[0]["Source"].ToString(),
                    WORKTYPE = dTab.Rows[0]["WorkType"].ToString(),
                    CATEGORY = dTab.Rows[0]["Category"].ToString(),
                    RLS_TRANS = dTab.Rows[0]["RLSTrans"].ToString(),
                    RLS_CAPTURE_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["RLSCaptureDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["RLSCaptureDate"].ToString()),
                    USER_ASSIGN = dTab.Rows[0]["UserAssign"].ToString(),
                    CREATE_DATE = Convert.ToDateTime(dTab.Rows[0]["CreatedDate"].ToString()),
                    DATE_COMPLETE = String.IsNullOrWhiteSpace(dTab.Rows[0]["CompleteDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["CompleteDate"].ToString()),
                    SENT_TO_COMPLIENCE = false,
                    //SLA = Convert.ToInt32(dTab.Rows[0]["SLA"].ToString()),
                    //AGINGSLA = Convert.ToInt32(dTab.Rows[0]["AGINGSLA"].ToString()),
                    HOLDER = dTab.Rows[0]["Holder"].ToString(),
                    //REMARKS = dTab.Rows[0]["MESSAGE"].ToString(),
                    //IS_PAID = String.IsNullOrWhiteSpace(dTab.Rows[0]["IsPaid"].ToString()) || dTab.Rows[0]["IsPaid"].ToString() == "0" ? false : true,
                    IS_COMPLETE = dTab.Rows[0]["IsComplete"].ToString().ToLower() == "true" ? true : false,
                    IS_PAID = dTab.Rows[0]["IsPaid"].ToString().ToLower() == "true" ? true : false,
                    IS_PENDING = dTab.Rows[0]["IsPending"].ToString().ToLower() == "true" ? true : false,
                    IS_COMPLIANCE = dTab.Rows[0]["IsCompliance"].ToString().ToLower() == "true" ? true : false,
                    PRIORITY = dTab.Rows[0]["Priority"].ToString(),
                    VIP = dTab.Rows[0]["VIP"].ToString(),
                    CASE_TYPE = dTab.Rows[0]["CaseType"].ToString(),
                    INCURED_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["IncuredDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["IncuredDate"].ToString()),
                    IS_REJECT = dTab.Rows[0]["IsReject"].ToString().ToLower() == "true" ? true : false,
                    RECLAIMNO = dTab.Rows[0]["ReClaimNo"].ToString(),
                    CLAIMNO = dTab.Rows[0]["ClaimNo"].ToString(),
                    OWNER_NAME = dTab.Rows[0]["OwnerName"].ToString(),
                    OWNER_DOB = String.IsNullOrWhiteSpace(dTab.Rows[0]["OwnerDOB"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["OwnerDOB"].ToString()),
                    MEMBER_NO = dTab.Rows[0]["MemberNo"].ToString(),
                    MEMBER_NAME = dTab.Rows[0]["MemberName"].ToString(),
                    PRIMARY_DOC = dTab.Rows[0]["PrimaryDoc"].ToString(),
                    MEMBER_ID = dTab.Rows[0]["MemberId"].ToString(),
                    PRODUCT_CODE = dTab.Rows[0]["ProductCode"].ToString(),
                    DOC_SOURCE_TYPE = dTab.Rows[0]["DocumentSourceType"].ToString(),
                    //TSA use SumInsured Coloumn
                    TSA = dTab.Rows[0]["SumInsured"].ToString(),
                    PRODUCT_NAME = dTab.Rows[0]["ProductName"].ToString(),
                    PRODUCT_TYPE = dTab.Rows[0]["ProductType"].ToString(),
                    DOCUMENT_TITLE = dTab.Rows[0]["DocumentTitle"].ToString(),
                    DOCUMENT_TYPE = dTab.Rows[0]["DocumentType"].ToString(),
                    CHANNEL = dTab.Rows[0]["Channel"].ToString(),
                    PENDING_REASON = dTab.Rows[0]["ReasonPending"].ToString(),
                    FINISHPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["FinishPendingDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["FinishPendingDate"].ToString()),
                    REQPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["RequestPendingDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["RequestPendingDate"].ToString()),
                    REMINDERPENDATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["ReminderPendDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["ReminderPendDate"].ToString()),
                    RECEIVED_DATE = String.IsNullOrWhiteSpace(dTab.Rows[0]["ReceivedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[0]["ReceivedDate"].ToString()),
                    OWNER_ID = dTab.Rows[0]["OwnerId"].ToString(),
                    PENDING_LETTER = dTab.Rows[0]["PendingLetter"].ToString(),
                    REJECT_LETTER = dTab.Rows[0]["RejectLetter"].ToString(),

                    COUNTS = 1
                };
                if (dTab.Rows[0]["ClaimAmount"].ToString() != "")
                    model.CLAIM_AMOUNT = Convert.ToInt32(dTab.Rows[0]["ClaimAmount"].ToString());

                DataTable dTabAuditHistories = Common.ExecuteQuery($"dbo.SPInq_CLM_AuditJobHistory '{case_id}',''");
                var auditHistories = new List<AuditJobHistory>();
                if (dTabAuditHistories.Rows.Count > 0)
                {
                    for (int i = 0; i < dTabAuditHistories.Rows.Count; i++)
                    {
                        var auditHist = new AuditJobHistory();
                        auditHist.Action = dTabAuditHistories.Rows[i]["JOB"].ToString();
                        auditHist.Date = String.IsNullOrWhiteSpace(dTabAuditHistories.Rows[i]["DATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTabAuditHistories.Rows[i]["DATE"].ToString());
                        auditHist.User = dTabAuditHistories.Rows[i]["USER"].ToString();
                        auditHist.Comment = dTabAuditHistories.Rows[i]["COMMENT"].ToString();
                        auditHist.Value = dTabAuditHistories.Rows[i]["VALUE"].ToString();
                        auditHistories.Add(auditHist);
                    }
                }
                model.AuditJobHistories = auditHistories;
            }

            return model;
        }

        public List<string> GetListWorkType()
        {
            List<string> workType = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.SPInq_CLM_Worktype");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    workType.Add($"{rows[2].ToString()}|{rows[1].ToString()}");
                }
            }

            return workType;
        }

        public List<string> GetReferenceData(string referenceDataType)
        {
            List<string> penReasonType = new List<string>();
            var type = "type = ''" + referenceDataType + "'' and status = 1";
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_ReferenceData 1, 10, '{type}', '', '1'");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    penReasonType.Add($"{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return penReasonType;
        }

        public List<UserView> GetUserClaim()
        {
            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_getUserActive");
            List<UserView> usr = new List<UserView>();

            foreach (DataRow item in dTab.Rows)
            {
                if (item["USERNAME"].ToString() == GetSession.UserName()) continue;
                if (item["PREVILAGE_NAME"].ToString() == "Associate") continue;
                usr.Add(new UserView() { username = item["USERNAME"].ToString() });
            }

            return usr;
        }

        public string GetProductName(string productCode)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from zmaster_product where product_code = '{productCode}'");
            var productName = "";
            if (dTab.Rows.Count > 0)
                productName = dTab.Rows[0]["product_name"].ToString();
            return productName;
        }

        public string GetDocumentDescription(string docCode)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from clm_referencedata where code = '{docCode}'");
            var documentName = "";
            if (dTab.Rows.Count > 0)
                documentName = dTab.Rows[0]["Description"].ToString();
            return documentName;
        }

        public double SubstractDateTimeWithoutWeekend(DateTime endDate, DateTime startDate)
        {
            //hitung stardate hari per hari
            //cek dia hari apa, klo misal hari sabtu atau minggu, jangan masukan di hitungan jam
            double hours = 0.0;
            //double cekcekHours = 0.0;
            DateTime newdatePlusOne = default(DateTime);
            DateTime cektggl = startDate;

            //kurangin tanggal tersebut dari holiday setup
            var holidaySetup = GetHolidaySetup(startDate, endDate);

            while (endDate.AddDays(1) > startDate)
            {
                if (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    //cekcekHours = endDate.Subtract(newdatePlusOne).TotalHours;
                    startDate = startDate.AddDays(1);
                }
                else
                {
                    if (cektggl == startDate)
                    {
                        startDate = startDate.AddDays(1);
                    }
                    else
                    {
                        newdatePlusOne = startDate.AddDays(1);
                        //cekcekHours = endDate.Subtract(newdatePlusOne).TotalHours;
                        hours = hours + newdatePlusOne.Subtract(startDate).TotalHours;
                        startDate = startDate.AddDays(1);
                    }
                }
            }
            int minusHoliday = 0;
            if (holidaySetup.Count > 1)
            {
                foreach (var item in holidaySetup)
                {
                    if (!(item.HolidayDate.DayOfWeek == DayOfWeek.Saturday && item.HolidayDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        if (!(item.HolidayDate.Date == cektggl.Date))
                        {
                            minusHoliday = minusHoliday + (-24);
                        }
                    }
                }
            }
            else
            {
                if (!(holidaySetup[0].HolidayDate.DayOfWeek == DayOfWeek.Saturday && holidaySetup[0].HolidayDate.DayOfWeek == DayOfWeek.Sunday))
                {
                    if (holidaySetup[0].HolidayDate.Date != cektggl.Date)
                        minusHoliday = -24;
                }
            }
            //int cekHours = Convert.ToInt32(Math.Floor(hours + (cekcekHours*-1)));
            newdatePlusOne = newdatePlusOne.AddDays(-1);
            var hoursDiff = endDate.Subtract(newdatePlusOne).TotalHours;
            if (endDate.DayOfWeek == DayOfWeek.Saturday || endDate.DayOfWeek == DayOfWeek.Sunday)
                hoursDiff = 0.0;
            double cekHours = 0;
            //if (hoursDiff >= 24 && (newdatePlusOne.DayOfWeek != DayOfWeek.Saturday) && (newdatePlusOne.DayOfWeek != DayOfWeek.Sunday) && (newdatePlusOne.DayOfWeek != DayOfWeek.Friday))
            //    cekHours = Convert.ToInt32(Math.Floor(hours + hoursDiff));
            //else
            if (hoursDiff == 0)
                cekHours = Convert.ToInt32(Math.Floor(hours));
            else
                cekHours = Convert.ToInt32(Math.Floor(hours + hoursDiff));

            return cekHours;
        }

        public List<string> GetListDocumentType()
        {
            List<string> DocumentTipe = new List<string>();

            DataTable dTab = Common.ExecuteQuery("dbo.[SPInq_CLM_Document_Type]");

            if (dTab.Rows.Count > 0)
            {
                foreach (DataRow rows in dTab.Rows)
                {
                    DocumentTipe.Add($"{rows[0].ToString()}|{rows[1].ToString()}|{rows[2].ToString()}|{rows[3].ToString()}");
                }
            }

            return DocumentTipe;
        }

        public string GetWorkType(int caseid)
        {
            DataTable dTab = Common.ExecuteQuery($"select top 1 WorkType from clm_policy_pool where caseid = '{caseid}'");
            var worktype = "";
            if (dTab.Rows.Count > 0)
                worktype = dTab.Rows[0]["WorkType"].ToString();
            return worktype;
        }

        public List<HolidayList> GetHolidaySetup(DateTime stardate, DateTime enddate)
        {
            DataTable dTab = Common.ExecuteQuery($"select * from HolidaySetup order by HolidayDate");

            var holidayList = new List<HolidayList>();
            if (dTab.Rows.Count > 0)
            {
                for (int i = 0; i < dTab.Rows.Count; i++)
                {
                    var dateholiday = new HolidayList();
                    dateholiday.HolidayDate = Convert.ToDateTime(dTab.Rows[i]["HolidayDate"]);
                    holidayList.Add(dateholiday);
                }
            }

            holidayList = holidayList.Where(x => x.HolidayDate >= stardate).ToList();
            holidayList = holidayList.Where(x => x.HolidayDate <= enddate).ToList();
            return holidayList;
        }
    }
}
