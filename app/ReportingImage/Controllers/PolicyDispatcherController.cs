﻿using Ionic.Zip;
using ReportingImage.App_Start;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using ReportingImage.Controllers.libTiffNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Data;
using System.Text;
using iTextSharp.text;
using System.Web;

namespace ReportingImage.Controllers
{
    public class PolicyDispatcherController : Controller
    {
        // GET: PolicyDispatcher
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "Policy Dispatcher")]
        public ActionResult Index(string ddlBucket, string ddlStatusSPAJ, string policyNo, string ddlSource, string sortBy = "", string sortExpression = "ASC", int pageNumber = 1)
        {
            var userName = GetSession.UserName();
            var privilegeId = GetSession.Privilege_ID();
            var privilegeName = _db.zPrevilages.Where(a => a.id == privilegeId).Single().previlage_name;

            _db.zsp_AuditTrailInsert("Policy Dispatcher", null, userName);
            List<SelectListItem> listBucket = new List<SelectListItem>();
            var modelPolicy = new PagedList<PolicyDispatcherViewModels>();
            //modelPolicy.Content = GetPolicyDispatcherList(userName, "", "My Bucket", "ALL", "ALL", pageNumber, privilegeName, sortBy, sortExpression);

            if (privilegeName == "Super Admin" || privilegeName == "SPV" || privilegeName == "Managers") //add SPV and Managers to be able assign to other team
            {
                listBucket.Add(new SelectListItem { Value = "ALL", Text = "ALL" });
                modelPolicy.Content = GetPolicyDispatcherList(userName, "", "ALL", "ALL", "ALL", pageNumber, privilegeName, sortBy, sortExpression);

                if (ddlBucket == null && ddlStatusSPAJ == null && ddlSource == null)
                {
                    ddlBucket = "ALL";
                    ddlStatusSPAJ = "ALL";
                    ddlSource = "ALL";
                }
            }
            listBucket.Add(new SelectListItem { Value = "My Bucket", Text = "My Bucket" });
            if ((ddlBucket == "" || ddlBucket == null || ddlStatusSPAJ == null || ddlSource == null) && privilegeName != "Super Admin")
            {
                ddlBucket = "My Bucket";
                ddlStatusSPAJ = "ALL";
                ddlSource = "ALL";

                modelPolicy.Content = GetPolicyDispatcherList(userName, policyNo, ddlBucket, ddlStatusSPAJ, ddlSource, pageNumber, privilegeName, sortBy, sortExpression);

                if (ddlBucket == "My Bucket")
                    modelPolicy.Content = modelPolicy.Content.Where(x => x.status_policy != "Pending").ToList();

                //modelPolicy.Content = GetPolicyDispatcherList(userName, policyNo, ddlBucket, ddlStatusSPAJ, ddlSource, pageNumber, privilegeName, sortBy, sortExpression);
            }
            else
            {
                //var parentLists = _db.zsp_UserParentList(GetSession.User_ID()).ToList();
                //foreach (var itemUser in parentLists)
                //{
                //    userName += "," + itemUser.username;
                //    var parentListsA = _db.zsp_UserParentList(itemUser.id).ToList();
                //    foreach (var itemUserA in parentListsA)
                //    {
                //        userName += "," + itemUserA.username;
                //    }
                //}

                modelPolicy.Content = GetPolicyDispatcherList(userName, policyNo, ddlBucket, ddlStatusSPAJ, ddlSource, pageNumber, privilegeName, sortBy, sortExpression);
            }
            if (privilegeName == "Managers" || privilegeName == "SPV")
            {
                listBucket.Add(new SelectListItem { Value = "My Team", Text = "My Team" });
            }

            modelPolicy.TotalRecords = Convert.ToInt32(modelPolicy.Content.Count == 0 ? 0 : modelPolicy.Content.FirstOrDefault().Counts);
            modelPolicy.CurrentPage = pageNumber;
            modelPolicy.PageSize = 10;

            List<SelectListItem> isCleanSelectListItem = new List<SelectListItem>();
            isCleanSelectListItem.Add(new SelectListItem { Text = "Clean", Value = "2" });
            isCleanSelectListItem.Add(new SelectListItem { Text = "Non Clean", Value = "1" });

            List<SelectListItem> statusSPAJSelectListItem = new List<SelectListItem>();
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Open", Value = "Open" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Closed", Value = "Closed" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Pending", Value = "Pending" });

            List<SelectListItem> dataSourceSelectListItem = new List<SelectListItem>();
            dataSourceSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "Amost" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "RDS", Value = "RDS" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "Smart" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "PS", Value = "PS" });

            List<SelectListItem> userSelectListItem = new List<SelectListItem>();
            var userList = new List<string>();
            if (ddlBucket == "My Team")
            {
                userList = _db.zsp_DDLEscalate(GetSession.UserName(), privilegeName).ToList();//_db.zUsers.Where(a => a.parent_id == userId);
            }
            else if (ddlBucket == "ALL")
            {
                userList = _db.zUsers.Where(x => x.previlage_id == 6 || x.previlage_id == 7 && x.is_delete == false).Select(s => s.username).ToList();
            }

            if (userList.Count() != 0)
            {
                foreach (var item in userList)
                {
                    userSelectListItem.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            List<SelectListItem> policySelectListItem = new List<SelectListItem>();
            var policyList = modelPolicy.Content.Where(x => x.is_clean == 1 || x.is_clean == 2).Select(s => s.policy_no).ToList();
            if (policyList.Count() != 0)
            {
                foreach (var item in policyList)
                {
                    policySelectListItem.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            ViewBag.Bucket = ddlBucket;
            ViewBag.StatusSPAJ = ddlStatusSPAJ;
            ViewBag.UserNameLogin = GetSession.UserName();
            ViewBag.DataSource = ddlSource;
            ViewBag.SortBy = sortBy;
            ViewBag.PageNumber = pageNumber;
            ViewBag.PolicyNumber = policyNo;
            ViewBag.SortExpression = sortExpression;
            ViewBag.Previlage = GetSession.Privilege_ID();

            if (sortBy != ViewBag.SortBy.ToString())
            {
                ViewBag.SortExpression = "ASC";
            }

            ViewBag.isClean = new SelectList(isCleanSelectListItem, "Value", "Text");
            ViewBag.ddlBucket = new SelectList(listBucket, "Value", "Text", ddlBucket);
            ViewBag.Users = new SelectList(userSelectListItem, "Value", "Text");
            ViewBag.ddlStatusSPAJ = new SelectList(statusSPAJSelectListItem, "Value", "Text", ddlStatusSPAJ);
            ViewBag.ddlSource = new SelectList(dataSourceSelectListItem, "Value", "Text", ddlSource);
            ViewBag.policy = new SelectList(policySelectListItem, "Value", "Text");

            return View(modelPolicy);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Policy Dispatcher")]
        public ActionResult Index(string ddlBucket, string ddlStatusSPAJ, string policyNo, string ddlSource, string sortBy = "", string sortExpression = "ASC", int pageNumber = 1, int pageSize = 10)
        {
            var userName = GetSession.UserName();
            var privilegeId = GetSession.Privilege_ID();
            var privilegeName = _db.zPrevilages.Where(a => a.id == privilegeId).Single().previlage_name;
            var titleBtnEscalate = string.Empty;

            //List<PolicyDispatcherViewModels> modelPolicyTmp = new List<PolicyDispatcherViewModels>();
            _db.zsp_AuditTrailInsert("Policy Dispatcher", policyNo, GetSession.UserName());
            List<SelectListItem> listBucket = new List<SelectListItem>();
            var modelPolicy = new PagedList<PolicyDispatcherViewModels>();
            modelPolicy.Content = GetPolicyDispatcherList(userName, policyNo, ddlBucket, ddlStatusSPAJ, ddlSource, pageNumber, privilegeName, sortBy, sortExpression);

            if (ddlBucket == "My Bucket")
                modelPolicy.Content = modelPolicy.Content.Where(x => x.status_policy != "Pending").ToList();

            if (policyNo != null || policyNo != "")
            {
                pageNumber = 1;
            }

            if (privilegeName == "Managers" || privilegeName == "SPV")
            {
                titleBtnEscalate = "Re-Assign";
            }
            else
            {
                titleBtnEscalate = "Escalate";
            }

            if (GetSession.Privilege_ID() == 1 || GetSession.Privilege_ID() == 2 || GetSession.Privilege_ID() == 5)
            {
                listBucket.Add(new SelectListItem { Value = "ALL", Text = "ALL" });
            }
            listBucket.Add(new SelectListItem { Value = "My Bucket", Text = "My Bucket" });

            if (privilegeName == "Managers" || privilegeName == "SPV")
            {
                listBucket.Add(new SelectListItem { Value = "My Team", Text = "My Team" });
            }

            modelPolicy.TotalRecords = Convert.ToInt32(modelPolicy.Content.Count == 0 ? 0 : modelPolicy.Content.FirstOrDefault().Counts);
            modelPolicy.CurrentPage = pageNumber;
            modelPolicy.PageSize = pageSize;

            //if (ddlBucket == "My Team")
            //{
            //    var parentLists = _db.zsp_UserParentList(GetSession.User_ID()).ToList();
            //    foreach (var itemUser in parentLists)
            //    {
            //        userName += "," + itemUser.username;

            //        var parentListsA = _db.zsp_UserParentList(itemUser.id).ToList();
            //        foreach (var itemUserA in parentListsA)
            //        {
            //            userName += "," + itemUserA.username;
            //        }
            //    }
            //} 

            List<SelectListItem> isCleanSelectListItem = new List<SelectListItem>();
            isCleanSelectListItem.Add(new SelectListItem { Text = "Clean", Value = "2" });
            isCleanSelectListItem.Add(new SelectListItem { Text = "Non Clean", Value = "1" });

            List<SelectListItem> statusSPAJSelectListItem = new List<SelectListItem>();
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Open", Value = "Open" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Closed", Value = "Closed" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Jawaban Pending", Value = "Jawaban Pending" });
            statusSPAJSelectListItem.Add(new SelectListItem { Text = "Pending", Value = "Pending" });

            List<SelectListItem> dataSourceSelectListItem = new List<SelectListItem>();
            dataSourceSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "Amost" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "RDS", Value = "RDS" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "Smart" });

            List<SelectListItem> userSelectListItem = new List<SelectListItem>();
            var userList = new List<string>();
            //if (ddlBucket == "My Team")
            //{
            //    userList = _db.zsp_DDLEscalate(GetSession.UserName(), privilegeName).ToList();//_db.zUsers.Where(a => a.parent_id == userId);
            //}
            //else if (ddlBucket == "ALL")
            //{
            userList = _db.zUsers.Where(x => x.previlage_id == 6 || x.previlage_id == 7 && x.is_delete == false).Select(s => s.username).ToList();
            //}

            if (userList.Count() != 0)
            {
                foreach (var item in userList)
                {
                    userSelectListItem.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            List<SelectListItem> policySelectListItem = new List<SelectListItem>();
            //var policyList = modelPolicy.Content.Where(x => x.is_clean == 1 || x.is_clean == 2).Select(s => s.policy_no).ToList();
            var policyList = modelPolicy.Content.Select(s => s.policy_no).ToList();
            if (policyList.Count() != 0)
            {
                foreach (var item in policyList)
                {
                    policySelectListItem.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            ViewBag.Bucket = ddlBucket;
            ViewBag.StatusSPAJ = ddlStatusSPAJ;
            ViewBag.UserNameLogin = GetSession.UserName();
            ViewBag.DataSource = ddlSource;
            ViewBag.SortBy = sortBy;
            ViewBag.PageNumber = pageNumber;
            ViewBag.PolicyNumber = policyNo;
            ViewBag.SortExpression = sortExpression;
            ViewBag.Previlage = GetSession.Privilege_ID();

            if (sortBy != ViewBag.SortBy.ToString())
            {
                ViewBag.SortExpression = "ASC";
            }

            ViewBag.isClean = new SelectList(isCleanSelectListItem, "Value", "Text");
            ViewBag.ddlBucket = new SelectList(listBucket, "Value", "Text", ddlBucket);
            ViewBag.Users = new SelectList(userSelectListItem, "Value", "Text");
            ViewBag.ddlStatusSPAJ = new SelectList(statusSPAJSelectListItem, "Value", "Text", ddlStatusSPAJ);
            ViewBag.ddlSource = new SelectList(dataSourceSelectListItem, "Value", "Text", ddlSource);
            ViewBag.policy = new SelectList(policySelectListItem, "Value", "Text");

            return View(modelPolicy);
        }
        Byte[] fileByte;
        [CheckAuthorize(Roles = "Detail Image Dispatcher")]
        public ActionResult DetailImage(string ddlBucket, string ddlStatusSPAJ, string policyNo, string ddlSource, string sortBy = "", string sortExpression = "asc")
        {
            //checking storage
            _StorageChecking str = new _StorageChecking();
            if (!str.AvailableFreeSpace())
            {
                return PartialView("~/Views/Shared/Error_Storage.cshtml");
            }

            string sourcePath = string.Empty;
            string filePath = string.Empty;
            string filePathTemp = string.Empty;
            string sourcePathTmp = string.Empty;
            string checking = string.Empty;

            ViewBag.Bucket = ddlBucket;
            ViewBag.StatusSPAJ = ddlStatusSPAJ;
            ViewBag.UserNameLogin = GetSession.UserName();
            ViewBag.DataSource = ddlSource;
            ViewBag.SortBy = sortBy;
            ViewBag.PolicyNumber = policyNo;
            ViewBag.SortExpression = sortExpression;

            var extension = string.Empty;
            var detailImageList = _db.zsp_DetailImageView(policyNo).ToList();
            List<DetailImageViewModels> modelDetailImage = new List<DetailImageViewModels>();
            foreach (var item in detailImageList)
            {
                try
                {
                    //View Image with File Path
                    sourcePathTmp = Server.MapPath("~/Files/" + item.POLICYNO);
                    sourcePath = Server.MapPath("~/Files/" + item.POLICYNO + "/");
                    if (!Directory.Exists(sourcePath))
                    {
                        Directory.CreateDirectory(sourcePath);
                    }
                    else
                    {
                        filePath = sourcePath + item.FILENAME;
                        System.IO.File.Delete(filePath);
                    }

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        using (MemoryStream outStream = new MemoryStream())
                        {
                            //Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                            //fileByte = outStream.ToArray();

                            //Author    : Andhi
                            //Date      : 2020-24-07
                            //Desc      : convert tiff to pdf using for PS
                            fileByte = inStream.ToArray();
                        }
                    }
                    else
                    {
                        fileByte = item.FILEIMAGE;
                    }

                    byte[] data = (byte[])fileByte;

                    string[] fileName = item.FILENAME.Split('.');
                    filePath = sourcePath + fileName[0].ToString();

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        //System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);

                        //Author    : Andhi
                        //Date      : 2020-24-07
                        //Desc      : set name file as tiff
                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".tiff", data);

                        #region ' Convert tiff to PDF '
                        //Author    : Andhi
                        //Date      : 2020-24-07
                        //Desc      : converting tiff to pdf 

                        if (item.Source != "PS")
                        {
                            Convert_Tiff2PDF call = new Convert_Tiff2PDF(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                            checking = call.fromByte(data);
                        }
                        else
                        {
                            //cara 2
                            //tiff2pdf_others call = new tiff2pdf_others(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                            //checking = call.useSyncFusion();

                            //cara 3
                            tiff2pdf_itextshrp call = new tiff2pdf_itextshrp();
                            checking = call.converting(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                        }
                        if (!string.IsNullOrEmpty(checking))
                        {
                            //Common.ExecuteQuery("dbo.zsp_image_corrupt '" + item.FILENAME + "','" + checking + "','" + GetSession.UserName() + "'");
                            continue;
                            //TempData["Error"] = $"Error : {item.FILENAME} {checking}";
                            //return PartialView("~/Views/Shared/Error_Image.cshtml");
                        }
                        FileInfo dirInfo = new FileInfo(sourcePath + fileName[0] + ".tiff");
                        dirInfo.Delete();
                        //call.startConvertTiff2PDF();
                        #endregion
                    }
                    else if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                    {
                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);
                    }
                    else
                    {
                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".pdf", data);
                    }

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        //extension = "image/jpg";
                        //filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";

                        //View Image with Base 64
                        //extension = "image/jpg";
                        //using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        //using (MemoryStream outStream = new MemoryStream())
                        //{
                        //    Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                        //    fileByte = outStream.ToArray();
                        //}
                        extension = "application/pdf";
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";

                    }
                    else if (Common.GetStringRight(item.FILENAME, 3) == "pdf")
                    {
                        extension = "application/pdf";

                        //View Image with File Path
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";

                        //View Image with Base 64
                        //fileByte = item.FILEIMAGE;
                    }
                    else
                    {
                        extension = "image/jpg";
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";

                        //View Image with Base 64
                        extension = "image/jpg";
                        using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        using (MemoryStream outStream = new MemoryStream())
                        {
                            Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                            fileByte = outStream.ToArray();
                        }
                    }

                    modelDetailImage.Add(
                    new DetailImageViewModels
                    {
                        PolicyNumber = item.POLICYNO,
                        Checked = false,
                        DocumentName = item.DocumentName,
                        FILENAME = item.FILENAME,
                        Source = item.Source,
                        FILEIMAGE = fileByte,
                        Extension = extension,
                        PreviewContentId = Common.SplitValueFileName(item.FILENAME),
                        FILEIMAGE_temp = item.FILEIMAGE,
                        FilePath = filePath,
                        CreatedDate = Convert.ToDateTime(item.CREATEDATE)
                    });
                }
                catch (Exception err)
                {
                    Common.ExecuteQuery("dbo.zsp_image_corrupt '" + item.FILENAME + "','" + err.Message + "','" + GetSession.UserName() + "'");
                    //TempData["Error"] = item.FILENAME + " " + err.Message;
                    //return PartialView("~/Views/Shared/Error_Image.cshtml");
                }
            }
            ListViewModels model = new ListViewModels
            {
                DetailImageViewModels = modelDetailImage
            };

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Detail Image Dispatcher", policyNo, userName);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Detail Image Dispatcher")]
        public ActionResult DetailImage(ListViewModels zListVIewModels)
        {
            string policyNo = string.Empty;
            string sourcePath = string.Empty;
            string filePath = string.Empty;
            string sourcePathTmp = string.Empty;
            string zipName = string.Empty;
            string fileName = string.Empty;
            string[] arrFileName;
            byte[] filedata;

            try
            {
                if (zListVIewModels.DetailImageViewModels.Count == 0)
                {
                    TempData["Error"] = "Please check before submit";
                }

                #region ' zip than download '
                //// -----------------------------------------------------
                //// Author       : Andhi Sumarjo
                //// Date         : 4 Nov 2020
                //// Desc         : issue slow on Donwload. Sebelumnya Download dilakukan terlebih dahulu lalu di zip. ini membuat ukuran download menjadi lambat.
                //// -----------------------------------------------------

                //policyNo = zListVIewModels.DetailImageViewModels[0].PolicyNumber;
                //sourcePath = Server.MapPath("~/Files/" + policyNo + "/");
                //if (!Directory.Exists(sourcePath))
                //{
                //    Directory.CreateDirectory(sourcePath);
                //}
                //using (var zipFile = new ZipFile())
                //{
                //    foreach (var item in zListVIewModels.DetailImageViewModels)
                //    {
                //        if (item.Checked != false)
                //        {
                //            fileName = item.FILENAME.Contains(";") ? item.FILENAME.Split(';')[0] : item.FILENAME;
                //            arrFileName = fileName.Split('.');
                //            fileName = arrFileName[0] + "." + (arrFileName[1].ToUpper() == "TIFF" || arrFileName[1].ToUpper() == "TIF" ? "pdf" : arrFileName[1]);
                //            fileName = sourcePath + fileName;
                //            zipFile.AddFile(fileName, string.Empty);



                //        }
                //    }
                //    fileName = $"{sourcePath}{policyNo}_{DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")}.zip";
                //    zipFile.Save(fileName);




                //    filedata = System.IO.File.ReadAllBytes(fileName);
                //    System.IO.File.Delete(fileName);

                //    //Response.Clear();

                //    zipName = String.Format(policyNo + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                //    //Response.ContentType = "application/zip";
                //    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                //    //zipFile.Save(Response.OutputStream);
                //    //Response.End();


                //}

                #endregion

                #region ' download than zip'
                using (ZipFile zip = new ZipFile())
                {
                    //zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    foreach (var item in zListVIewModels.DetailImageViewModels)
                    {
                        if (item.Checked != false)
                        {
                            if (policyNo == string.Empty)
                            {
                                policyNo = item.PolicyNumber;
                                //zip.AddDirectoryByName(policyNo);
                            }
                            sourcePathTmp = Server.MapPath("~/Files/" + item.PolicyNumber);
                            sourcePath = Server.MapPath("~/Files/" + item.PolicyNumber + "/");
                            if (!Directory.Exists(sourcePath))
                            {
                                Directory.CreateDirectory(sourcePath);
                            }

                            var fileImageTmp = _db.zsp_DetailImageViewTmp(item.PolicyNumber, item.FILENAME).ToList().FirstOrDefault().FILEIMAGE;

                            //byte[] data = (byte[])item.FILEIMAGE_temp;
                            byte[] data = (byte[])fileImageTmp;

                            System.IO.File.WriteAllBytes(sourcePath + item.FILENAME, data);

                            filePath = sourcePath + item.FILENAME;
                            zip.AddFile(filePath, "");
                        }
                    }

                    _db.zsp_AuditTrailInsert("Download Detail Image Infomation", policyNo, GetSession.UserName());

                    Response.Clear();
                    Response.BufferOutput = false;
                    zipName = String.Format(policyNo + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                }


                foreach (var item in zListVIewModels.DetailImageViewModels)
                {
                    if (item.Checked != false)
                    {
                        filePath = sourcePath + item.FILENAME;
                        System.IO.File.Delete(filePath);

                        if (item.Extension == "image/jpg")
                        {
                            arrFileName = item.FILENAME.Split('.');
                            fileName = arrFileName[0] + ".jpg";
                            filePath = sourcePath + fileName;
                            System.IO.File.Delete(filePath);
                        }

                    }
                }
                Directory.Delete(sourcePath, true);
                #endregion
            }
            catch (Exception ex)
            {
                //throw;
                TempData["Error"] = $"Error : {ex.Message}";
                return PartialView("~/Views/Shared/Error_Image.cshtml");
            }

            //return File(filedata, MimeMapping.GetMimeMapping(fileName));
            return View(zListVIewModels);
        }


        public JsonResult IsCleanUpdate(string policyNo, string ddlIsClean)
        {
            string userName = GetSession.UserName();
            //string polNo = Common.GetStringRight(policyNo, 11);
            _db.zsp_IsCleanUpdate(policyNo, userName, ddlIsClean);
            if (ddlIsClean == "1")
            {
                ddlIsClean = "Non Clean";
                TempData["Success"] = policyNo + " Non Clean was successfully";
            }
            else if (ddlIsClean == "2")
            {
                ddlIsClean = "Is Clean";
                TempData["Success"] = policyNo + " Is Clean was successfully";
            }
            _db.zsp_AuditTrailInsert(ddlIsClean, policyNo, userName);
            return Json("Ok", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Escalate(string policyNo)
        {
            try
            {
                var privilegeId = GetSession.Privilege_ID();
                var privilegeName = _db.zPrevilages.Where(a => a.id == privilegeId).Single().previlage_name;

                EscalateViewModels model = new EscalateViewModels();
                model.Policy_Number = policyNo;
                model.ErrorUsers = "ErrorUser" + policyNo;
                model.ErrorRemarks = "ErrorRemarks" + policyNo;

                List<SelectListItem> userSelectListItem = new List<SelectListItem>();
                var userList = _db.zsp_DDLEscalate(GetSession.UserName(), privilegeName).ToList();//_db.zUsers.Where(a => a.parent_id == userId);
                if (userList.Count() != 0)
                {
                    foreach (var item in userList)
                    {
                        userSelectListItem.Add(new SelectListItem
                        {
                            Text = item,
                            Value = item
                        });
                    }
                }
                ViewBag.Users = new SelectList(userSelectListItem, "Value", "Text");

                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Escalate", null, userName);

                return PartialView("Escalate", model);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public JsonResult EscalateUpdate(string[] policyNo, string user, string remarks)
        {
            string userName = GetSession.UserName();
            string polList = string.Empty;
            var lastItem = policyNo.Last();
            //string polNo = Common.GetStringRight(policyNo, 11);
            foreach (string item in policyNo)
            {
                _db.zsp_EscalateUpdate(item, user, remarks, userName);
                _db.zsp_AuditTrailInsert("Escalate", item, userName);
                if (item != lastItem)
                    polList += item + ", ";
                else
                    polList += item;
            }

            TempData["Success"] = polList + " Escalate was successfully";
            return Json("Ok", JsonRequestBehavior.AllowGet);
        }
        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public ActionResult DownloadImage(string fileName)
        {
            var model = _db.zsp_DetailFile(fileName).ToList();
            Byte[] data = (Byte[])model.Single().FILEIMAGE;
            string ext = model.Single().Extension;
            string flName = model.Single().FILENAME;
            return File(data, ext, flName);
        }

        public ActionResult Closed(string policyNo)
        {
            if (policyNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zPolicy_Dispatch zPolicyDispatcher = _db.zPolicy_Dispatch.Find(policyNo);
            if (zPolicyDispatcher == null)
            {
                TempData["Error"] = "Policy No not Found";
            }
            _db.zsp_PolicyDispatcherUpdate(policyNo, "Closed");
            _db.zsp_AuditTrailInsert("Closed", policyNo, GetSession.UserName());
            TempData["Success"] = "Closed Success for " + zPolicyDispatcher.policy_no;
            return RedirectToAction("Index");
        }

        public ActionResult Open(string policyNo)
        {
            if (policyNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zPolicy_Dispatch zPolicyDispatcher = _db.zPolicy_Dispatch.Find(policyNo);
            if (zPolicyDispatcher == null)
            {
                TempData["Error"] = "Policy No not Found";
            }
            _db.zsp_PolicyDispatcherUpdate(policyNo, "Jawaban Pending");
            _db.zsp_AuditTrailInsert("Jawaban Pending", policyNo, GetSession.UserName());
            TempData["Success"] = "Reverse Success for " + zPolicyDispatcher.policy_no;
            return RedirectToAction("Index");
        }

        public ActionResult PreviewContent(string fileName)
        {
            try
            {
                var detailFile = _db.zsp_DetailFile(fileName).FirstOrDefault();
                DetailImageViewModels model = new DetailImageViewModels();
                model.Extension = detailFile.Extension;
                model.FILENAME = detailFile.FILENAME;
                model.FILEIMAGE = detailFile.FILEIMAGE;

                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Preview Content", null, userName);

                return PartialView("PreviewContent", model);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonResult HideRowStatusSPAJ()
        {
            bool x = false;
            var privelegeId = GetSession.Privilege_ID();
            var modelPrivilege = _db.zPrevilages.Find(privelegeId);
            if (modelPrivilege.previlage_name == "Managers" || modelPrivilege.previlage_name == "SPV" || modelPrivilege.previlage_name == "Super Admin")
            {
                x = true;
            }
            return Json(x, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SortByHeader(string userName, string policyNumber, string ddlBucket, string ddlSPAJ, string ddlDataSource, string sortBy, int pageNumber, string privilegeName)
        {
            string sortExpression = "asc";
            if (Session["sortBy"] == null)
            {
                Session["sortBy"] = sortBy;
                Session["sortExpression"] = "asc";
            }
            else if (Session["sortBy"].ToString() == sortBy)
            {
                sortExpression = "desc";
            }
            else if (Session["sortBy"].ToString() != sortBy)
            {
                sortExpression = "asc";
            }

            return Redirect("Index?ddlBucket=" + ddlBucket + "&&ddlStatusSPAJ=" + ddlSPAJ + "&&policyNo=" + policyNumber + "&&ddlSource=" + ddlDataSource + "&&sortBy=" + sortBy + "&&sortExpression=" + sortExpression + "&&pageNumber=" + pageNumber);
        }
        public static List<DetailImageViewModels> GetDetailImageList(string policyNumber, string fileName)
        {
            List<DetailImageViewModels> model = new List<DetailImageViewModels>();

            DataTable tbl = Common.ExecuteQuery("dbo.zsp_DetailImageViewTmp '" + policyNumber + "','" + fileName + "'");

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                byte[] fileImageTmp = Encoding.ASCII.GetBytes(tbl.Rows[i]["FILEIMAGE"].ToString());
                model.Add(new DetailImageViewModels()
                {
                    PolicyNumber = tbl.Rows[i]["POLICYNO"].ToString(),
                    DocumentName = tbl.Rows[i]["DocumentName"].ToString(),
                    FILENAME = tbl.Rows[i]["FILENAME"].ToString(),
                    Source = tbl.Rows[i]["Source"].ToString(),
                    FILEIMAGE = fileImageTmp
                });
            }

            return model;
        }
        public static List<PolicyDispatcherViewModels> GetPolicyDispatcherList(string userName, string policyNumber, string bucket, string statusSPAJ, string dataSource, int pageNumber, string privilegeName, string sortBy, string sortExpression)
        {
            List<PolicyDispatcherViewModels> model = new List<PolicyDispatcherViewModels>();

            var titleBtnEscalate = string.Empty;

            if (privilegeName == "Managers" || privilegeName == "SPV")
            {
                titleBtnEscalate = "Re-Assign";
            }
            else
            {
                titleBtnEscalate = "Escalate";
            }

            //DataTable tbl = Common.ExecuteQuery("dbo.zsp_PolicyDispatcher '" + userName + "','" + policyNumber + "','" + bucket + "','" + statusSPAJ + "','" + dataSource + "','" + pageNumber + "'");
            DataTable tbl = new DataTable();
            if (privilegeName == "Super Admin" || privilegeName == "SPV" || privilegeName == "Managers")
                tbl = Common.ExecuteQuery("dbo.zsp_PolicyDispatcher_New '" + userName + "','" + policyNumber + "','" + bucket + "','" + statusSPAJ + "','" + dataSource + "','" + sortBy + "','" + privilegeName + "','" + sortExpression + "','" + pageNumber + "'");
            else
                tbl = Common.ExecuteQuery("dbo.zsp_PolicyDispatcher_User '" + userName + "','" + policyNumber + "','" + bucket + "','" + statusSPAJ + "','" + dataSource + "','" + sortBy + "','" + privilegeName + "','" + sortExpression + "','" + pageNumber + "'");

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                var isEskalasi = Convert.ToBoolean(tbl.Rows[i]["is_escalate"].ToString());
                var isEskalasiView = string.Empty;
                if (isEskalasi == null || isEskalasi == false)
                {
                    isEskalasiView = "No";
                }
                else
                {
                    isEskalasiView = "Yes";
                }

                var isClean = Convert.ToInt32(tbl.Rows[i]["is_clean"].ToString());
                var isCleanView = string.Empty;
                if (isClean == 0)
                {
                    isCleanView = "";
                }
                else if (isClean == 1)
                {
                    isCleanView = "Non Clean";
                }
                else if (isClean == 2)
                {
                    isCleanView = "Clean";
                }

                model.Add(new PolicyDispatcherViewModels()
                {
                    policy_no = tbl.Rows[i]["policy_no"].ToString()
                    ,
                    status_policy = tbl.Rows[i]["status_policy"].ToString()
                    ,
                    ape = Convert.ToDecimal(tbl.Rows[i]["ape"].ToString())
                    ,
                    sum_assured = Convert.ToDecimal(tbl.Rows[i]["sum_assured"].ToString())
                    ,
                    type_product = tbl.Rows[i]["type_product"].ToString()
                    ,
                    is_escalate = isEskalasiView
                    ,
                    remark_escalate = tbl.Rows[i]["remark_escalate"].ToString()
                    ,
                    is_clean = Convert.ToInt32(tbl.Rows[i]["is_clean"].ToString())
                    ,
                    policy_no_closed = "Closed" + tbl.Rows[i]["policy_no"].ToString()
                    ,
                    policy_no_escalate = "Escalate" + tbl.Rows[i]["policy_no"].ToString()
                    ,
                    policy_no_isclean = "IsClean" + tbl.Rows[i]["policy_no"].ToString()
                    ,
                    title_btn_escalate = titleBtnEscalate
                    ,
                    date_submission = Convert.ToDateTime(tbl.Rows[i]["date_submission"].ToString())
                    ,
                    assigned_to = tbl.Rows[i]["assigned_to"].ToString()
                    ,
                    is_clean_name = isCleanView
                    ,
                    pa_date = !string.IsNullOrEmpty(tbl.Rows[i]["pa_date"].ToString()) ? Convert.ToDateTime(tbl.Rows[i]["pa_date"].ToString()) : (DateTime?)null
                    ,
                    Counts = Convert.ToInt32(tbl.Rows[i]["Counts"].ToString())
                });
            }
            return model;
        }
    }
}