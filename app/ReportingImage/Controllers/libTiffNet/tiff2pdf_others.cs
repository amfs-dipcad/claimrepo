﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System.Drawing;
using BitMiracle.LibTiff.Classic;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ReportingImage.Controllers.libTiffNet
{
    public class tiff2pdf_others
    {
        protected static string FromFile, ToFile;
        public tiff2pdf_others(string tiffDir, string pdfDir)
        {
            FromFile = tiffDir;
            ToFile = pdfDir;
        }

        public string useSyncFusion()
        {
            string result = string.Empty;
            PdfDocument docs = new PdfDocument();
            docs.PageSettings.Margins.All = 0;
            using (var tiffImage = new PdfBitmap(FromFile))
            {
                try
                {
                    int count = tiffImage.FrameCount;
                    for (int i = 0; i < count; i++)
                    {
                        var page = docs.Pages.Add();

                        SizeF pageSize = page.GetClientSize();

                        tiffImage.ActiveFrame = i;

                        page.Graphics.DrawImage(tiffImage, 0, 0, pageSize.Width, pageSize.Height);
                    }
                }
                catch(Exception err)
                {
                    result = err.Message;
                    goto iferr;
                }
            }

            docs.Save(ToFile);
            iferr:
            docs.Close(true);
            
            return result;
        }

        public Bitmap tifftoBitmap()
        {
            string result = string.Empty;

            using (Tiff input = Tiff.Open(FromFile, "r"))
            {
                // Find the width and height of the image
                FieldValue[] value = input.GetField(TiffTag.IMAGEWIDTH);
                int width = value[0].ToInt();

                value = input.GetField(TiffTag.IMAGELENGTH);
                int height = value[0].ToInt();

                // Read the image into the memory buffer
                int[] raster = new int[height * width];
                if (!input.ReadRGBAImage(width, height, raster))
                {
                    return null;
                }

                using (Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppRgb))
                {
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);

                    BitmapData bmpdata = bmp.LockBits(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
                    byte[] bits = new byte[bmpdata.Stride * bmpdata.Height];

                    for (int y = 0; y < bmp.Height; y++)
                    {
                        int rasterOffset = y * bmp.Width;
                        int bitsOffset = (bmp.Height - y - 1) * bmpdata.Stride;

                        for (int x = 0; x < bmp.Width; x++)
                        {
                            int rgba = raster[rasterOffset++];
                            bits[bitsOffset++] = (byte)((rgba >> 16) & 0xff);
                            bits[bitsOffset++] = (byte)((rgba >> 8) & 0xff);
                            bits[bitsOffset++] = (byte)(rgba & 0xff);
                            bits[bitsOffset++] = (byte)((rgba >> 24) & 0xff);
                        }
                    }

                    System.Runtime.InteropServices.Marshal.Copy(bits, 0, bmpdata.Scan0, bits.Length);
                    bmp.UnlockBits(bmpdata);

                    //bmp.Save("TiffTo32BitBitmap.bmp");
                    //System.Diagnostics.Process.Start("TiffTo32BitBitmap.bmp");
                    return bmp;
                }
            }

        }

        public string checkPageError()
        {
            string result = string.Empty;
            int width = 0;
            int height = 0;
            int directories = 0;
            int imageSize = 0;
            
            using (Tiff input = Tiff.Open(FromFile, "r"))
            {
                directories = input.NumberOfDirectories();

                for(int i = 0; i < directories; i++)
                {
                    input.SetDirectory((short)i);

                    width = input.GetField(TiffTag.IMAGEWIDTH)[0].ToInt();
                    height = input.GetField(TiffTag.IMAGELENGTH)[0].ToInt();

                    imageSize = width * height;
                    int[] raster = new int[imageSize];

                    if(!input.ReadRGBAImage(width, height, raster,true))
                    {
                        result = $"Page {i} is corrupted";
                        break;
                    }
                }
            }
            
            return result;
        }

    }
}