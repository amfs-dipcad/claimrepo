﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace ReportingImage.Controllers.libTiffNet
{
    public class tiff2pdf_itextshrp
    {
        public int count_page(string DirTiff)
        {
            int page = 0;
            Bitmap bMap = new Bitmap(DirTiff);
            page = bMap.GetFrameCount(FrameDimension.Page);
            return page;
        }

        public string converting(string Dirtiff, string Dirpdf)
        {
            string result = string.Empty;
            int total = 0;
            float prc, pos;

            using (var stream = new MemoryStream())
            {
                Bitmap bMap = new Bitmap(Dirtiff);
                using (var docs = new Document())
                {
                    //var ck = new tiff2pdf_others(Dirtiff, Dirpdf);
                    //Bitmap bMap = ck.tifftoBitmap();
                    try {
                        var writter = 
                            iTextSharp.text.pdf.PdfWriter.GetInstance(docs, new FileStream(Dirpdf, FileMode.Create));
                        
                        total = bMap.GetFrameCount(FrameDimension.Page);

                        docs.Open();

                        iTextSharp.text.pdf.PdfContentByte cbyte = writter.DirectContent;
                        for (int i = 0; i < total; ++i)
                        {
                            bMap.SelectActiveFrame(FrameDimension.Page, i);
                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bMap, ImageFormat.Bmp);
                            
                            pos = bMap.Height;
                            //calculate dimension
                            if (docs.PageSize.Width < bMap.Width)
                            {
                                prc = docs.PageSize.Width / bMap.Width * 100;
                                img.ScalePercent(prc);
                                pos = pos * prc / 100;
                            }
                            img.SetAbsolutePosition(10, docs.PageSize.Height-pos-10);
                            //img.ScaleAbsoluteHeight(bMap.Height);
                            //img.ScaleAbsoluteWidth(bMap.Width);
                            cbyte.AddImage(img);
                            docs.NewPage();
                        }

                        docs.Close();
                        bMap.Dispose();
                        //FileInfo dirInfo = new FileInfo(Dirtiff);
                        //dirInfo.Delete();
                    }
                    catch(Exception err)
                    {
                        if (docs.IsOpen()) docs.Close();
                        bMap.Dispose();
                        return err.Message;
                    }
                }
            }

            return result;
        }

        public bool isErrorPDF(string Directory)
        {
            StringBuilder txt = new StringBuilder();

            if(File.Exists(Directory))
            {
                PdfReader pdfRead = new PdfReader(Directory);

                try
                {
                    for (int page = 0; page <= pdfRead.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentText = PdfTextExtractor.GetTextFromPage(pdfRead, page, strategy);

                        currentText = Encoding.UTF8.GetString(
                            ASCIIEncoding.Convert(
                                Encoding.Default,
                                Encoding.UTF8,
                                Encoding.Default.GetBytes(currentText)));
                        txt.Append(currentText);
                    }
                    pdfRead.Close();
                }catch
                {
                    pdfRead.Close();
                    return true;
                }
            }

            return false;
        }
    }
}