﻿using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ReportingImage.App_Start;
using ReportingImage.Controllers.libTiffNet;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class PolicyInformationController : Controller
    {
        // GET: PolicyInformation
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "Policy Information")]
        public ActionResult Index(string policyNumber, string ddlSource, int pageNumber = 1)
        {
            var userName = GetSession.UserName();
            TempData["ddlSource"] = ddlSource;
            //var policyDispatcherList = _db.zsp_PolicyInformation(searchBy).ToList();

            List<SelectListItem> dataSourceSelectListItem = new List<SelectListItem>();
            dataSourceSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            //dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "DLFE" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "Amost" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "RDS", Value = "RDS" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "Smart" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "PS", Value = "PS" });
            //dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "EAPPS" });

            var modelPolicy = new PagedList<PolicyDispatcherViewModels>();
            //modelPolicy.Content = GetPolicyInformationList(policyNumber, ddlSource, pageNumber);
            
            // Dwi Suderajat 20211117
            // Changes NB get Policy Information List
            modelPolicy.Content = new List<PolicyDispatcherViewModels>();

            modelPolicy.TotalRecords = Convert.ToInt32(modelPolicy.Content.Count == 0 ? 0 : modelPolicy.Content.FirstOrDefault().Counts);
            modelPolicy.CurrentPage = pageNumber;
            modelPolicy.PageSize = 10;

            ViewBag.ddlSource = new SelectList(dataSourceSelectListItem, "Value", "Text", ddlSource);

            _db.zsp_AuditTrailInsert("Policy Information", policyNumber, GetSession.UserName());

            return View(modelPolicy);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Policy Information")]
        public ActionResult Index(string policyNumber, string ddlSource, int pageNumber = 1, int pageSize = 10)
        {
            var userName = GetSession.UserName();
            TempData["ddlSource"] = ddlSource;
            //var policyDispatcherList = _db.zsp_PolicyInformation(searchBy).ToList();

            List<SelectListItem> dataSourceSelectListItem = new List<SelectListItem>();
            dataSourceSelectListItem.Add(new SelectListItem { Text = "ALL", Value = "ALL" });
            //dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "DLFE" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Amost", Value = "Amost" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "RDS", Value = "RDS" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "Smart" });
            dataSourceSelectListItem.Add(new SelectListItem { Text = "PS", Value = "PS" });
            //dataSourceSelectListItem.Add(new SelectListItem { Text = "Smart", Value = "EAPPS" });

            var modelPolicy = new PagedList<PolicyDispatcherViewModels>();
            modelPolicy.Content = GetPolicyInformationList(policyNumber, ddlSource, pageNumber);
            modelPolicy.TotalRecords = Convert.ToInt32(modelPolicy.Content.Count == 0 ? 0 : modelPolicy.Content.FirstOrDefault().Counts);
            modelPolicy.CurrentPage = pageNumber;
            modelPolicy.PageSize = 10;

            _db.zsp_AuditTrailInsert("Policy Information", policyNumber, GetSession.UserName());

            ViewBag.ddlSource = new SelectList(dataSourceSelectListItem, "Value", "Text", ddlSource);

            return View(modelPolicy);
        }

        public ActionResult DetailFile(string fileName)
        {
            try
            {
                var model = _db.zsp_DetailFile(fileName).ToList();
                //Byte[] data = (Byte[])model.Single().FILEIMAGE;
                //string ext = model.Single().Extension;
                //string flName = model.Single().FILENAME;
                //return File(data, ext, flName);
                var userName = GetSession.UserName();
                _db.zsp_AuditTrailInsert("Detail File", null, userName);

                return PartialView("DetailFile", model.FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        Byte[] fileByte;
        [CheckAuthorize(Roles = "Detail Image Information")]
        public ActionResult DetailImage(string policyNumber, string ddlSource)
        {
            //checking storage
            _StorageChecking str = new _StorageChecking();
            if (!str.AvailableFreeSpace())
            {
                return PartialView("~/Views/Shared/Error_Storage.cshtml");
            }

            TempData["tempPolicyNo"] = policyNumber;
            TempData["ddlSource"] = ddlSource;

            string sourcePath = string.Empty;
            string filePath = string.Empty;
            string filePathTemp = string.Empty;
            string sourcePathTmp = string.Empty;
            string checking = string.Empty;

            var extension = string.Empty;
            var detailImageList = _db.zsp_DetailImageView(policyNumber).ToList();
            List<DetailImageViewModels> modelDetailImage = new List<DetailImageViewModels>();
            foreach (var item in detailImageList)
            {
                try
                {
                    #region ' put image '
                    checking = item.FILENAME;
                    //View Image with File Path
                    sourcePathTmp = Server.MapPath("~/Files/" + item.POLICYNO);
                    sourcePath = Server.MapPath("~/Files/" + item.POLICYNO + "/");
                    if (!Directory.Exists(sourcePath))
                    {
                        Directory.CreateDirectory(sourcePath);
                    }
                    else
                    {
                        filePath = sourcePath + item.FILENAME;
                        System.IO.File.Delete(filePath);
                        //Directory.Delete(sourcePath);
                    }

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        using (MemoryStream outStream = new MemoryStream())
                        {
                            //Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                            //fileByte = outStream.ToArray();

                            //Author    : Andhi
                            //Date      : 2020-24-07
                            //Desc      : convert tiff to pdf using for PS

                            fileByte = inStream.ToArray();
                        }
                    }
                    else
                    {
                        fileByte = item.FILEIMAGE;
                    }

                    byte[] data = (byte[])fileByte;

                    string[] fileName = item.FILENAME.Split('.');
                    filePath = sourcePath + fileName[0].ToString();

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        //System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);

                        //Author    : Andhi
                        //Date      : 2020-24-07
                        //Desc      : set name file as tiff

                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".tiff", data);

                        #region ' Convert tiff to PDF '
                        //cara 1
                        if (item.Source != "PS")
                        {
                            Convert_Tiff2PDF call = new Convert_Tiff2PDF(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                            checking = call.fromByte(data);
                        }
                        else
                        {
                            //cara 2
                            //tiff2pdf_others call = new tiff2pdf_others(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                            //checking = call.useSyncFusion();

                            //cara 3
                            tiff2pdf_itextshrp call = new tiff2pdf_itextshrp();
                            checking = call.converting(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");
                        }


                        if (!string.IsNullOrEmpty(checking))
                        {
                            Common.ExecuteQuery("dbo.zsp_image_corrupt '" + item.FILENAME + "','" + checking + "','" + GetSession.UserName() + "'");
                            continue;
                            //TempData["Error"] = $"Error : {item.FILENAME} {checking}";
                            //return PartialView("~/Views/Shared/Error_Image.cshtml");
                        }


                        //tiff2pdf_itextshrp itxt = new tiff2pdf_itextshrp();
                        //itxt.converting(sourcePath + fileName[0] + ".tiff", sourcePath + fileName[0] + ".pdf");

                        //FileInfo dirInfo = new FileInfo(sourcePath + fileName[0] + ".tiff");
                        //dirInfo.Delete();
                        //call.startConvertTiff2PDF();
                        #endregion

                    }
                    else if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                    {
                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);
                    }
                    else
                    {
                        System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".pdf", data);
                    }

                    if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                    {
                        //extension = "image/jpg";
                        //filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";

                        //View Image with Base 64
                        //extension = "image/jpg";
                        //using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        //using (MemoryStream outStream = new MemoryStream())
                        //{
                        //    Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                        //    fileByte = outStream.ToArray();
                        //}

                        extension = "application/pdf";
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                    }
                    else if (Common.GetStringRight(item.FILENAME, 3) == "pdf")
                    {
                        extension = "application/pdf";

                        //View Image with File Path
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";

                        //View Image with Base 64
                        //fileByte = item.FILEIMAGE;
                    }
                    else
                    {
                        extension = "image/jpg";
                        filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";

                        //View Image with Base 64
                        extension = "image/jpg";
                        using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                        using (MemoryStream outStream = new MemoryStream())
                        {
                            Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                            fileByte = outStream.ToArray();
                        }
                    }

                    modelDetailImage.Add(
                    new DetailImageViewModels
                    {
                        PolicyNumber = item.POLICYNO,
                        Checked = false,
                        DocumentName = item.DocumentName,
                        FILENAME = item.FILENAME,
                        Source = item.Source,
                        FILEIMAGE = fileByte,
                        Extension = extension,
                        //Fix by DWI Sudrajat,
                        //Id error with white space
                        PreviewContentId = Common.SplitValueFileName(item.FILENAME).Replace(" ","_"),
                        FILEIMAGE_temp = item.FILEIMAGE,
                        FilePath = filePath,
                        CreatedDate = Convert.ToDateTime(item.CREATEDATE)
                    });
                    #endregion
                }
                catch (Exception err)
                {
                    Common.ExecuteQuery("dbo.zsp_image_corrupt '" + item.FILENAME + "','" + err.Message + "','" + GetSession.UserName() + "'");
                    //TempData["Error"] = item.FILENAME + " " + err.Message;
                    //return PartialView("~/Views/Shared/Error_Image.cshtml");

                }

            }
            ListViewModels model = new ListViewModels
            {
                DetailImageViewModels = modelDetailImage
            };

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Detail Image Information", policyNumber, userName);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Detail Image Information")]
        public ActionResult DetailImage(ListViewModels zListVIewModels)
        {
            string policyNo = string.Empty;
            string sourcePath = string.Empty;
            string filePath = string.Empty;
            string sourcePathTmp = string.Empty;
            string zipName = string.Empty;
            string fileName = string.Empty;
            string[] arrFileName;
            byte[] filedata;

            try
            {
                if (zListVIewModels.DetailImageViewModels.Count == 0)
                {
                    TempData["Error"] = "Please check before submit";
                }

                #region ' zip than download '
                // -----------------------------------------------------
                // Author       : Andhi Sumarjo
                // Date         : 4 Nov 2020
                // Desc         : issue slow on Donwload. Sebelumnya Download dilakukan terlebih dahulu lalu di zip. ini membuat ukuran download menjadi lambat.
                // -----------------------------------------------------

                //policyNo = zListVIewModels.DetailImageViewModels[0].PolicyNumber;
                //sourcePath = Server.MapPath("~/Files/" + policyNo + "/");
                //if (!Directory.Exists(sourcePath))
                //{
                //    Directory.CreateDirectory(sourcePath);
                //}
                //using (var zipFile = new ZipFile())
                //{
                //    foreach (var item in zListVIewModels.DetailImageViewModels)
                //    {
                //        if (item.Checked != false)
                //        {
                //            fileName = item.FILENAME.Contains(";") ? item.FILENAME.Split(';')[0] : item.FILENAME;
                //            arrFileName = fileName.Split('.');
                //            fileName = arrFileName[0] +"."+ (arrFileName[1].ToUpper() == "TIFF" || arrFileName[1].ToUpper() == "TIF" ? "pdf" : arrFileName[1]);
                //            fileName = sourcePath + fileName;
                //            zipFile.AddFile(fileName, string.Empty);

                //            //Directory.Delete(fileName);
                //        }
                //    }
                //    fileName = $"{sourcePath}{policyNo}_{DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")}.zip";
                //    zipFile.Save(fileName);

                //    filedata = System.IO.File.ReadAllBytes(fileName);
                //    System.IO.File.Delete(fileName);

                //    //Response.Clear();

                //    zipName = String.Format(policyNo + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                //    //Response.ContentType = "application/zip";
                //    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                //    //zipFile.Save(Response.OutputStream);
                //    //Response.End();


                //}

                #endregion

                #region ' download than zip'
                using (ZipFile zip = new ZipFile())
                {
                    //zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    foreach (var item in zListVIewModels.DetailImageViewModels)
                    {
                        if (item.Checked != false)
                        {
                            if (policyNo == string.Empty)
                            {
                                policyNo = item.PolicyNumber;
                                //zip.AddDirectoryByName(policyNo);
                            }
                            sourcePathTmp = Server.MapPath("~/Files/" + item.PolicyNumber);
                            sourcePath = Server.MapPath("~/Files/" + item.PolicyNumber + "/");
                            if (!Directory.Exists(sourcePath))
                            {
                                Directory.CreateDirectory(sourcePath);
                            }

                            var fileImageTmp = _db.zsp_DetailImageViewTmp(item.PolicyNumber, item.FILENAME).ToList().FirstOrDefault().FILEIMAGE;

                            //byte[] data = (byte[])item.FILEIMAGE_temp;
                            byte[] data = (byte[])fileImageTmp;

                            System.IO.File.WriteAllBytes(sourcePath + item.FILENAME, data);

                            filePath = sourcePath + item.FILENAME;
                            zip.AddFile(filePath, "");
                        }
                    }

                    _db.zsp_AuditTrailInsert("Download Detail Image Infomation", policyNo, GetSession.UserName());

                    Response.Clear();
                    Response.BufferOutput = false;
                    zipName = String.Format(policyNo + "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                }


                foreach (var item in zListVIewModels.DetailImageViewModels)
                {
                    if (item.Checked != false)
                    {
                        filePath = sourcePath + item.FILENAME;
                        System.IO.File.Delete(filePath);

                        if (item.Extension == "image/jpg")
                        {
                            arrFileName = item.FILENAME.Split('.');
                            fileName = arrFileName[0] + ".jpg";
                            filePath = sourcePath + fileName;
                            System.IO.File.Delete(filePath);
                        }

                    }
                }
                Directory.Delete(sourcePath, true);
                #endregion
            }
            catch (Exception ex)
            {
                //throw;
                TempData["Error"] = $"Error : {ex.Message}";
                return PartialView("~/Views/Shared/Error_Image.cshtml");
            }

            //return File(filedata, MimeMapping.GetMimeMapping(fileName));
            return View(zListVIewModels);
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public ActionResult DownloadImage(string fileName)
        {
            var model = _db.zsp_DetailFile(fileName).ToList();
            Byte[] data = (Byte[])model.Single().FILEIMAGE;
            string ext = model.Single().Extension;
            string flName = model.Single().FILENAME;
            return File(data, ext, flName);
        }

        public static List<PolicyDispatcherViewModels> GetPolicyInformationList(string policyNumber, string dataSource, int pageNumber)
        {
            List<PolicyDispatcherViewModels> model = new List<PolicyDispatcherViewModels>();

            DataTable tbl = Common.ExecuteQuery("dbo.zsp_PolicyInformation '" + policyNumber + "','" + dataSource + "','" + pageNumber + "'");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {

                var isEskalasiView = string.Empty;
                if (tbl.Rows[i]["is_escalate"] == null || tbl.Rows[i]["is_escalate"].ToString() == "")
                {
                    isEskalasiView = "No";
                }
                else
                {
                    var isEskalasi = Convert.ToBoolean(tbl.Rows[i]["is_escalate"].ToString());
                    if (isEskalasi == false)
                    {
                        isEskalasiView = "No";
                    }
                    else
                    {
                        isEskalasiView = "Yes";
                    }
                }

                model.Add(new PolicyDispatcherViewModels()
                {
                    policy_no = tbl.Rows[i]["policy_no"].ToString()
                    ,
                    insured_name = tbl.Rows[i]["insured_name"].ToString()
                    ,
                    dob_insured = Convert.ToDecimal(tbl.Rows[i]["dob_insured"].ToString())
                    ,
                    dob_owner = Convert.ToDecimal(tbl.Rows[i]["dob_owner"].ToString())
                    ,
                    sex_insured = tbl.Rows[i]["sex_insured"].ToString()
                    ,
                    sex_owner = tbl.Rows[i]["sex_owner"].ToString()
                    ,
                    status_policy = tbl.Rows[i]["status_policy"].ToString()
                    ,
                    owner_name = tbl.Rows[i]["owner_name"].ToString()
                    ,
                    currency = tbl.Rows[i]["currency"].ToString()
                    ,
                    ape = (!string.IsNullOrEmpty(tbl.Rows[i]["ape"].ToString()) ? Convert.ToDecimal(tbl.Rows[i]["ape"].ToString()) : default(decimal?))
                    ,
                    sum_assured = Convert.ToDecimal(tbl.Rows[i]["sum_assured"].ToString())
                    ,
                    type_product = tbl.Rows[i]["type_product"].ToString()
                    ,
                    payment_type = tbl.Rows[i]["payment_type"].ToString()
                    ,
                    product_code = tbl.Rows[i]["product_code"].ToString()
                    ,
                    assigned_to = tbl.Rows[i]["assigned_to"].ToString()
                    ,
                    batch_id = Convert.ToInt32(tbl.Rows[i]["batch_id"].ToString())
                    ,
                    remark_escalate = tbl.Rows[i]["remark_escalate"].ToString()
                    ,
                    is_escalate = isEskalasiView
                    ,
                    Counts = Convert.ToInt32(tbl.Rows[i]["Counts"].ToString())
                });
            }
            return model;
        }
    }
}