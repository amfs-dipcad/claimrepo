﻿using Newtonsoft.Json;
using OfficeOpenXml;
using ReportingImage.App_Start;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        ReportingImageEntities _db = new ReportingImageEntities();
        [CheckAuthorize(Roles = "Report")]
        public ActionResult Index()
        {
            List<ListViewModels> typeOfUwList = new List<ListViewModels>();
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "", TypeOfUwText = "" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "ALL", TypeOfUwText = "ALL" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "Jet UW", TypeOfUwText = "Jet UW" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "Manual UW", TypeOfUwText = "Manual UW" });

            ViewBag.typeOfUw = new SelectList(typeOfUwList, "TypeOfUwId", "TypeOfUwText");

            //var reportList = _db.zsp_Report("","","").ToList();
            DataTable tbl = Common.ExecuteQuery("dbo.zsp_Report '','','' ");
            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report", null, userName);

            List<ReportViewModels> model = new List<ReportViewModels>();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Report")]
        public ActionResult Index(string Submit, string txbDateFrom, string txbDateTo, string typeOfUw)
        {
            List<ListViewModels> typeOfUwList = new List<ListViewModels>();
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "", TypeOfUwText = "" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "ALL", TypeOfUwText = "ALL" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "Jet UW", TypeOfUwText = "Jet UW" });
            typeOfUwList.Add(new ListViewModels { TypeOfUwId = "Manual UW", TypeOfUwText = "Manual UW" });

            ViewBag.typeOfUw = new SelectList(typeOfUwList, "TypeOfUwId", "TypeOfUwText", typeOfUw);

            //var reportList = _db.zsp_Report(txbDateFrom, txbDateTo, typeOfUw).ToList();
            DataTable tbl = Common.ExecuteQuery("dbo.zsp_Report '" + txbDateFrom + "','" + txbDateTo + "','" + typeOfUw + "'");
            List<ReportViewModels> model = new List<ReportViewModels>();
            //foreach(var item in reportList)
            //{
            //    model.Add(new ReportViewModels()
            //    {
            //        WDOS = item.WDOS,
            //        WPMODE = item.WPMODE,
            //        WTTPRM = item.WTTPRM,
            //        NOMOR_POLIS = item.NOMOR_POLIS,
            //        PLAN_CODE = item.PLAN_CODE,
            //        Remark = item.Remark,
            //        Status_Dispatcher = item.Status_Dispatcher,
            //        source_data = item.source_data,
            //        Type_of_UW = item.Type_of_UW
            //    });
            //}

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                model.Add(new ReportViewModels()
                {
                    WDOS = tbl.Rows[i]["WDOS"].ToString(),
                    WPMODE = tbl.Rows[i]["WPMODE"].ToString(),
                    WTTPRM = tbl.Rows[i]["WTTPRM"].ToString(),
                    NOMOR_POLIS = tbl.Rows[i]["NOMOR_POLIS"].ToString(),
                    PLAN_CODE = tbl.Rows[i]["PLAN_CODE"].ToString(),
                    Remark = tbl.Rows[i]["Remark"].ToString(),
                    Status_Dispatcher = tbl.Rows[i]["Status_Dispatcher"].ToString(),
                    source_data = tbl.Rows[i]["source_data"].ToString(),
                    Type_of_UW = tbl.Rows[i]["Type_of_UW"].ToString(),
                    User = tbl.Rows[i]["User"].ToString(),
                    PADate = tbl.Rows[i]["pa_date"].ToString()
                });
            }

            if (Submit == "Download")
            {
                ExcelPackage package = new ExcelPackage();
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Report");
                ws.Cells["A1"].Value = "No";
                ws.Cells["B1"].Value = "WDOS";
                ws.Cells["C1"].Value = "WPMODE";
                ws.Cells["D1"].Value = "WTTPRM";
                ws.Cells["E1"].Value = "Policy Number";
                ws.Cells["F1"].Value = "Plan Code";
                ws.Cells["G1"].Value = "Remark";
                ws.Cells["H1"].Value = "Status Dispatcher";
                ws.Cells["I1"].Value = "Source Data";
                ws.Cells["J1"].Value = "Type of UW";
                ws.Cells["K1"].Value = "User";
                ws.Cells["L1"].Value = "PA Date";

                for (int i = 1; i <= 10; i++)
                {
                    ws.Column(i).AutoFit();
                }

                int no = 1;
                int idx = 2;
                //foreach(var item in reportList)
                //{
                //    ws.Cells["A" + idx.ToString()].Value = no.ToString();
                //    ws.Cells["B" + idx.ToString()].Value = item.WDOS;
                //    ws.Cells["C" + idx.ToString()].Value = item.WPMODE;
                //    ws.Cells["D" + idx.ToString()].Value = item.WTTPRM;
                //    ws.Cells["E" + idx.ToString()].Value = item.NOMOR_POLIS;
                //    ws.Cells["F" + idx.ToString()].Value = item.PLAN_CODE;
                //    ws.Cells["G" + idx.ToString()].Value = item.Remark;
                //    ws.Cells["H" + idx.ToString()].Value = item.Status_Dispatcher;
                //    ws.Cells["I" + idx.ToString()].Value = item.source_data;
                //    ws.Cells["J" + idx.ToString()].Value = item.Type_of_UW;
                //    no++;
                //    idx++;
                //}
                for (int i = 0; i < tbl.Rows.Count; i++)
                {
                    ws.Cells["A" + idx.ToString()].Value = no.ToString();
                    ws.Cells["B" + idx.ToString()].Value = tbl.Rows[i]["WDOS"].ToString();
                    ws.Cells["C" + idx.ToString()].Value = tbl.Rows[i]["WPMODE"].ToString();
                    ws.Cells["D" + idx.ToString()].Value = tbl.Rows[i]["WTTPRM"].ToString();
                    ws.Cells["E" + idx.ToString()].Value = tbl.Rows[i]["NOMOR_POLIS"].ToString();
                    ws.Cells["F" + idx.ToString()].Value = tbl.Rows[i]["PLAN_CODE"].ToString();
                    ws.Cells["G" + idx.ToString()].Value = tbl.Rows[i]["Remark"].ToString();
                    ws.Cells["H" + idx.ToString()].Value = tbl.Rows[i]["Status_Dispatcher"].ToString();
                    ws.Cells["I" + idx.ToString()].Value = tbl.Rows[i]["source_data"].ToString();
                    ws.Cells["J" + idx.ToString()].Value = tbl.Rows[i]["Type_of_UW"].ToString();
                    ws.Cells["K" + idx.ToString()].Value = tbl.Rows[i]["User"].ToString();
                    ws.Cells["L" + idx.ToString()].Value = tbl.Rows[i]["pa_date"].ToString();

                    no++;
                    idx++;
                }

                string fileName = "Report_" + Convert.ToDateTime(DateTime.Now).Year + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + ".xlsx";
                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                    package.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report", null, userName);

            return View(model);
        }
        [CheckAuthorize(Roles = "Report Weekly")]
        public ActionResult Week()
        {
            var getMonth = DateTime.Now.ToString("MM-yyyy");
            var weekList = GetReportWeekly(getMonth);

            TempData["monthlyToday"] = getMonth;
            ViewBag.DataPoints = JsonConvert.SerializeObject(weekList, _jsonSetting);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Weekly", null, userName);

            return View(weekList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Report Weekly")]
        public ActionResult Week(string txbDateMonthly)
        {
            var weekList = GetReportWeekly(txbDateMonthly);

            TempData["monthlyToday"] = txbDateMonthly;
            ViewBag.DataPoints = JsonConvert.SerializeObject(weekList, _jsonSetting);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Weekly", null, userName);

            return View(weekList);
        }
        [CheckAuthorize(Roles = "Report Hoursly")]
        public ActionResult Hoursly()
        {
            var getDate = DateTime.Now.ToString("dd-MM-yyyy");
            var hoursList = GetReportHoursly(getDate);

            TempData["hourslyToday"] = getDate;
            ViewBag.DataPoints = JsonConvert.SerializeObject(hoursList, _jsonSetting);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Hoursly", null, userName);

            return View(hoursList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Report Hoursly")]
        public ActionResult Hoursly(string txbDate)
        {
            var hoursList = GetReportHoursly(txbDate);

            TempData["hourslyToday"] = txbDate;
            ViewBag.DataPoints = JsonConvert.SerializeObject(hoursList, _jsonSetting);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Hoursly", null, userName);

            return View(hoursList);
        }

        [CheckAuthorize(Roles = "Report Target Actual")]
        public ActionResult TargetActual()
        {
            var userManagersList = _db.zsp_DDLUserManagers().ToList();
            var userManager = userManagersList.FirstOrDefault().Value;
            var targetActualList = GetReportTargetActual(userManager);

            ViewBag.DataPoints = JsonConvert.SerializeObject(targetActualList, _jsonSetting);
            ViewBag.ddlTeams = new SelectList(userManagersList, "Value", "Text",userManager);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Target Actual", null, userName);

            return View(targetActualList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Report Target Actual")]
        public ActionResult TargetActual(string ddlTeams)
        {
            var targetActualList = GetReportTargetActual(ddlTeams);
            var userManagersList = _db.zsp_DDLUserManagers().ToList();

            ViewBag.DataPoints = JsonConvert.SerializeObject(targetActualList, _jsonSetting);
            ViewBag.ddlTeams = new SelectList(userManagersList, "Value", "Text",ddlTeams);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Report Target Actual", null, userName);

            return View(targetActualList);
        }
        public static List<WeekViewModels> GetReportWeekly(string monthly)
        {
            List<WeekViewModels> model = new List<WeekViewModels>();
            DataTable tbl = Common.ExecuteQuery("dbo.zsp_Report_Weekly '" + monthly + "'");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                model.Add(new WeekViewModels()
                {
                    Row_Label = tbl.Rows[i]["Name"].ToString()
                    ,Week_1 = Convert.ToInt32(tbl.Rows[i]["Week1"].ToString())
                    ,Week_2 = Convert.ToInt32(tbl.Rows[i]["Week2"].ToString())
                    ,Week_3 = Convert.ToInt32(tbl.Rows[i]["Week3"].ToString())
                    ,Week_4 = Convert.ToInt32(tbl.Rows[i]["Week4"].ToString())
                    ,Week_5 = Convert.ToInt32(tbl.Rows[i]["Week5"].ToString())
                });
            }

            return model;
        }

        public static List<HoursViewModels> GetReportHoursly(string date)
        {
            List<HoursViewModels> model = new List<HoursViewModels>();
            DataTable tbl = Common.ExecuteQuery("dbo.zsp_ReportPerHours '" + date + "'");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                model.Add(new HoursViewModels()
                {
                    Row_Label = tbl.Rows[i]["Row_Label"].ToString()
                    ,Eight = Convert.ToInt32(tbl.Rows[i]["08_00"].ToString())
                    ,Nine = Convert.ToInt32(tbl.Rows[i]["09_00"].ToString())
                    ,Ten = Convert.ToInt32(tbl.Rows[i]["10_00"].ToString())
                    ,Eleven = Convert.ToInt32(tbl.Rows[i]["11_00"].ToString())
                    ,Twelve = Convert.ToInt32(tbl.Rows[i]["12_00"].ToString())
                    ,Thirteen = Convert.ToInt32(tbl.Rows[i]["13_00"].ToString())
                    ,FourTeen = Convert.ToInt32(tbl.Rows[i]["14_00"].ToString())
                    ,FifTeen = Convert.ToInt32(tbl.Rows[i]["15_00"].ToString())
                    ,SixTeen = Convert.ToInt32(tbl.Rows[i]["16_00"].ToString())
                    ,SevenTeen = Convert.ToInt32(tbl.Rows[i]["17_00"].ToString())
                    ,Eighteen = Convert.ToInt32(tbl.Rows[i]["18_00"].ToString())
                    ,Total = Convert.ToInt32(tbl.Rows[i]["Total"].ToString())
                });
            }

            return model;
        }

        public static List<TargetActualViewModels> GetReportTargetActual(string team)
        {
            List<TargetActualViewModels> model = new List<TargetActualViewModels>();
            DataTable tbl = Common.ExecuteQuery("dbo.zsp_ReportTargetActual '" + team + "'");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                model.Add(new TargetActualViewModels()
                {
                    Team = tbl.Rows[i]["Team"].ToString()
                    ,Target = Convert.ToInt32(tbl.Rows[i]["Target"].ToString())
                    ,Actual = Convert.ToInt32(tbl.Rows[i]["Actual"].ToString())
                    ,GAP = Convert.ToInt32(tbl.Rows[i]["GAP"].ToString())
                    ,Total = Convert.ToInt32(tbl.Rows[i]["Total"].ToString())
                });
            }

            return model;
        }
        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
    }
}