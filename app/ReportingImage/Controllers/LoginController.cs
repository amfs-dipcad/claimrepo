﻿using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        ReportingImageEntities _db = new ReportingImageEntities();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(zUser zUser, string returnUrl)
        {
            try
            {
                if (zUser.username == null) { ModelState.AddModelError("username", "This field is required."); }
                if (zUser.password == null) { ModelState.AddModelError("password", "This field is required."); }
                if (ModelState.IsValid)
                {

                    string Pass = Common.EncodePasswordMd5(zUser.password);
                    var Dt = (from c in _db.UserViewLogins
                              where c.username == zUser.username && c.username != "Administrator"
                              select c);
                    if (Dt.ToList().Count != 0)
                    {
                        bool Result = false;
                        bool windowsAuthentication = Convert.ToBoolean(Dt.Single().active_directory);
                        if (windowsAuthentication)
                        {
                            Session["User_ID"] = Dt.Single().id;
                            Session["Password"] = zUser.password;

                            string result = LDPALogin(zUser.username, zUser.password, Dt.Single().password);
                            if (result == "success")
                            {
                                Result = true;
                            }
                        }
                        else
                        {
                            if (Dt.Single().password == Pass)
                            {
                                Result = true;
                            }
                        }
                        if (Result)
                        {
                            Session["User_ID"] = Dt.Single().id;
                            Session["Privilege_ID"] = Dt.Single().previlage_id;
                            Session["User_Name"] = Dt.Single().username;
                        }
                        else
                        {
                            TempData["Error"] = "User " + zUser.username + " and Password not match!!";
                            return View(zUser);
                        }

                        _db.zsp_AuditTrailInsert("Login", null, zUser.username);
                        _db.zsp_LoginUpdate(Dt.Single().id, true);
                    }
                    else
                    {
                        if (zUser.username.ToUpper() == "ADMINISTRATOR" && zUser.password == "123456")
                        {
                            Session["User_ID"] = "1";
                            Session["Privilege_ID"] = "1";
                            Session["User_Name"] = "Administrator";
                        }
                        else
                        {
                            TempData["Error"] = "User " + zUser.username + " and Password not match!!";
                            return View(zUser);
                        }
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);

                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }


            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message.ToString() + " Please check your connection or contact Administator";
            }

            return View(zUser);
        }

        private string LDPALogin(string UserName, string Password, string PassEncrypt)
        {
            String adPath = ConfigurationManager.AppSettings["LDAPPath"].ToString();
            String domain = ConfigurationManager.AppSettings["LDAPDomain"].ToString();

            LdapAuthentication adAuth = new LdapAuthentication(adPath);

            string message = "";
            try
            {
                if (true == adAuth.IsAuthenticated(domain, UserName, Password))
                {
                    zUser zUser = new zUser();
                    zUser.id = GetSession.User_ID();
                    UpdateLDPA(zUser.id, Password);

                    message = "success";
                    return message;
                }
                else
                {
                    message = "Username or password is wrong";
                    return message;
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233088)
                {
                    string Pass = Common.EncodePasswordMd5(Password);
                    if (Pass == PassEncrypt)
                    {
                        message = "success";
                        return message;
                    }
                }
                message = ex.Message;
                return message;
            }

        }

        public ActionResult Logout(string UserID)
        {
            try
            {
                //_db.zsp_AuditTrailInsert("Log Out by Click", null, GetSession.UserName());
                _db.zsp_LoginUpdate(GetSession.User_ID(), false);
                Common.RemoveSession(int.Parse(Session["User_ID"].ToString()));
                Session.RemoveAll();
                //bool Result = ILogin.LogOutIsLogin(UserID);
            }
            catch (Exception e)
            {
                return View();
            }
            return RedirectToAction("Index");
        }

        private void UpdateLDPA(int id, string Pwd)
        {
            try
            {
                zUser DBPassword = _db.zUsers.Find(id);
                if (DBPassword != null)
                {
                    ReportingImageEntities db = new ReportingImageEntities();
                    DBPassword.password = Common.EncodePasswordMd5(Pwd);
                    DBPassword.session_id = this.Session.SessionID; // store session id
                    db.Entry(DBPassword).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpGet]
        public ActionResult ExtendSession()
        {
            var data = new { IsSuccess = true };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}