﻿using OfficeOpenXml;
using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class DipcadClaimReportController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        // GET: DipcadClaimReport
        public ActionResult Index(string policyNumber, string receiveddatefrom = null, string receiveddateto = null, string ddlListRFStatus = null, string ddlListRFClaimType = null)
        {
            try
            {
                var User = GetSession.UserName();

                // Get Dropdown -----------------------------------------------------------------------------

                var tempResultListReferenceData = _db.CLM_ReferenceData.Where(a => a.Status == true).ToList();

                ViewBag.ListRFStatus = tempResultListReferenceData.Where(a => a.Type == "DipcadStatus").ToList();
                List<SelectListItem> dataRFStatus = new List<SelectListItem>();
                dataRFStatus.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
                foreach (var item in ViewBag.ListRFStatus)
                {
                    dataRFStatus.Add(new SelectListItem() { Text = item.Description, Value = item.Code });
                }
                ViewBag.ListRFStatus = new SelectList(dataRFStatus, "Value", "Text", "");

                ViewBag.ListRFClaimType = tempResultListReferenceData.Where(a => a.Type == "ClaimType").ToList();
                List<SelectListItem> dataRFClaimType = new List<SelectListItem>();
                dataRFClaimType.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
                foreach (var item in ViewBag.ListRFClaimType)
                {
                    dataRFClaimType.Add(new SelectListItem() { Text = item.Description, Value = item.Code });
                }
                ViewBag.ListRFClaimType = new SelectList(dataRFClaimType, "Value", "Text", "");

                if ((receiveddatefrom != null && receiveddatefrom != "") ||
                    (receiveddateto != null && receiveddateto != "") ||
                    (ddlListRFStatus != null && ddlListRFStatus != "") ||
                    (ddlListRFClaimType != null && ddlListRFClaimType != "")
                )
                {
                    // Get List Report -----------------------------------------------------------------------------

                    var tempResultListReport = _db.SPCLM_PolicyPoolReport_Inq().ToList();

                    if (receiveddatefrom != null && receiveddatefrom != "")
                    {
                        DateTime dateTimereceiveddatefrom = Convert.ToDateTime(receiveddatefrom);
                        tempResultListReport = tempResultListReport.Where(a => a.Createddate.Date >= dateTimereceiveddatefrom).ToList();
                    }

                    if (receiveddateto != null && receiveddateto != "")
                    {
                        DateTime dateTimereceiveddateto = Convert.ToDateTime(receiveddateto);
                        tempResultListReport = tempResultListReport.Where(a => a.Createddate.Date <= dateTimereceiveddateto).ToList();
                    }

                    if (policyNumber != null && policyNumber != "")
                    {
                        tempResultListReport = tempResultListReport.Where(a => a.PolicyNo == policyNumber).ToList();
                    }

                    if (ddlListRFStatus != null && ddlListRFStatus != "")
                    {
                        if (ddlListRFStatus != "ALL")
                        {
                            if (ddlListRFStatus == "OPEN")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsComplete != true && a.IsPending != true).ToList();
                            }

                            if (ddlListRFStatus == "PENDING")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsPending == true).ToList();
                            }

                            if (ddlListRFStatus == "COMPLETE")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsComplete == true).ToList();
                            }
                        }
                    }

                    if (ddlListRFClaimType != null && ddlListRFClaimType != "")
                    {
                        if (ddlListRFClaimType == "ALL")
                        {
                            tempResultListReport = tempResultListReport.ToList();
                        }
                        else
                        {
                            tempResultListReport = tempResultListReport.Where(a => a.ClaimType?.ToUpper() == ddlListRFClaimType.ToUpper()).ToList();
                        }
                    }

                    tempResultListReport = tempResultListReport.OrderBy(a => a.CaseId).ToList();

                    ViewBag.ListPolicyPool = tempResultListReport
                        .Select((x, index) => new {
                            No = index +1, //1
                            x.CaseId, //2
                            x.PolicyNo, //3
                            x.VIP, //3
                            x.ReceivedDate, //4
                            x.Createddate, //5
                            x.ReclaimNo, //6
                            x.ClaimNo, //7
                            x.SLACLaim, //8
                            x.SLAPending, //9
                            x.ClaimType, //10
                            x.Status, //11
                            x.ReasonReject, //12
                            x.RejectLetter, //13
                            x.ReasonPending, //14
                            x.RequestPendingDate, //15
                            x.FinishPendingDate, //16
                            x.PendingLetter, //17
                            x.CompletedDate, //18
                            x.Source, //19
                            x.UserFCR, //20
                            x.UserApprove, //21
                            x.UserInvestigation //22
                        }).ToList();

                    if (tempResultListReport.Count > 0)
                    {
                        var stream = new MemoryStream();
                        using (var package = new ExcelPackage(stream))
                        {
                            var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                            workSheet.Cells.LoadFromCollection(ViewBag.ListPolicyPool, true); 
                            workSheet.Cells[workSheet.Dimension.Address].AutoFitColumns();
                            workSheet.Column(6).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(5).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(16).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(17).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(19).Style.Numberformat.Format = "yyyy-mm-dd";

                            workSheet.Column(14).Width = 50; 
                            workSheet.Column(14).Style.WrapText = true;
                            workSheet.Column(18).Width = 50;
                            workSheet.Column(18).Style.WrapText = true;

                            for (int i = 0; i < tempResultListReport.Count; i++)
                            {
                                if (tempResultListReport[i].RejectLetter != null && tempResultListReport[i].RejectLetter != "")
                                    workSheet.Row(i + 1).CustomHeight = false;
                                if (tempResultListReport[i].PendingLetter != null && tempResultListReport[i].PendingLetter != "")
                                    workSheet.Row(i + 1).CustomHeight = false;
                            }
                            
                            package.Save();
                        }
                        stream.Position = 0;
                        string excelName = $"ClaimReport-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                        return File(stream, "application/octet-stream", excelName);
                    }
                }
                else
                {
                    //TempData["Error"] = "Filter must be fill";
                }

                _db.zsp_AuditTrailInsert("Claim Report", policyNumber, GetSession.UserName());
            }
            catch(Exception ex)
            {
                TempData["Error"] = ex.Message;
            }

            return View();
        }
        public ActionResult DownloadExcelEPPlus(string policyNumber, string receiveddatefrom = null, string receiveddateto = null, string ddlListRFStatus = null, string ddlListRFClaimType = null)
        {
            var User = GetSession.UserName();

            // Get List Report -----------------------------------------------------------------------------

            var tempResultListReport = _db.SPCLM_PolicyPool_Inq().Where(x => User == x.UserAssign.Trim()).ToList();

            if (receiveddatefrom != null && receiveddatefrom != "")
            {
                DateTime dateTimereceiveddatefrom = Convert.ToDateTime(receiveddatefrom);
                tempResultListReport = tempResultListReport.Where(a => a.CreatedDate.Date >= dateTimereceiveddatefrom).ToList();
            }
            if (receiveddateto != null && receiveddateto != "")
            {
                DateTime dateTimereceiveddateto = Convert.ToDateTime(receiveddateto);
                tempResultListReport = tempResultListReport.Where(a => a.CreatedDate.Date <= dateTimereceiveddateto).ToList();
            }
            if (policyNumber != null && policyNumber != "")
            {
                tempResultListReport = tempResultListReport.Where(a => a.PolicyNo == policyNumber).ToList();
            }

            ViewBag.ListPolicyPool = tempResultListReport;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(ViewBag.ListPolicyPool, true);
                workSheet.Column(10).Style.Numberformat.Format = "yyyy-mm-dd";
                package.Save();
            }
            stream.Position = 0;
            string excelName = $"ClaimReport-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
            return File(stream, "application/octet-stream", excelName);
        }

        public ActionResult Daily(string policyNumber, string receiveddatefrom = null, string receiveddateto = null, string ddlListRFStatus = null, string ddlListRFClaimType = null)
        {
            try
            {
                var User = GetSession.UserName();

                // Get Dropdown -----------------------------------------------------------------------------

                var tempResultListReferenceData = _db.CLM_ReferenceData.Where(a => a.Status == true).ToList();

                ViewBag.ListRFStatus = tempResultListReferenceData.Where(a => a.Type == "DipcadStatus").ToList();
                List<SelectListItem> dataRFStatus = new List<SelectListItem>();
                dataRFStatus.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
                foreach (var item in ViewBag.ListRFStatus)
                {
                    dataRFStatus.Add(new SelectListItem() { Text = item.Description, Value = item.Code });
                }
                ViewBag.ListRFStatus = new SelectList(dataRFStatus, "Value", "Text", "");

                ViewBag.ListRFClaimType = tempResultListReferenceData.Where(a => a.Type == "ClaimType").ToList();
                List<SelectListItem> dataRFClaimType = new List<SelectListItem>();
                dataRFClaimType.Add(new SelectListItem() { Text = "ALL", Value = "ALL" });
                foreach (var item in ViewBag.ListRFClaimType)
                {
                    dataRFClaimType.Add(new SelectListItem() { Text = item.Description, Value = item.Code });
                }
                ViewBag.ListRFClaimType = new SelectList(dataRFClaimType, "Value", "Text", "");

                if ((receiveddatefrom != null && receiveddatefrom != "") ||
                    (receiveddateto != null && receiveddateto != "") ||
                    (ddlListRFStatus != null && ddlListRFStatus != "") ||
                    (ddlListRFClaimType != null && ddlListRFClaimType != "")
                )
                {
                    // Get List Report -----------------------------------------------------------------------------

                    var tempResultListReport = _db.SPCLM_PolicyPoolReport_Daily_Inq().ToList();

                    if (receiveddatefrom != null && receiveddatefrom != "")
                    {
                        DateTime dateTimereceiveddatefrom = Convert.ToDateTime(receiveddatefrom);
                        tempResultListReport = tempResultListReport.Where(a => a.Createddate.Date >= dateTimereceiveddatefrom).ToList();
                    }

                    if (receiveddateto != null && receiveddateto != "")
                    {
                        DateTime dateTimereceiveddateto = Convert.ToDateTime(receiveddateto);
                        tempResultListReport = tempResultListReport.Where(a => a.Createddate.Date <= dateTimereceiveddateto).ToList();
                    }

                    if (policyNumber != null && policyNumber != "")
                    {
                        tempResultListReport = tempResultListReport.Where(a => a.PolicyNo == policyNumber).ToList();
                    }

                    if (ddlListRFStatus != null && ddlListRFStatus != "")
                    {
                        if (ddlListRFStatus != "ALL")
                        {
                            if (ddlListRFStatus == "OPEN")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsComplete != true && a.IsPending != true).ToList();
                            }

                            if (ddlListRFStatus == "PENDING")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsPending == true).ToList();
                            }

                            if (ddlListRFStatus == "COMPLETE")
                            {
                                tempResultListReport = tempResultListReport.Where(a => a.IsComplete == true).ToList();
                            }
                        }
                    }

                    if (ddlListRFClaimType != null && ddlListRFClaimType != "")
                    {
                        if (ddlListRFClaimType == "ALL")
                        {
                            tempResultListReport = tempResultListReport.ToList();
                        }
                        else
                        {
                            tempResultListReport = tempResultListReport.Where(a => a.ClaimType?.ToUpper() == ddlListRFClaimType.ToUpper()).ToList();
                        }
                    }

                    tempResultListReport = tempResultListReport.OrderBy(a => a.CaseId).ToList();

                    ViewBag.ListPolicyPool = tempResultListReport
                        .Select((x, index) => new {
                            No = index + 1,
                            x.CaseId,
                            x.PolicyNo,
                            x.VIP,
                            x.ReceivedDate,
                            x.Createddate,
                            x.ReclaimNo,
                            x.ClaimNo,
                            x.SLACLaim,
                            x.SLAPending,
                            x.ClaimType,
                            x.ReasonReject,
                            x.ReasonPending,
                            x.RequestPendingDate,
                            x.FinishPendingDate,
                            x.StatusCase,
                            x.Source,
                            x.UserFCR,
                            x.UserApprove,
                            x.UserInvestigation,
                            x.Category,
                            x.CompletedDate
                        }).ToList();

                    if (tempResultListReport.Count > 0)
                    {
                        var stream = new MemoryStream();
                        using (var package = new ExcelPackage(stream))
                        {
                            var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                            workSheet.Cells.LoadFromCollection(ViewBag.ListPolicyPool, true);
                            workSheet.Cells[workSheet.Dimension.Address].AutoFitColumns();
                            workSheet.Column(5).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(6).Style.Numberformat.Format = "yyyy-mm-dd";
                            //workSheet.Column(13).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(14).Style.Numberformat.Format = "yyyy-mm-dd";
                            workSheet.Column(15).Style.Numberformat.Format = "yyyy-mm-dd";
                            package.Save();
                        }
                        stream.Position = 0;
                        string excelName = $"ClaimReportDaily-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                        return File(stream, "application/octet-stream", excelName);
                    }
                }
                else
                {
                    //TempData["Error"] = "Filter must be fill";
                }

                _db.zsp_AuditTrailInsert("Claim Report Daily", policyNumber, GetSession.UserName());
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
            }

            return View();
        }

        // GET: DipcadClaimReport/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DipcadClaimReport/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DipcadClaimReport/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DipcadClaimReport/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DipcadClaimReport/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DipcadClaimReport/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DipcadClaimReport/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
