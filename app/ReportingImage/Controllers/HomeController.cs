﻿using ReportingImage.Models;
using System.Linq;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class HomeController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        //[CheckAuthorize(Roles = "Home")]
        public ActionResult Index()
        {
            return View();
            //var policyInformation = _db.PolicyInformationDashboard_sp("",GetSession.UserName());
            //return View(policyInformation.ToList());
        }

        public JsonResult UpdateUserLogin(int count)
        {
            if(count == 0)
            {
                Common.LoginOff(_db, GetSession.User_ID());
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[CheckAuthorize(Roles = "Home")]
        public ActionResult Index(string policyNo)
        {
            //var policyInformation = _db.PolicyInformationViews.Where(x => x.POLICYNO == policyNo).GroupBy(a => a.POLICYNO);
            var policyInformation = _db.zsp_PolicyInformationDashboard(policyNo, GetSession.UserName());
            return View(policyInformation.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        #region"Error Pages"
        public ActionResult Error404()
        {
            return View();
        }
        public ActionResult Error403()
        {
            return View();
        }
        public ActionResult Error400()
        {
            return View();
        }
        public ActionResult Error500()
        {
            return View();
        }
        #endregion
    }
}