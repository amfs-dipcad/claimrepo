﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ReportingImage.Models;
using ReportingImage.App_Start;
using ReportingImage.ViewModels;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using System.Drawing.Imaging;
using Ionic.Zip;

namespace ReportingImage.Controllers
{
    public class DipcadClaimPOSAssignController : Controller
    {
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();

        //[CheckAuthorize(Roles = "Case List")]
        // GET: POSAssign
        public ActionResult Index()
        {
            var User = GetSession.UserName();

            var modelPolicyPool = new ListPosPolicyPoolViewModels() { ListPosPolicyPoolOpenViewModels = new List<PosPolicyPoolOpenViewModels>() };
            //var modelPolicyPool = new ListPosPolicyPoolViewModels()
            //{
            //     ListPosPolicyPoolOpenViewModels = GetPolicyPoolList("", "", "", "0","","", 1)
            //};

            #region ' data basic '
            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            var users = GetUserPOS();
            if (users != null)
            {
                foreach (var item in users)
                { dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username }); }
            }
            ViewBag.ddlAssignTo = new SelectList(dataAssignTo, "Value", "Text", null);

            // assign in search
            ViewBag.ddl2AssignTo = new SelectList(dataAssignTo, "Value", "Text", null);
            #endregion

            return View(modelPolicyPool);
        }

        [HttpPost]
        //[CheckAuthorize(Roles = "Case List")]
        public ActionResult Index(ListPosPolicyPoolViewModels models, string Submit, string ddlAssignTo, string assignTo, string txbDateFrom, string txbDateTo, string remarks, string policyNumber, int pageNumber = 1)
        {
            TempData["AssignTo"] = assignTo;

            int count = 0;
            string cek = string.Empty;
            string errorstr = string.Empty;
            string success = string.Empty;
            string user = "";
            int ceknotRR = 0;
            DataTable result;

            #region submit POS Assign
            /*
            //if (Submit == "Submit")
            //{
            //    if (models.ListPosPolicyPoolOpenViewModels == null)
            //    {
            //        TempData["Error"] = $"Data Not Found";
            //        goto ShowList;
            //    }
            //    user = GetSession.UserName();
            //    foreach (PosPolicyPoolOpenViewModels item in models.ListPosPolicyPoolOpenViewModels)
            //    {
            //        if (item.CHECKED)
            //        {
            //            if (item.METHOD != "RR" && (ddlAssignTo == null ? "" : ddlAssignTo).Length > 0)
            //            {
            //                result = Common.ExecuteQuery($"dbo.sp_POS_Submit {item.CASE_ID},'{item.CIF}','{item.POLICY_NO}','','','SYS ASSIGN','{ddlAssignTo}','{remarks}','0',''");
            //                cek = result.Rows[0][0].ToString();
            //                if (cek != "SUCCESS")
            //                {
            //                    if (string.IsNullOrEmpty(errorstr))
            //                    {
            //                        errorstr = $"Case ID ({item.CASE_ID}) {cek} <br>";
            //                    }
            //                    else
            //                    { errorstr += $"Case ID ({item.CASE_ID}) {cek} <br>"; }
            //                    count--;
            //                }
            //                else
            //                { ceknotRR++; }
            //            }
            //            else
            //            {
            //                result = Common.ExecuteQuery($"dbo.sp_POS_RR_assign 'SYS ASSIGN',{item.CASE_ID},'{remarks}'");
            //                cek = result.Rows[0][0].ToString();
            //                if (cek.Contains("Error"))
            //                {
            //                    if (string.IsNullOrEmpty(errorstr))
            //                    {
            //                        errorstr = $"RR Case ID ({item.CASE_ID}) {cek} <br>";
            //                    }
            //                    else
            //                    { errorstr += $"RR Case ID ({item.CASE_ID}) {cek} <br>"; }
            //                    count--;
            //                }
            //                else
            //                {
            //                    if (string.IsNullOrEmpty(success))
            //                    {
            //                        success = $"RR Case ID {item.CASE_ID} to {cek} <br>";
            //                    }
            //                    else
            //                    {
            //                        success += $"RR Case ID {item.CASE_ID} to {cek} <br>";
            //                    }
            //                }
            //            }
            //            count++;
            //        }
            //    }

            //    if (!string.IsNullOrEmpty(errorstr))
            //    {
            //        TempData["Error"] = $"Please check before submit";
            //        goto ShowList;
            //    }

            //    if (count > 0)
            //    {
            //        if (ceknotRR > 0)
            //        {
            //            success = string.IsNullOrEmpty(success) ? "" : success;
            //            success += $"Success Assign To {ddlAssignTo} ";
            //        }
            //        TempData["Success"] = success.Trim();
            //    }

            //    _db.zsp_AuditTrailInsert("Assign To", null, user);
            //}
            //else
            //{
            //    //if (txbDateFrom == "" || txbDateTo == "")
            //    //{
            //    //    TempData["Error"] = $"Choose Date From and To";
            //    //    goto ShowList;
            //    //}
            //}
            */
            #endregion

            user = GetSession.UserName();

        //ShowList:

            ModelState.Clear();
            var modelPolicyPool = new ListPosPolicyPoolViewModels()
            {
                ListPosPolicyPoolOpenViewModels = GetPolicyPoolList(assignTo, policyNumber, "", "", "0", txbDateFrom, txbDateTo, 1)
            };

            #region ' data basic '
            List<SelectListItem> dataAssignTo = new List<SelectListItem>();
            dataAssignTo.Add(new SelectListItem() { Text = null, Value = "" });
            var users = GetUserPOS();
            if (users != null)
            {
                foreach (var item in users)
                { dataAssignTo.Add(new SelectListItem() { Text = item.username, Value = item.username }); }
            }
            ViewBag.ddlAssignTo = new SelectList(dataAssignTo, "Value", "Text", null);

            // assign in search
            ViewBag.ddl2AssignTo = new SelectList(dataAssignTo, "Value", "Text", assignTo);

            List<SelectListItem> listMethod = new List<SelectListItem>();
            listMethod.Add(new SelectListItem() { Text = "Direct", Value = "Direct" });
            listMethod.Add(new SelectListItem() { Text = "RR", Value = "RR" });
            ViewBag.ddlMethod = new SelectList(listMethod, "Value", "Text", null);

            TempData["txbDateFrom"] = txbDateFrom;
            TempData["txbDateTo"] = txbDateTo;
            #endregion

            return View(modelPolicyPool);
        }

        Byte[] fileByte;
        //[CheckAuthorize(Roles = "Images Client POS")]
        public ActionResult ImagesClient(int case_id)
        {
            string sourcePathTmp = string.Empty;
            string sourcePath = string.Empty;
            string filePath = string.Empty;
            var extension = string.Empty;

            POSListImageViewModel model = new POSListImageViewModel();
            List<DetailImageViewModels> images = new List<DetailImageViewModels>();
            //var imagesList = _db.sp_POS_Images(case_id, GetSession.UserName());
            List<ImageResult> imagesList = new List<ImageResult>();

            DataTable dTab = Common.ExecuteQuery($"dbo.SPInq_CLM_Images '{case_id}',''");
            int counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                imagesList.Add(new ImageResult()
                {
                    POLICYNO = dTab.Rows[i]["POLICYNO"].ToString(),
                    FILENAMEORIGIN = dTab.Rows[i]["FILENAMEORIGIN"].ToString(),
                    FILENAME = dTab.Rows[i]["FILENAME"].ToString(),//.Replace(" ", "_"),
                    DOCUMENTNAME = dTab.Rows[i]["DOCUMENTNAME"].ToString(),
                    SOURCE = dTab.Rows[i]["SOURCE"].ToString(),
                    FILEIMAGE = (Byte[])dTab.Rows[i]["FILEIMAGE"],
                    CREATEDATE = Convert.ToDateTime(dTab.Rows[i]["CREATEDATE"].ToString()),
                });
            }

            foreach (var item in imagesList)
            {
                sourcePathTmp = Server.MapPath("~/Files/" + item.POLICYNO);
                sourcePath = Server.MapPath("~/Files/" + item.POLICYNO + "/");
                if (!Directory.Exists(sourcePath))
                {
                    Directory.CreateDirectory(sourcePath);
                }
                else
                {
                    filePath = sourcePath + item.FILENAME;
                    System.IO.File.Delete(filePath);
                }

                if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    using (MemoryStream inStream = new MemoryStream(item.FILEIMAGE))
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        //Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);
                        //fileByte = outStream.ToArray();

                        //Author    : Andhi
                        //Date      : 2020-24-07
                        //Desc      : convert tiff to pdf using for PS
                        fileByte = inStream.ToArray();
                        string bytes = BitConverter.ToString(fileByte).Replace("-", "");
                    }
                }
                else
                {
                    fileByte = item.FILEIMAGE;
                }

                byte[] data = (byte[])fileByte;

                string[] fileName = item.FILENAME.Split('.');
                filePath = sourcePath + fileName[0].ToString();

                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".jpg", data);
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".tiff", data);
                }
                else
                {
                    System.IO.File.WriteAllBytes(sourcePath + fileName[0] + ".pdf", data);
                }

                if (Common.GetStringRight(item.FILENAME, 3) == "jpg" || Common.GetStringRight(item.FILENAME, 4) == "jpeg")
                {
                    extension = "image/jpg";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".jpg";
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "tif" || Common.GetStringRight(item.FILENAME, 4) == "tiff")
                {
                    #region ' Convert tiff to PDF '
                    //Author    : Andhi
                    //Date      : 2020-24-07
                    //Desc      : converting tiff to pdf then delete tiff
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                    using (var stream = new MemoryStream())
                    {
                        using (var docs = new Document())
                        {
                            var writer =
                                iTextSharp.text.pdf.PdfWriter.GetInstance(docs, new FileStream(sourcePath + fileName[0] + ".pdf", FileMode.Create));

                            //count files images in tiff
                            Bitmap bMap = new Bitmap(sourcePath + fileName[0] + ".tiff");
                            int total = bMap.GetFrameCount(FrameDimension.Page);

                            docs.Open();

                            iTextSharp.text.pdf.PdfContentByte cbyte = writer.DirectContent;
                            for (int k = 0; k < total; ++k)
                            {
                                bMap.SelectActiveFrame(FrameDimension.Page, k);
                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bMap, ImageFormat.Bmp);

                                //calculate dimension
                                img.ScalePercent(50f / img.DpiX * 100);
                                img.SetAbsolutePosition(0, 0);
                                cbyte.AddImage(img);
                                docs.NewPage();
                            }

                            docs.Close();
                            bMap.Dispose();
                            FileInfo dirInfo = new FileInfo(sourcePath + fileName[0] + ".tiff");
                            dirInfo.Delete();
                        }
                    }
                    #endregion
                }
                else if (Common.GetStringRight(item.FILENAME, 3) == "pdf")
                {
                    extension = "application/pdf";
                    filePath = "../Files/" + item.POLICYNO + "/" + fileName[0].ToString() + ".pdf";
                }

                images.Add(new DetailImageViewModels()
                {
                    PolicyNumber = item.POLICYNO,
                    Checked = false,
                    DocumentName = item.DOCUMENTNAME,
                    FILENAME = item.FILENAME,
                    FILENAMEORIGIN = item.FILENAMEORIGIN,
                    Source = item.SOURCE,
                    FILEIMAGE = fileByte,
                    Extension = extension,
                    PreviewContentId = Common.SplitValueFileName(item.FILENAME),
                    FILEIMAGE_temp = item.FILEIMAGE,
                    FilePath = filePath,
                    CreatedDate = Convert.ToDateTime(item.CREATEDATE)
                });
            }

            model.Case_ID = case_id;
            model.DetailImageViewModels = images;

            return View(model);
        }

        [HttpPost]
        [CheckAuthorize(Roles = "Images Client POS")]
        public ActionResult ImagesClient(POSListImageViewModel images)
        {
            try
            {
                string policyNo = string.Empty;
                string sourcePathTmp = string.Empty;
                string sourcePath = string.Empty;
                string filePath = string.Empty;
                string zipName = string.Empty;
                int count = 0;

                string[] arrFileName;
                string fileName = string.Empty;

                using (ZipFile zip = new ZipFile())
                {
                    foreach (var item in images.DetailImageViewModels)
                    {
                        if (item.Checked)
                        {
                            count++;
                            if (policyNo == string.Empty)
                            { policyNo = item.PolicyNumber; }

                            sourcePathTmp = Server.MapPath("~/Files/" + item.PolicyNumber);
                            sourcePath = Server.MapPath("~/Files/" + item.PolicyNumber + "/");
                            if (!Directory.Exists(sourcePath))
                            {
                                Directory.CreateDirectory(sourcePath);
                            }

                            DataTable dTab = Common.ExecuteQuery($"[dbo].[sp_POS_DetailImageViewTmp] '{item.PolicyNumber}','{item.FILENAMEORIGIN}'");
                            DetailImageTempView dataImage = new DetailImageTempView();

                            if (dTab.Rows.Count > 0)
                            {
                                dataImage.FileImage = (Byte[])dTab.Rows[0]["FILEIMAGE"];
                            }


                            //var fileImageTmp = _db.sp_POS_DetailImageViewTmp(item.PolicyNumber, item.FILENAMEORIGIN).ToList().Single().FILEIMAGE;
                            //byte[] data = (byte[])item.FILEIMAGE_temp;
                            var fileImageTmp = dataImage.FileImage;
                            byte[] data = (byte[])fileImageTmp;

                            System.IO.File.WriteAllBytes(sourcePath + item.FILENAME, data);

                            filePath = sourcePath + item.FILENAME;
                            zip.AddFile(filePath, "");
                        }
                    }

                    if (count == 0)
                    {
                        TempData["Error"] = "Please check before submit";
                        goto End;
                    }

                    _db.zsp_AuditTrailInsert("Download Detail Image POS", policyNo, GetSession.UserName());

                    Response.Clear();
                    Response.BufferOutput = false;
                    zipName = $"{policyNo}_{DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")}.zip";
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                }

                foreach (var item in images.DetailImageViewModels)
                {
                    if (item.Checked)
                    {
                        filePath = sourcePath + item.FILENAME;
                        System.IO.File.Delete(filePath);
                        if (item.Extension == "image/jpg")
                        {
                            arrFileName = item.FILENAME.Split('.');
                            fileName = arrFileName[0] + ".jpg";
                            filePath = sourcePath + fileName;
                            System.IO.File.Delete(filePath);
                        }
                    }
                }
                Directory.Delete(sourcePath);
            }
            catch (Exception er)
            {
                throw;
            }
        End:
            return View(images);
        }

        //------------------------------------------------------------------ attribut non action ---------------------------------
        public static List<PosPolicyPoolOpenViewModels> GetPolicyPoolList(string assignTo, string policyNumber, string user, string ddlCategory, string statusComplete, string datefr, string dateto, int pageNumber)
        {
            List<PosPolicyPoolOpenViewModels> models = new List<PosPolicyPoolOpenViewModels>();
            int counter = 0;

            if (ddlCategory == "ALL") ddlCategory = "";
            DataTable dTab = Common.ExecuteQuery($"dbo.[SPInq_CLM_POSList] '{assignTo}','{policyNumber}','{ddlCategory}','','{datefr}','{dateto}','{pageNumber}'");
            counter = dTab.Rows.Count;
            for (int i = 0; i < dTab.Rows.Count; i++)
            {
                models.Add(new PosPolicyPoolOpenViewModels()
                {
                    CASE_ID = Convert.ToInt32(dTab.Rows[i]["CASE_ID"].ToString()),
                    CIF = dTab.Rows[i]["CIF"].ToString(),
                    POLICY_NO = dTab.Rows[i]["POLICY_NO"].ToString(),
                    SOURCE = dTab.Rows[i]["SOURCE"].ToString(),
                    WORKTYPE_ID = dTab.Rows[i]["WORKTYPE_ID"].ToString(),
                    WORKTYPE = dTab.Rows[i]["WORKTYPE"].ToString(),
                    CATEGORY = dTab.Rows[i]["CATEGORY"].ToString(),
                    RLS_TRANS = dTab.Rows[i]["RLS_TRANS"].ToString(),
                    RLS_CAPTURE_DATE = String.IsNullOrWhiteSpace(dTab.Rows[i]["RLS_CAPTURE_DATE"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dTab.Rows[i]["RLS_CAPTURE_DATE"].ToString()),
                    USER_ASSIGN = dTab.Rows[i]["USER_ASSIGN"].ToString(),
                    CREATE_DATE = Convert.ToDateTime(dTab.Rows[i]["CREATE_DATE"].ToString()),
                    BUCKET = dTab.Rows[i]["BUCKET"].ToString(),
                    CHECKED = false,
                    COUNTS = counter
                });
            }

            return models;
        }

        public List<UserView> GetUserPOS()
        {
            DataTable dTab = Common.ExecuteQuery($"dbo.[SPInq_CLM_POSgetUserActive]");
            List<UserView> usr = new List<UserView>();

            foreach (DataRow item in dTab.Rows)
            {
                usr.Add(new UserView() { username = item["USERNAME"].ToString() });
            }

            return usr;
        }
    }
}
