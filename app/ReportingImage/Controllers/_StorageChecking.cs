﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace ReportingImage.Controllers
{
    public class _StorageChecking
    {
        public bool AvailableFreeSpace()
        {
            string driveName = ConfigurationManager.AppSettings["CheckDir"].ToString();
            long freeSpace = 0;
                        
            DriveInfo dInfo = new DriveInfo(driveName);
            freeSpace = dInfo.AvailableFreeSpace;

            freeSpace = freeSpace / 1024; //KB
            freeSpace = freeSpace / 1024; //MB
            freeSpace = freeSpace / 1024; //GB

            if(freeSpace < 0)
            { return false; }

            return true;
        }
    }
}