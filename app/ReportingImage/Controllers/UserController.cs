﻿using ReportingImage.App_Start;
using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ReportingImage.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        ReportingImageEntities _db = new ReportingImageEntities();
        GetSession MeGetSession = new GetSession();
        [CheckAuthorize(Roles = "User")]
        public ActionResult Index(int pageNumber = 1)
        {
            var userName = GetSession.UserName();
            var privilegeId = GetSession.Privilege_ID();
            var privilegeName = _db.zPrevilages.Where(a => a.id == privilegeId).Single().previlage_name;
            var userId = GetSession.User_ID();

            var userList = _db.zsp_User(userId, privilegeId).ToList();
            //var parentLists = _db.zsp_UserParentList(GetSession.User_ID()).ToList();
            //foreach (var itemUser in parentLists)
            //{
            //    userName += "," + itemUser.username;
            //    var parentListsA = _db.zsp_UserParentList(itemUser.id).ToList();
            //    foreach (var itemUserA in parentListsA)
            //    {
            //        userName += "," + itemUserA.username;
            //    }
            //}

            //var modelUser = new PagedList<UserViewModels>();
            //modelUser.Content = GetUserList(userId, privilegeId, pageNumber, userName);
            //modelUser.TotalRecords = Convert.ToInt32(modelUser.Content.Count == 0 ? 0 : modelUser.Content.FirstOrDefault().Counts);
            //modelUser.CurrentPage = pageNumber;
            //modelUser.PageSize = 10;

            List<UserViewModels> modelUser = new List<UserViewModels>();
            foreach (var item in userList)
            {
                var _user = _db.zUsers.FirstOrDefault(o => o.id == item.id);
                modelUser.Add(new UserViewModels()
                {
                    id = item.id,
                    username = item.username,
                    dept_id = item.dept_id,
                    parent_name = item.parent_id,
                    privilege_name = item.previlage_name,
                    created_date = Convert.ToDateTime(item.created_date),
                    created_by = item.created_by,
                    updated_date = item.updated_date,
                    updated_by = item.updated_by,
                    is_login = _user.is_login.HasValue ? _user.is_login : false
                });

                if (privilegeName != "Super Admin")
                {
                    var parentLists = _db.zsp_UserParentList(item.id).ToList();
                    foreach (var itemUser in parentLists)
                    {
                        var _userParent = _db.zUsers.FirstOrDefault(o => o.id == item.id);
                        modelUser.Add(new UserViewModels()
                        {
                            id = itemUser.id,
                            username = itemUser.username,
                            dept_id = itemUser.dept_id,
                            parent_name = itemUser.parent_id,
                            privilege_name = itemUser.previlage_name,
                            created_date = Convert.ToDateTime(itemUser.created_date),
                            created_by = itemUser.created_by,
                            updated_date = itemUser.updated_date,
                            updated_by = itemUser.updated_by,
                            is_login = _userParent.is_login.HasValue ? _userParent.is_login : false
                        });

                        var parentListA = _db.zsp_UserParentList(itemUser.id).ToList();

                        foreach (var itemUserA in parentListA)
                        {
                            var _userParentA = _db.zUsers.FirstOrDefault(o => o.id == item.id);
                            modelUser.Add(new UserViewModels()
                            {
                                id = itemUserA.id,
                                username = itemUserA.username,
                                dept_id = itemUserA.dept_id,
                                parent_name = itemUserA.parent_id,
                                privilege_name = itemUserA.previlage_name,
                                created_date = Convert.ToDateTime(itemUserA.created_date),
                                created_by = itemUserA.created_by,
                                updated_date = itemUserA.updated_date,
                                updated_by = itemUserA.updated_by,
                                is_login = _userParent.is_login.HasValue ? _userParent.is_login : false
                            });
                        }
                    }
                }
            }

            ViewBag.priveledge = privilegeName;
            _db.zsp_AuditTrailInsert("User", null, userName);

            return View(modelUser);
        }

        [CheckAuthorize(Roles = "Create User")]
        public ActionResult Create()
        {
            var privilegeId = GetSession.Privilege_ID();

            List<DepartmentViewModels> DepartmentList = new List<DepartmentViewModels>();
            DepartmentList.Add(new DepartmentViewModels { dept_id = "IT", dept_name = "IT" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "NBUW", dept_name = "NBUW" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "POS", dept_name = "POS" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "CLAIM", dept_name = "CLAIM" });

            var UserList = _db.zUsers.ToList();
            List<ParentViewModels> ParentList = new List<ParentViewModels>();
            ParentList.Add(new ParentViewModels { Parent_Id = null, Parent_Name = "None" });
            foreach (var itemUser in UserList)
            {
                ParentList.Add(new ParentViewModels { Parent_Id = itemUser.id.ToString(), Parent_Name = itemUser.username });
            }

            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();

            ViewBag.deptId = new SelectList(DepartmentList, "dept_id", "dept_name");
            ViewBag.parentId = new SelectList(ParentList, "Parent_Id", "Parent_Name");
            ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name");

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Create User", null, userName);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Create User")]
        public ActionResult Create(UserViewModels zUser)
        {
            var privilegeId = GetSession.Privilege_ID();
            string password = string.Empty;
            if (zUser.password != null)
            {
                password = Common.EncodePasswordMd5(zUser.password);
            }

            if (zUser.username == null) { ModelState.AddModelError("username", "This field is required."); }
            if (!Convert.ToBoolean(zUser.active_directory))
            {
                if (zUser.password == null) { ModelState.AddModelError("password", "This field is required."); }
            }

            if (zUser.previlage_id == null) { ModelState.AddModelError("previlage_id", "This field is required."); }
            //check username in database if already exist skip
            var CheckUserName = _db.zUsers.Where(x => x.username == zUser.username).ToList();
            if (CheckUserName.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_UserInsert(GetSession.UserName().ToString(), zUser.username, zUser.parent_id, zUser.previlage_id,
                       zUser.dept_id, zUser.min_ape, zUser.max_ape, zUser.active_directory, password);
                    _db.zsp_AuditTrailInsert("Create User", null, GetSession.UserName());
                    TempData["Success"] = "Success for Saving " + zUser.username;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Name :" + zUser.username + " already exist!";
            }

            List<DepartmentViewModels> DepartmentList = new List<DepartmentViewModels>();
            DepartmentList.Add(new DepartmentViewModels { dept_id = "IT", dept_name = "IT" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "NBUW", dept_name = "NBUW" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "POS", dept_name = "POS" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "CLAIM", dept_name = "CLAIM" });

            var UserList = _db.zUsers.ToList();
            List<ParentViewModels> ParentList = new List<ParentViewModels>();
            ParentList.Add(new ParentViewModels { Parent_Id = null, Parent_Name = "None" });
            foreach (var itemUser in UserList)
            {
                ParentList.Add(new ParentViewModels { Parent_Id = itemUser.id.ToString(), Parent_Name = itemUser.username });
            }

            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();

            ViewBag.deptId = new SelectList(DepartmentList, "dept_id", "dept_name", zUser.dept_id);
            ViewBag.parentId = new SelectList(ParentList, "Parent_Id", "Parent_Name", zUser.parent_id);
            ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name", zUser.previlage_id);
            return View(zUser);
        }

        [CheckAuthorize(Roles = "Update User")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<DepartmentViewModels> DepartmentList = new List<DepartmentViewModels>();
            DepartmentList.Add(new DepartmentViewModels { dept_id = "IT", dept_name = "IT" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "NBUW", dept_name = "NBUW" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "POS", dept_name = "POS" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "CLAIM", dept_name = "CLAIM" });

            var UserList = _db.zUsers.ToList();
            var privilegeId = GetSession.Privilege_ID();
            List<ParentViewModels> ParentList = new List<ParentViewModels>();
            ParentList.Add(new ParentViewModels { Parent_Id = null, Parent_Name = "None" });
            foreach (var itemUser in UserList)
            {
                ParentList.Add(new ParentViewModels { Parent_Id = itemUser.id.ToString(), Parent_Name = itemUser.username });
            }

            zUser zUser = _db.zUsers.Find(id);
            if (zUser == null)
            {
                return HttpNotFound();
            }

            UserViewModels modelUser = new UserViewModels();
            modelUser.username = zUser.username;
            modelUser.dept_id = zUser.dept_id;
            modelUser.parent_id = zUser.parent_id;
            modelUser.previlage_id = zUser.previlage_id;
            modelUser.min_ape = Convert.ToDecimal(zUser.min_ape);
            modelUser.max_ape = Convert.ToDecimal(zUser.max_ape);
            modelUser.active_directory = Convert.ToBoolean(zUser.active_directory);
            modelUser.password = zUser.password;

            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();

            ViewBag.deptId = new SelectList(DepartmentList, "dept_id", "dept_name", zUser.dept_id);
            ViewBag.parentId = new SelectList(ParentList, "Parent_Id", "Parent_Name", zUser.parent_id);
            ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name", zUser.previlage_id);

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Update User", null, userName);

            return View(modelUser);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckAuthorize(Roles = "Update User")]
        public ActionResult Edit(UserViewModels zUser)
        {
            var userName = GetSession.UserName();
            string password = string.Empty;
            if (zUser.password != null)
            {
                password = Common.EncodePasswordMd5(zUser.password);
            }
            if (zUser.username == null) { ModelState.AddModelError("username", "This field is required."); }
            if (Convert.ToBoolean(!zUser.active_directory))
            {
                if (zUser.password == null) { ModelState.AddModelError("password", "This field is required."); }
            }
            if (zUser.id == null) { ModelState.AddModelError("privilege_id", "This field is required."); }

            //check data in database if already exist skip
            var CheckUserName = _db.zUsers.Where(x => x.username == zUser.username && x.id != zUser.id).ToList();
            if (CheckUserName.Count() == 0)
            {
                if (ModelState.IsValid)
                {
                    _db.zsp_UserUpdate(zUser.id, userName, zUser.username, zUser.parent_id, zUser.previlage_id,
                         zUser.dept_id, zUser.min_ape, zUser.max_ape, zUser.active_directory, password, false);
                    _db.zsp_AuditTrailInsert("Update User", null, GetSession.UserName());
                    TempData["Success"] = "Success Edit for " + zUser.username;
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Error"] = "Name :" + zUser.username + " already exist!";
            }

            List<DepartmentViewModels> DepartmentList = new List<DepartmentViewModels>();
            DepartmentList.Add(new DepartmentViewModels { dept_id = "IT", dept_name = "IT" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "NBUW", dept_name = "NBUW" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "POS", dept_name = "POS" });
            DepartmentList.Add(new DepartmentViewModels { dept_id = "CLAIM", dept_name = "CLAIM" });

            var UserList = _db.zUsers.ToList();
            List<ParentViewModels> ParentList = new List<ParentViewModels>();
            ParentList.Add(new ParentViewModels { Parent_Id = null, Parent_Name = "None" });
            foreach (var itemUser in UserList)
            {
                ParentList.Add(new ParentViewModels { Parent_Id = itemUser.id.ToString(), Parent_Name = itemUser.username });
            }

            var privilegeId = GetSession.Privilege_ID();
            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();

            ViewBag.deptId = new SelectList(DepartmentList, "dept_id", "dept_name", zUser.dept_id);
            ViewBag.parentId = new SelectList(ParentList, "Parent_Id", "Parent_Name", zUser.parent_id);
            ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name", zUser.previlage_id);
            return View(zUser);
        }

        public Boolean CheckPassword(int? id, string password)
        {
            ReportingImageEntities _dbx = new ReportingImageEntities();
            zUser DBPassword = _dbx.zUsers.Find(id);
            if (DBPassword.password == password)
            {
                return true;
            }
            _dbx.Dispose();
            return false;
        }

        // GET: User/Delete/5
        //[CheckAuthorize(Roles = "Delete User")]
        [CheckAuthorize(Roles = "Delete User")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zUser zUser = _db.zUsers.Find(id);
            if (zUser == null)
            {
                TempData["Error"] = "User not Found";
            }
            _db.zsp_AuditTrailInsert("Delete User", null, GetSession.UserName());
            _db.zsp_UserUpdate(zUser.id, GetSession.User_ID().ToString(), zUser.username, zUser.parent_id, zUser.previlage_id,
                      zUser.dept_id, zUser.min_ape, zUser.max_ape, zUser.active_directory, zUser.password, true);

            TempData["Success"] = "Delete Success for " + zUser.username;
            return RedirectToAction("Index");
        }

        //[CheckAuthorize(Roles = "Profile User")]
        public ActionResult Profile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            zUser zUser = _db.zUsers.Find(id);

            if (zUser == null)
            {
                return HttpNotFound();
            }

            var privilegeId = GetSession.Privilege_ID();
            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();
            if (!GetSession.IsAdministrator())
            {
                int PrivID = zUser.id;
                ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name", zUser.previlage_id);
            }
            else
            {
                return RedirectToAction("../Home");
            }
            UserViewModels modelUser = new UserViewModels();
            modelUser.username = zUser.username;
            modelUser.password = zUser.password;
            modelUser.previlage_id = zUser.previlage_id;
            modelUser.active_directory = Convert.ToBoolean(zUser.active_directory);
            //modelUser.bucket = zUser.bucket.HasValue ? zUser.bucket.Value : false;

            var userName = GetSession.UserName();
            _db.zsp_AuditTrailInsert("Profile User", null, userName);

            return View(modelUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Profile(UserViewModels zUser)
        {
            if (zUser.username == null) { ModelState.AddModelError("username", "This field is required."); }
            if (Convert.ToBoolean(!zUser.active_directory))
            {
                if (zUser.password == null) { ModelState.AddModelError("password", "This field is required."); }
            }
            if (zUser.previlage_id == null) { ModelState.AddModelError("privilege_id", "This field is required."); }

            //check data in database if already exist skip
            var CheckUserName = _db.zUsers.Where(x => x.username == zUser.username && x.id != zUser.id).ToList();
            if (CheckUserName.Count() == 0)
            {

                if (ModelState.IsValid)
                {
                    var userName = GetSession.UserName();
                    _db.zsp_AuditTrailInsert("Profile User", null, userName);

                    _db.zsp_UserChangePassword(zUser.id, GetSession.UserName(), Common.EncodePasswordMd5(zUser.password), zUser.active_directory);
                    TempData["Success"] = "Success Edit for " + zUser.username;
                    ViewBag.privilege_id = new SelectList(_db.zPrevilages.Where(x => x.id == zUser.previlage_id).ToList(), "id", "previlage_name", zUser.previlage_id);
                }
            }
            else
            {
                TempData["Error"] = "Name :" + zUser.username + " already exist!";
            }

            UserViewModels modelUser = new UserViewModels();
            modelUser.username = zUser.username;
            modelUser.password = _db.zUsers.Find(zUser.id).password;
            modelUser.previlage_id = zUser.previlage_id;
            modelUser.active_directory = Convert.ToBoolean(zUser.active_directory);
            //modelUser.bucket = zUser.bucket.HasValue ? zUser.bucket.Value : false;

            var privilegeId = GetSession.Privilege_ID();
            var privilegeList = _db.zsp_PrivilegeUserDDL(privilegeId).ToList();
            if (!GetSession.IsAdministrator())
            {
                int PrivID = zUser.id;
                ViewBag.privilege_id = new SelectList(privilegeList, "id", "previlage_name", privilegeId);
            }
            return View(modelUser);
        }

        public static List<UserViewModels> GetUserList(int userId, int? privilegeId, int pageNumber, string parentUserName)
        {
            List<UserViewModels> model = new List<UserViewModels>();

            DataTable tbl = Common.ExecuteQuery("dbo.zsp_UserTmp '" + userId + "','" + privilegeId + "','" + pageNumber + "','" + parentUserName + "'");

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                DateTime updatedDate = DateTime.Now;
                if (tbl.Rows[i]["updated_date"].ToString() == "")
                {
                    updatedDate = Convert.ToDateTime(tbl.Rows[i]["created_date"].ToString());
                }
                else
                {
                    updatedDate = Convert.ToDateTime(tbl.Rows[i]["updated_date"].ToString());
                }

                model.Add(new UserViewModels()
                {
                    id = Convert.ToInt32(tbl.Rows[i]["id"].ToString())
                    ,
                    username = tbl.Rows[i]["username"].ToString()
                    ,
                    dept_id = tbl.Rows[i]["dept_id"].ToString()
                    ,
                    parent_name = tbl.Rows[i]["parent_id"].ToString()
                    ,
                    privilege_name = tbl.Rows[i]["previlage_name"].ToString()
                    ,
                    created_date = Convert.ToDateTime(tbl.Rows[i]["created_date"].ToString())
                    ,
                    created_by = tbl.Rows[i]["created_by"].ToString()
                    ,
                    updated_date = updatedDate
                    ,
                    updated_by = tbl.Rows[i]["updated_by"].ToString()
                    ,
                    password = tbl.Rows[i]["password"].ToString()
                    ,
                    active_directory = Convert.ToBoolean(tbl.Rows[i]["active_directory"].ToString())
                    ,
                    Counts = Convert.ToInt32(tbl.Rows[i]["Counts"].ToString())
                });
            }
            return model;
        }

        public ActionResult ChangeLogStatus(int? id, bool is_login)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zUser zUser = _db.zUsers.Find(id);
            if (zUser != null)
            {
                try
                {
                    ReportingImageEntities db = new ReportingImageEntities();
                    zUser.is_login = (is_login) ? false : true;
                    db.Entry(zUser).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return RedirectToAction("Index");
        }
    }
}