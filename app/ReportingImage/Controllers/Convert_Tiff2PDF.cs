﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using BitMiracle.LibTiff.Classic;
using ReportingImage.Controllers.libTiffNet;
using System.Globalization;

namespace ReportingImage.Controllers
{
    public class Convert_Tiff2PDF
    {
        public string FromFile = string.Empty;
        public string ToFile = string.Empty;
        T2P t2p = null;
        public Convert_Tiff2PDF(string tiffFromFile, string pdfToFile)
        {
            this.FromFile = tiffFromFile;
            this.ToFile = pdfToFile;
            t2p = new T2P();
            t2p.m_testFriendly = true;
        }

        public string fromByte(byte[] TiffByte)
        {
            //tambahan untuk
            string result = string.Empty;
            //tiff2pdf_itextshrp check = new tiff2pdf_itextshrp();
            //if(check.count_page(FromFile) == 1)
            //{
            //    result = check.converting(FromFile,ToFile);
            //    if(!string.IsNullOrWhiteSpace(result))
            //    {
            //        return result;
            //    }
            //}

            MemoryStream mStream = new MemoryStream(TiffByte);
            using (Tiff input = Tiff.ClientOpen("in-memory","r", mStream, new TiffStream()))
            {
                try {
                    startConvertTiff2PDF(input);

                    //if pdf error
                    //tiff2pdf_itextshrp itxt = new tiff2pdf_itextshrp();
                    //if(itxt.isErrorPDF(ToFile))
                    //{
                    //    if (File.Exists(ToFile))
                    //    {
                    //        FileInfo dirInfo = new FileInfo(ToFile);
                    //        dirInfo.Delete();
                    //    }
                    //    itxt.converting(FromFile,ToFile);
                    //}
                }
                catch(Exception err)
                {
                    result = err.Message;
                }
            }
            return result;
        }

        public void startConvertTiff2PDF(Tiff tiffInput)
        {
            t2p.m_outputdisable = false;

            t2p.m_outputdisable = false;
            t2p.m_outputfile = System.IO.File.Open(this.ToFile, FileMode.Create, FileAccess.Write);

            using (Tiff output = Tiff.ClientOpen(this.ToFile, "w", t2p, t2p.m_stream))
            {
                if (output == null)
                {
                    t2p.m_outputfile.Dispose();
                }
                t2p.validate();

                object client = output.Clientdata();
                TiffStream stream = output.GetStream();
                stream.Seek(client, 0, SeekOrigin.Begin);

                FieldValue[] value = tiffInput.GetField(TiffTag.IMAGEWIDTH);
                int width = value[0].ToInt();

                value = tiffInput.GetField(TiffTag.IMAGELENGTH);
                int height = value[0].ToInt();

                value = tiffInput.GetField(TiffTag.XRESOLUTION);
                float dpiX = value[0].ToFloat();

                value = tiffInput.GetField(TiffTag.YRESOLUTION);
                float dpiY = value[0].ToInt();

                if (dpiX == 0)
                    t2p.m_pdf_defaultxres = float.Parse("15", CultureInfo.InvariantCulture) / (t2p.m_pdf_centimeters ? 2.54F : 1.0F);//Angka 15 ini dari experimental, kalau ad pdf cacadnya veri beda mungkin harus diganti lagi sampai dapat rumus yang benar
                if (dpiY == 0)
                    t2p.m_pdf_defaultyres = float.Parse("15", CultureInfo.InvariantCulture) / (t2p.m_pdf_centimeters ? 2.54F : 1.0F);//ini juga


                FieldValue[] result = tiffInput.GetField(TiffTag.IMAGEWIDTH);

                //to fix ycbcr photometric tiff (color tiff)
                t2p.m_pdf_defaultcompression = t2p_compress_t.T2P_COMPRESS_NONE;
                //t2p.m_pdf_defaultcompression = t2p_compress_t.T2P_COMPRESS_ZIP;
                t2p.m_pdf_nopassthrough = false;

                //set page to letter
                //if (tiff2pdf_match_paper_size(out t2p.m_pdf_defaultpagewidth, out t2p.m_pdf_defaultpagelength, "Letter"))
                //    t2p.m_pdf_overridepagesize = true;

                t2p.write_pdf(tiffInput, output);
                if (t2p.m_error)
                {
                    t2p.m_outputfile.Dispose();
                }
            }

            t2p.m_outputfile.Dispose();

        }
    }



}