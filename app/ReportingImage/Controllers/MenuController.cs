﻿using ReportingImage.Models;
using ReportingImage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ReportingImage.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        ReportingImageEntities _db = new ReportingImageEntities();
        public ActionResult Menu(string controller, string action)
        {
            ViewBag.UserID = GetSession.User_ID();
            var xData = (from x in _db.zMenus
                         where x.Control.ToUpper() == controller.ToUpper() && x.Action.ToUpper() == action.ToUpper()
                         select x);
            int ID1 = 0, ID2 = 0, ID3 = 0;
            if (xData.ToList().Count() != 0)
            {
                ID1 = Convert.ToInt32(xData.Single().ParentID);
                ID2 = Convert.ToInt32(xData.Single().id);
                var D3 = _db.zMenus.Where(x => x.id ==  ID1).FirstOrDefault();
                try
                {
                    ID3 = Convert.ToInt32(D3.ParentID);
                }
                catch { }

            }
            else
            {
                var d = (from x in _db.zMenus
                         where x.Control.ToUpper() == controller.ToUpper() && x.Action.ToUpper() == "INDEX"
                         select x);
                if (d.ToList().Count != 0)
                {
                    ID1 = Convert.ToInt32(d.Single().ParentID);
                    ID2 = Convert.ToInt32(d.Single().id);
                    var D3 = _db.zMenus.Where(x => x.id == ID1).FirstOrDefault();
                    try
                    {
                        ID3 = Convert.ToInt32(D3.ParentID);
                    }
                    catch { }
                }

            }


            List<MenuViewModels> DT = DTMenu(0, ID1, ID2, ID3);
            MenuModels DTPartial = new ViewModels.MenuModels { Menu = DT };
            //   (from x in dataBase.Customers
            //    where x.Name == "Test"
            //selext x).ToList().ForEach(xx => x.Name = "New Name");

            return PartialView("_Menu", DTPartial);
        }

        public List<MenuViewModels> DTMenu(int? ParentIDValue, int? ID1, int? ID2, int? ID3)
        {

            List<MenuViewModels> DATA = new List<MenuViewModels>();
            List<zMenu> DT;
            if (GetSession.IsAdministrator())
            {
                DT = (from c in _db.zMenus
                      where c.ParentID == ParentIDValue && c.ShowMenu == true
                      orderby c.Sort
                      select c).ToList();
                DT.Where(x => x.id.ToString() == ID1.ToString()).ToList().ForEach(x => x.Description = "active");
                DT.Where(x => x.id.ToString() == ID2.ToString()).ToList().ForEach(x => x.Description = "active");
                DT.Where(x => x.id.ToString() == ID3.ToString()).ToList().ForEach(x => x.Description = "active");
            }
            else
            {
                int Priv_id = GetSession.Privilege_ID();
                DT = (from c in _db.zMenus
                      join b in _db.zRole_Menu on c.id equals b.menu_id
                      where c.ParentID == ParentIDValue && c.ShowMenu == true && b.previlage_id == Priv_id && b.is_read == true
                      orderby c.Sort
                      select c).ToList();
                DT.Where(x => x.id.ToString() == ID1.ToString()).ToList().ForEach(x => x.Description = "active");
                DT.Where(x => x.id.ToString() == ID2.ToString()).ToList().ForEach(x => x.Description = "active");
                DT.Where(x => x.id.ToString() == ID3.ToString()).ToList().ForEach(x => x.Description = "active");
            }


            foreach (zMenu itemMenu in DT)
            {
                MenuController MeMC = new Controllers.MenuController();
                var Menu = new MenuViewModels
                {
                    Parent = itemMenu,
                    Child = MeMC.DTMenu(Convert.ToInt32(itemMenu.id.ToString()), ID1, ID2, ID3)
                };

                DATA.Add(Menu);
            }

            return DATA;
        }

        public ActionResult Breadcrumb(string Controller, string Action)
        {
            List<zMenu> DataMenu = new List<zMenu>();
            var DMenu = _db.zMenus.Where(x => x.Control == Controller && x.Action == Action).FirstOrDefault();
            DataMenu.Add(DMenu);
            int ParentID = DMenu == null ? 0 : Convert.ToInt32(DMenu.ParentID);
            var loop = false;
            while (loop == false)
            {
                var DRow = _db.zMenus.Where(x => x.id == ParentID).FirstOrDefault();
                if (DRow == null)
                {
                    loop = true;
                }
                else
                {
                    DataMenu.Add(DRow);
                    ParentID = Convert.ToInt32(DRow.ParentID);
                }
            }
            var Breadcrumb = DMenu == null ? null : DataMenu.OrderBy(x => x.Sort);
            return PartialView("_Breadcrumb", Breadcrumb);
        }
    }
}