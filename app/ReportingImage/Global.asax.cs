﻿using ReportingImage.Controllers;
using ReportingImage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ReportingImage
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static string userId;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //protected void Application_End()
        //{
        //    ReportingImageEntities _db = new ReportingImageEntities();
        //    Common.LoginOff(_db, HttpContext.Current.Session["User_ID"].ToString());
        //    Session.RemoveAll();
        //}
        //protected void Session_Start()
        //{
        //    if(HttpContext.Current.Session["User_ID"] != null)
        //    {
        //        userId = HttpContext.Current.Session["User_ID"].ToString();
        //    }
        //}
        protected void session_end()
        {
            //reportingimageentities _db = new reportingimageentities();
            //common.loginoff(_db, userid);
            //session.removeall();
            try
            {
                Common.RemoveSession(int.Parse(Session["User_ID"].ToString()));
            }
            catch (Exception ex)
            {
                throw;
            }

            Session.RemoveAll();
        }
    }
}
